﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System;
using UnityEngine.UI;

public class AdmobControl : MonoBehaviour
{
    public string ANDROID_BANNER_ID = "";
    public string ANDROID_INTERTITIAL_ID = "";

    public string IOS_BANNER_ID = "";
	public string IOS_INTERTITIAL_ID = "";

    public bool PositionTop = false;

	public Text m_text;
    public BannerView bannerView;
    public InterstitialAd interstitial;

    private static AdmobControl _instance;

    public static AdmobControl Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {

      PlayerPrefs.SetFloat("PlayerMoney",999999); 
       // m_text.text = ANDROID_INTERTITIAL_ID.ToString();

       // Debug.Log("ID " + ANDROID_INTERTITIAL_ID);
       
        if (_instance == null)
        {
            _instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
		int rd = UnityEngine.Random.Range (0, 2);
		if (rd == 1)
			ANDROID_INTERTITIAL_ID = "ca-app-pub-1311538535873216/4964355680";
        RequestBanner();
    }

    public void requeseadmobfull()
    {
        int rd = (int)UnityEngine.Random.Range(0, 2);
        if (rd == 1)
        {
            ANDROID_INTERTITIAL_ID = "ca-app-pub-8500210729663715/8239969188";
        }
        else
        {
            ANDROID_INTERTITIAL_ID = "ca-app-pub-9600655386058129/4930067498";
        }
        RequestInterstitial();
    }

    // Use this for initialization
    void Start()
    {
    
        RequestInterstitial();
       showBanner();
       
    }
    public void hideBanner()
    {
        bannerView.Hide();
    }
    public void showBanner()
    {
        bannerView.Show();
    }
    public void showInterstitial()
    {
        //UIData.iShowAd++;
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
			//UIData.iShowAd = 0;
            StartCoroutine(waitRequest());
        }
        else
        {

            print("Interstitial is not ready yet.");
        }

    }
    IEnumerator waitRequest()
    {
        yield return new WaitForSeconds(1f);
        RequestInterstitial();
    }
    void OnDestroy()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

    }

    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
			string adUnitId = ANDROID_BANNER_ID;
#elif UNITY_IPHONE
			string adUnitId = IOS_BANNER_ID;
#else
			string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        if (PositionTop)
        {
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
        }
        else
        {
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        }

        // Register for ad events.
        bannerView.AdLoaded += HandleAdLoaded;
        bannerView.AdFailedToLoad += HandleAdFailedToLoad;
        bannerView.AdOpened += HandleAdOpened;
        bannerView.AdClosing += HandleAdClosing;
        bannerView.AdClosed += HandleAdClosed;
        bannerView.AdLeftApplication += HandleAdLeftApplication;
        // Load a banner ad.
        bannerView.LoadAd(createAdRequest());
    }

    public void RequestInterstitial()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
			string adUnitId = ANDROID_INTERTITIAL_ID;
#elif UNITY_IPHONE
			string adUnitId = IOS_INTERTITIAL_ID;
#else
			string adUnitId = "unexpected_platform";
#endif

        // Create an interstitial.
        interstitial = new InterstitialAd(adUnitId);
        // Register for ad events.
        interstitial.AdLoaded += HandleInterstitialLoaded;
        interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
        interstitial.AdOpened += HandleInterstitialOpened;
        interstitial.AdClosing += HandleInterstitialClosing;
        interstitial.AdClosed += HandleInterstitialClosed;
        interstitial.AdLeftApplication += HandleInterstitialLeftApplication;
        // Load an interstitial ad.
        interstitial.LoadAd(createAdRequest());
    }

    // Returns an ad request with custom ad targeting.
    public AdRequest createAdRequest()
    {
        return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
                .AddKeyword("game")
                .SetGender(Gender.Male)
                .SetBirthday(new DateTime(1985, 1, 1))
                .TagForChildDirectedTreatment(false)
                .AddExtra("color_bg", "9B30FF")
                .Build();

    }



    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        print("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        print("HandleAdOpened event received");
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
        print("HandleAdClosing event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        print("HandleAdLeftApplication event received");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        print("HandleInterstitialLoaded event received.");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        print("HandleInterstitialOpened event received");
    }

    public void HandleInterstitialClosing(object sender, EventArgs args)
    {
        print("HandleInterstitialClosing event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        print("HandleInterstitialLeftApplication event received");
    }

    #endregion
}

