using UnityEngine;
using System.Collections;

public class NPCAttribute 
{
	/// <summary>
	/// npc01
	/// </summary>
	public static float[] NpcBlood01 = new float[]{8,21,35,83,125,179,210} ;
	public static float[] NpcDamage01 = new float[]{12,16,22,28,36,41,45};
	/// <summary>
	/// npc02
	/// </summary>
	public static float[] NpcBlood02 = new float[]{8,21,35,83,125,179,210};
	public static float[] NpcDamage02 = new float[]{11,15,21,27,35,40,44};
	/// <summary>
	/// npc03
	/// </summary>
	public static float[] NpcBlood03 = new float[]{8,21,35,83,125,179,210};
	public static float[] NpcDamage03 = new float[]{11,14,20,26,34,39,43};
	/// <summary>
	/// npc04
	/// </summary>
	public static float[] NpcBlood04 = new float[]{16,42,69,165,249,357,420};
	public static float[] NpcDamage04 = new float[]{12,16,22,28,36,41,45};
	/// <summary>
	/// npcTNT
	/// </summary>
	public static float[] NpcBlood05 = new float[]{4,11,17,41,62,89,105};
	/// <summary>
	/// Boss
	/// </summary>
	public static float[] BossBlood01 = new float[]{0,100000} ;
	public static float[] BossDamage01 = new float[]{0,80} ;
}
