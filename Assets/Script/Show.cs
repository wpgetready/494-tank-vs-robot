using UnityEngine;
using System.Collections;


public class Show : IgnoreTimeScale
{
	public Attribute[]    attribute ;
	public Transform weapons ;
	public Transform armors ;
	private Transform thisTransform ;
	private int WeaponsIndex ;
	private int ArmorsIndex ;
	void Start () 
	{
		thisTransform = transform ;
	}
	void Update () 
	{
		thisTransform.Rotate(0,UpdateRealTimeDelta()*40,0) ;
	}
	public void WeaponsChange(int level=-1,bool assign=false)
	{
		if(assign==false)
		{
			//level = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel ;
			level = UserData.Instance.player_attribute.weaponsLevel;
		}
		if(level>=12)
		{
			WeaponsIndex = 3 ;
		}
		else if(level>=8)
		{
			WeaponsIndex = 2 ;
			
		}
		else if(level>=4)
		{
			WeaponsIndex = 1 ;
		}
		else if(level>=0)
		{
			WeaponsIndex = 0 ;
		}
		ChangeWeapons() ;
	}
	public void ArmorsChange(int level=-1,bool assign=false)
	{
		if(assign==false)
		{
			//level = PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel ;
			level = UserData.Instance.player_attribute.armorLevel;
		}
		if(level>=12)
		{
			armors.localScale = new Vector3(1f,1f,1f) ;
			weapons.localScale = new Vector3(1f,1f,1f) ;
			ArmorsIndex = 3 ;
		}
		else if(level>=8)
		{
			armors.localScale = new Vector3(1f,1f,1f) ;
			weapons.localScale = new Vector3(1f,1f,1f) ;
			ArmorsIndex = 2 ;
		}
		else if(level>=4)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 1 ;
		}
		else if(level>=0)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 0 ;
		}
		ChangeArmor() ;
	}
	void ChangeArmor()
	{
		for(int i=0;i<attribute.Length;i++)
		{
			for(int j=0;j<attribute[i].armors.Length;j++)
				attribute[i].armors[j].enabled = false ;
		}
		for(int j=0;j<attribute[ArmorsIndex].armors.Length;j++)
		{
			attribute[ArmorsIndex].armors[j].enabled = true ;
		}
	}
	void ChangeWeapons()
	{
		for(int i=0;i<attribute.Length;i++)
		{
			for(int j=0;j<attribute[i].weapons.Length;j++)
				attribute[i].weapons[j].enabled = false ;
		}
		for(int j=0;j<attribute[WeaponsIndex].weapons.Length;j++)
		{
			attribute[WeaponsIndex].weapons[j].enabled = true ;
		}
			
	}
}
