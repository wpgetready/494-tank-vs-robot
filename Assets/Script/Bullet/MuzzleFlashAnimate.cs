using UnityEngine;
using System.Collections;

public class MuzzleFlashAnimate : MonoBehaviour 
{
	public int         tankType ;
	public Renderer    fireRender ;
	public string       rotationAxis = "z" ;
	private Transform _thisTransform ;
	void Start () 
	{
		_thisTransform = this.transform ;
	}
	void Update ()
	{
		if(UIManager.gameState!= UIManager.GameState.run||!fireRender.enabled)
		{
			return ;
		}
		if(tankType==1)
		{
			_thisTransform.LookAt(Camera.mainCamera.transform) ;
		}
		_thisTransform.localScale = Vector3.one * Random.Range(0.5f,1.5f) ;
		if(rotationAxis == "z")
		{
			_thisTransform.localEulerAngles = new Vector3(_thisTransform.localEulerAngles.x,_thisTransform.localEulerAngles.y, Random.Range(0,90.0f));
		}
		else if(rotationAxis == "x")
		{
			_thisTransform.localEulerAngles = new Vector3(Random.Range(0,90.0f),_thisTransform.localEulerAngles.y,_thisTransform.localEulerAngles.z);
		}
	}
}
