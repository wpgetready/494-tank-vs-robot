using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	
	public int        Index ;
	public float      speed = 20 ;
	public float      destroyTime = 0.5f ;
	public bool      isPlayer ;
	public AudioClip  hitTheEnemy ;
	public GameObject selfDestruction ;  /* 导弹自己爆炸 */
	public GameObject spark ;                     /*　击中物体的效果　*/
	
	public int        type ;
	private Transform _thisTransform ;
	private float     _destroyTime ;
	public MoveWay   way ;
	private MeshRenderer   myRender ;
	private float      _currentDamage ;
	
	public void setDamage(float damage)
	{
		_currentDamage = damage;
	}
	
	void Start () 
	{
		_thisTransform = this.transform ;
		myRender =  _thisTransform.GetComponentInChildren<MeshRenderer>() ;
		 Vector2 size = new Vector2 (1/4.0f, 1/8.0f);
   		 int uIndex = Index % 4;
   		 int vIndex = Index / 4;
  		 Vector2 offset = new Vector2 (uIndex * size.x, 1.0f - size.y - vIndex * size.y);
		 myRender.material.SetTextureOffset ("_MainTex", offset);
   		 myRender.material.SetTextureScale ("_MainTex",size);
	}
	void OnEnable()
	{
		_destroyTime = Time.time + destroyTime ;
	}
	
	void Update () 
	{
		if(!isPlayer)
		{
			if(NPCManager.destroyNPCBullet)
			{
				if(selfDestruction!=null)
					Instantiate(selfDestruction,_thisTransform.position,_thisTransform.rotation);
				ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			}
		}
		if(UIManager.gameState != UIManager.GameState.run)
		{
			return ;
		}
		if(way == MoveWay.line)
		{
			_thisTransform.Translate(Vector3.forward*speed*Time.deltaTime) ;
		}
		else if(way == MoveWay.lookAtPlayer)
		{
			if(PlayerManager.playerTransform!=null)
			{
				Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*2.0f) ;
			}
			_thisTransform.Translate(Vector3.forward*speed*Time.deltaTime) ;
		}
		else if(way == MoveWay.lookAtEnemy)
		{
			if(PlayerManager.playerTarget!=null)
			{
				Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTarget.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*2.0f) ;
			}
			_thisTransform.Translate(Vector3.forward*speed*Time.deltaTime) ;
		}
		Vector3 pos = _thisTransform.position ;
		if(Time.time > _destroyTime||pos.x>130||pos.x<-130||pos.z>90||pos.z<-100)
		{
			if(selfDestruction!=null)
				Instantiate(selfDestruction,_thisTransform.position,_thisTransform.rotation);
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
		}	
	}
	void OnTriggerEnter(Collider other )
	{
		if(isPlayer)
		{
			if(other.tag == "Enemy")
			{
				UIManager.PlayMusic(hitTheEnemy) ;
				if(spark!=null)
					Instantiate(spark,_thisTransform.position,_thisTransform.rotation);
				if(UIManager.firePowerProp>0)
				{
					//_currentDamage = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[10] ;
					//如果使用火力道具,则在当前伤害基础上增加一个加成，见MoneyProps.xml
					_currentDamage = UserData.Instance.player_attribute.weapons * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("FirePower")).ratio;
				}
				else
				{
					//_currentDamage = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel] ;
					_currentDamage = UserData.Instance.player_attribute.weapons;
				}
				if(!UIManager.bossInvincible)
					other.gameObject.SendMessage("ApplyPlayDamage",_currentDamage,SendMessageOptions.DontRequireReceiver) ;
				ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			}
		}
		else
		{
			if(other.tag == "PC")
			{
				ObjectBuffer.DestroyBufferObject(this.gameObject) ;
				if(spark!=null)
					Instantiate(spark,_thisTransform.position,_thisTransform.rotation);
				
				//Debug.Log("_currentDamage:"+_currentDamage);
				//Debug.Log("player armor:"+PlayerManager.playerAttribute[PlayerManager.currentTank].armorPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].ArmorLevel]);
				//
				//float damage = _currentDamage-PlayerManager.playerAttribute[PlayerManager.currentTank].armorPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel];
				float damage = _currentDamage - UserData.Instance.player_attribute.armor;
				PlayerManager.ApplyNPCDamage(damage);
			}
		}
	}
	public enum MoveWay
	{
		line,
		lookAtPlayer,
		lookAtEnemy,
	}
}
