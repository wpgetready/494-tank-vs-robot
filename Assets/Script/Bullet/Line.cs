using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour 
{
	public Transform[] point ;
	public bool      moveOver ;
	
	private Vector3[] newVertices = new Vector3[4];
	private Vector2[] newUV = new Vector2[]{new Vector2(0,0),new Vector2(1,0),new Vector2(0,1),new Vector2(1,1)};
	private int[] newTriangles = new int[]{2,0,3,3,0,1} ;
	private MeshFilter thisMeshFilter ;
	private Transform  point1,point2 ;
	private string moveState = "move1" ;
	private float a = 1 ;
	private MeshRenderer  thisMeshRender ;
	void Start ()
	{
		point1 = new GameObject("point1").transform ;
		point2 = new GameObject("point2").transform ;
		point1.position = point[0].position+new Vector3(0,2,0) ;
		point2.position = point[0].position+new Vector3(0,2,0) ;
		thisMeshFilter = GetComponent<MeshFilter>();
		thisMeshFilter.mesh.uv = newUV;
		thisMeshFilter.mesh.triangles = newTriangles;
		thisMeshRender = GetComponent<MeshRenderer>();
	}
	void Update ()
	{
		if(point[1]==null||point[0]==null)
		{
			DestroyThis() ;
			return ;
		}
		if(point[1].gameObject.active==false||point[0].gameObject.active==false)
		{
			DestroyThis() ;
			return ;
		}
		switch(moveState)
		{
		case "move1":
			point1.position = point[0].position ;
			point2.LookAt(point[1].position) ;
			point2.Translate(Vector3.forward*Time.deltaTime*180) ;
			if(Vector3.Distance(point2.position,point[1].position)<=3)
			{
				moveOver = true ;
				moveState = "move2" ;
			}
			break;
		case "move2":
			point1.position = point[0].position ;
			point2.position = point[1].position ;
			a -= Time.deltaTime*1.5f ;
			if(a<=0)
			{
				a = 0 ;
				DestroyThis() ;
			}
			thisMeshRender.material.color = new Color(1,1,1,a) ;
			break;
		}
		Offset() ;
		ChangeMesh(Vector3.Cross((point2.position-point1.position).normalized,Vector3.up));
	}
	void ChangeMesh(Vector3 vec)
	{
		newVertices[0] = point[0].position-vec ;
		newVertices[1] = point[0].position+vec ;
		newVertices[2] = point[1].position-vec ;
		newVertices[3] = point[1].position+vec ;
		thisMeshFilter.mesh.vertices = newVertices;
	}
	void DestroyThis()
	{
		Destroy(gameObject);
		Destroy(point1.gameObject) ;
		Destroy(point2.gameObject) ;
	}
	private int Index = 0;
	private float t;
	void Offset()
	{
		t += Time.deltaTime;
		if(t > 0.1f)
		{
			Index += 1;
			if( Index >= 6 )
			{
				Index = 0 ;
			}
			Vector2 size = new Vector2 (1.0f, 1/6.0f);
   		 int uIndex = Index % 1;
   		 int vIndex = Index / 1;
  		 Vector2 offset = new Vector2 (uIndex * size.x, 1.0f - size.y - vIndex * size.y);
		 thisMeshRender.material.SetTextureOffset ("_MainTex", offset);
   		 thisMeshRender.material.SetTextureScale ("_MainTex",size);
		}
		
	}
}
