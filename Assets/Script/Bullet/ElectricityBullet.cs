using UnityEngine;
using System.Collections;

public class ElectricityBullet : MonoBehaviour
{
	public float       damage = 3 ;
	public int        Index ;
	public GameObject linePrefab ;
	public MeshRenderer  bulletMesh ;
	public GameObject selfDestruction ;
	private Line line ;
	private float     range = 90 ;
	private Transform _thisTransform ;
	private GameObject[]  HitEnemy = new GameObject[5] ;
	public Collider[]  aroundEnemy ;
	private GameObject  preEnemy ;
	private GameObject  currentEnemy ;
	private int         hitNum ;
	private float     _destroyTime ;
	private MeshRenderer   myRender ;
	private float      _currentDamage ;
	void Start () 
	{
		_thisTransform = this.transform ;
		myRender =  _thisTransform.GetComponentInChildren<MeshRenderer>() ;
		 Vector2 size = new Vector2 (1/4.0f, 1/8.0f);
   		 int uIndex = Index % 4;
   		 int vIndex = Index / 4;
  		 Vector2 offset = new Vector2 (uIndex * size.x, 1.0f - size.y - vIndex * size.y);
		 myRender.material.SetTextureOffset ("_MainTex", offset);
   		 myRender.material.SetTextureScale ("_MainTex",size);
	}
	void OnEnable()
	{
		_destroyTime = Time.time + 5 ;
		bulletMesh.enabled = true ;
		hitNum = 0 ;
	}
	void Update () 
	{
		if(bulletMesh.enabled)
		{
			_thisTransform.Translate(Vector3.forward*100*Time.deltaTime) ;
			Vector3 pos = _thisTransform.position ;
			if(Time.time > _destroyTime||pos.x>130||pos.x<-130||pos.z>90||pos.z<-100)
			{
				if(selfDestruction!=null)
					Instantiate(selfDestruction,_thisTransform.position,_thisTransform.rotation);
				ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			}
		}
		else
		{
//			if(line!=null)
//			{
//				if(line.moveOver)
//				{
//					line.moveOver = false ;
//					AddHitEnemy(currentEnemy) ;
//				}
//			}
		}
	}
	void OnTriggerEnter(Collider other )
	{
		if(other.tag == "Enemy"&&bulletMesh.enabled)
		{
			bulletMesh.enabled = false ;
			//AddHitEnemy(other.gameObject) ;
			if(UIManager.firePowerProp>0)
			{
				//_currentDamage = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[10] ;
				//如果使用火力道具,则在当前伤害基础上增加一个加成，见MoneyProps.xml
				//int item_id = 3;
				_currentDamage = UserData.Instance.player_attribute.weapons * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("FirePower")).ratio;

			
			}
			else
			{
				//_currentDamage = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel] ;
				_currentDamage = UserData.Instance.player_attribute.weapons;
			}
			if(!UIManager.bossInvincible)
				other.gameObject.SendMessage("ApplyPlayDamage",_currentDamage,SendMessageOptions.DontRequireReceiver) ;
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			
		}
	}
	void AddHitEnemy(GameObject obj)
	{
		HitEnemy[hitNum] = obj ;
		preEnemy = obj ;
		aroundEnemy = Physics.OverlapSphere(preEnemy.transform.position,range,1<<15) ;
		Debug.Log("electricityBullet AddHitEnemy ");
		if(UIManager.firePowerProp>0)
		{
			//_currentDamage = damage + damage*PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[10] ;
			//int item_id = 3;
			_currentDamage = damage + damage* UserData.Instance.player_attribute.weapons * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("FirePower")).ratio;
			_currentDamage = _currentDamage-_currentDamage*hitNum*0.2f ;
		}
		else
		{
			//_currentDamage = damage + damage*PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel] ;
			_currentDamage = damage + damage* UserData.Instance.player_attribute.weapons;
			_currentDamage = _currentDamage-_currentDamage*hitNum*0.2f ;
		}
		obj.SendMessage("ApplyPlayDamage",_currentDamage,SendMessageOptions.DontRequireReceiver) ;
		hitNum ++ ;
		if(hitNum>=4)
		{
			hitNum = 4 ;
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
		}
		GetEnemy() ;
	}
	void GetEnemy()
	{
		for(int i=0;i<aroundEnemy.Length;i++)
		{
			if(CheckEnemy(aroundEnemy[i].gameObject))
			{
				GameObject obj = Instantiate(linePrefab,Vector3.zero,Quaternion.identity) as GameObject ;
				line = obj.GetComponent<Line>() ;
				line.point[0] = preEnemy.transform ;
				currentEnemy = aroundEnemy[i].gameObject ;
				line.point[1] = currentEnemy.transform ;
				return ;
			}
		}
		ObjectBuffer.DestroyBufferObject(this.gameObject) ;
	}
	bool CheckEnemy(GameObject obj)
	{
		for(int i=0;i<hitNum;i++)
		{
			if(obj == HitEnemy[i])
			{
				return false ;
			}
		}
		return true ;
	}
}
