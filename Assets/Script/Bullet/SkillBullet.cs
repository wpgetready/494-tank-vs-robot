using UnityEngine;
using System.Collections;

public class SkillBullet : MonoBehaviour 
{
	public GameObject Explosion ;
	public Transform  target ;
	private Transform _thisTransform ;
	void Start () 
	{
		if(_thisTransform==null)
			_thisTransform = this.transform ;
	}
	void OnEnable()
	{
		if(_thisTransform==null)
			_thisTransform = this.transform ;
		_thisTransform.eulerAngles = new Vector3(90,0,0) ;
	}
	void OnDisable()
	{
		target = null ;
	}
	void Update ()
	{
		if(UIManager.gameState != UIManager.GameState.run)
		{
			return ;
		}
		if(target!=null)
			_thisTransform.LookAt(target.position) ;
		_thisTransform.Translate(Vector3.forward*100*Time.deltaTime) ;
		if(_thisTransform.position.y<0)
		{
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			Instantiate(Explosion,_thisTransform.position+new Vector3(0,8,0),_thisTransform.rotation) ;
		}
	}
	void OnTriggerEnter(Collider other )
	{
		if(other.tag == "Enemy")
		{
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
			Instantiate(Explosion,_thisTransform.position,_thisTransform.rotation) ;
			other.gameObject.SendMessage("ApplyPlayDamage",200,SendMessageOptions.DontRequireReceiver) ;
		}
	}
}
