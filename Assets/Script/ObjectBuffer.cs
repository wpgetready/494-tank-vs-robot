using UnityEngine;
using System.Collections;

[System.Serializable]
public class Buffer 
{
	public GameObject prefab ;
	public int        bufferSize ;
	private GameObject[] objects ;
	private int          index ;
	private GameObject   obj ;
	public void Init()
	{
		objects = new GameObject[bufferSize] ;
		for(int i=0;i<bufferSize;i++)
		{
			objects[i] = MonoBehaviour.Instantiate(prefab) as GameObject ;
			objects[i].SetActiveRecursively(false) ;
			
		}
	}
	public GameObject GetGameObject()
	{
		for(int i=0;i<bufferSize;i++)
		{
			obj = objects[index] ;
			if(!obj.active)
				break ;
			index = (index+1)%bufferSize ;
		}
		if(obj.active)
		{
			obj = MonoBehaviour.Instantiate(prefab) as GameObject ;
			obj.SetActiveRecursively(false) ;
			obj.name = "CanDestroy" ;
			return obj ;
		}
		index = (index+1)%bufferSize ;
		return obj ;
	}
}
public class ObjectBuffer : MonoBehaviour
{
	private static ObjectBuffer objectBuffer ;
	public Buffer[] buffer ;
	void Awake ()
	{
		objectBuffer = this ;
		for(int i=0;i<buffer.Length;i++)
		{
			buffer[i].Init() ;
		}
	}
	public static GameObject InstantiateObject(int objectIndex,Vector3 pos,Quaternion mRotation)
	{
		GameObject obj = objectBuffer.buffer[objectIndex].GetGameObject() ;
		obj.transform.position = pos ;
		obj.transform.rotation = mRotation ;
		obj.SetActiveRecursively(true) ;
		return obj ;
	}
	public static void DestroyBufferObject(GameObject obj)
	{
		if(obj.name=="CanDestroy")
			Destroy(obj) ;
		else
			obj.SetActiveRecursively(false);
		
	}
}
