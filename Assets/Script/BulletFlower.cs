using UnityEngine;
using System.Collections;

[System.Serializable]
public class Parameter
{
	public int  fireType ;
	public int  rotateType ;
	
	public int  bulletPrefab ;
	public Transform[] bulletPos ;
	
	public float       fireTime ;
	public float       intervalTime ;
	public float       singleIntervalTime ; /* 单发间隔时间 */
	public int         shootNum ;
	
	public float     gunRotateSpeed = 80 ;
	public Transform gunCarriage ;
}
public class BulletFlower : MonoBehaviour 
{
	public Parameter[] parameter ;
	public int         paraIndex ;
	private bool       _isShoot = true ;
	private Transform  _target ;
	private GameObject  _thisObj ;
	void Start ()
	{
		//Debug.Log("bulletflower");
		_target = PlayerManager.playerTransform ;
		_thisObj = this.gameObject ;
	}
	void OnEnable()
	{
		_isShoot = true ;
	}
	void Update()
	{
		if(parameter[paraIndex].fireType==0)
		{
			if(_isShoot)
			{
				_isShoot = false ;
				StartCoroutine(Shoot1()) ;
			}
		}
		else if(parameter[paraIndex].fireType==1)
		{
			if(_isShoot)
			{
				_isShoot = false ;
				StartCoroutine(Shoot2()) ;
			}
		}
		
		if(parameter[paraIndex].rotateType==0)
		{
			GunCarriageRotate1() ;
		}
		else if(parameter[paraIndex].rotateType==1)
		{
			GunCarriageRotate2() ;
		}
	}
	void GunCarriageRotate1()
	{
		if(_target != null)
		{
			Quaternion rotate = Quaternion.LookRotation(_target.position-parameter[paraIndex].gunCarriage.position) ;
			parameter[paraIndex].gunCarriage.rotation = Quaternion.Slerp(parameter[paraIndex].gunCarriage.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*2.0f) ;
		}
	}
	void GunCarriageRotate2()
	{
		parameter[paraIndex].gunCarriage.Rotate(Vector3.up*Time.deltaTime*parameter[paraIndex].gunRotateSpeed) ;
	}
	IEnumerator Shoot1()
	{
		yield return new WaitForSeconds(parameter[paraIndex].fireTime) ;
		for(int i=0;i<parameter[paraIndex].bulletPos.Length;i++)
		{
			if(_thisObj.active)
			{
				//Debug.Log("in BulletFlower Shoot1 parameter[paraIndex].bulletPrefab :"+parameter[paraIndex].bulletPrefab);
				GameObject objBullet = ObjectBuffer.InstantiateObject(parameter[paraIndex].bulletPrefab,parameter[paraIndex].bulletPos[i].position,parameter[paraIndex].bulletPos[i].rotation) ;
				NPC npc = _thisObj.GetComponent<NPC>();
				//Debug.Log("npc info damage:"+npc.damage);
				objBullet.GetComponent<Bullet>().setDamage(npc.damage);
			}
		}
		yield return new WaitForSeconds(parameter[paraIndex].intervalTime) ;
		_isShoot = true ;
	}
	IEnumerator Shoot2()
	{
		yield return new WaitForSeconds(parameter[paraIndex].fireTime) ;
		for(int i=0;i<parameter[paraIndex].shootNum;i++)
		{
			for(int j=0;j<parameter[paraIndex].bulletPos.Length;j++)
			{
				if(_thisObj.active)
				{
					//Debug.Log("in BulletFlower Shoot2 parameter[paraIndex].bulletPrefab :"+parameter[paraIndex].bulletPrefab);
					GameObject objBullet = ObjectBuffer.InstantiateObject(parameter[paraIndex].bulletPrefab,parameter[paraIndex].bulletPos[j].position,parameter[paraIndex].bulletPos[j].rotation) ;
					NPC npc = _thisObj.GetComponent<NPC>();
					//Debug.Log("npc info damage:"+npc.damage);		
					objBullet.GetComponent<Bullet>().setDamage(npc.damage);
				}
			}
			yield return new WaitForSeconds(parameter[paraIndex].singleIntervalTime) ;
		}
		yield return new WaitForSeconds(parameter[paraIndex].intervalTime) ;
		_isShoot = true ;
	}
}
