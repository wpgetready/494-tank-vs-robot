using UnityEngine;
using System.Collections;

public class PlayerAttribute {
	
	public int propsAdsorptionLevel;
	public int moveSpeedLevel;
	public int turretSpeedLevel;
	public int armorLevel;
	public int weaponsLevel;
	public int attackSpeedLevel;
	
	public float propsAdsorption;
	public float moveSpeed;
	public float turretSpeed;
	public float armor;
	public float weapons;
	public float attackSpeed;
	
	/*
	public float[]  propsAdsorptionDis = new float[]{0,10,16,22,28,34,40,46,52,60,70}  ;// 金币吸附距离
	public float[]  walkingSpeed       = new float[]{20f,23f,26f,29f,32f,35f,38f,41f,44f,47f,50f} ;// 行走速度 
	public float[]  turretSpeed        = new float[]{4f,4.5f,5f,5.5f,6,6.5f,7,7.5f,8,8.5f,10} ;// 炮管旋转速度 
	public float[]  armorPercentage    = new float[]{10f,11f,13f,15f,17f,20f,23f,27f,31f,35f,40f} ;
	public float[]  weaponsPercentage    = new float[]{10f,12f,14f,18f,22f,28f,34f,42f,50f,60f,70f} ;
	public float[]  attackSpeed        = new float[]{0.30f,0.289f,0.276f,0.261f,0.244f,0.225f,0.204f,0.181f,0.156f,0.129f,0.10f} ;// 攻击速度
	*/
	/*
	private static PlayerAttribute instance = new PlayerAttribute();
	
	public static PlayerAttribute Instance
	{
		get
		{
			return instance;
		}
	}
	*/
	
	public PlayerAttribute()
	{
		propsAdsorptionLevel = 0;
		moveSpeedLevel = 0;
		turretSpeedLevel = 0;
		armorLevel = 0;
		weaponsLevel = 0;
		attackSpeedLevel = 0;		
	}
	
	public void RefreshAttributes()
	{
		int max_level = 15;
		if(isAllReachMaxLevel(max_level))
		{
			foreach(DictionaryEntry item in GoldPropConfig.Instance.gold_prop_list)
			{
				GoldProp gp = item.Value as GoldProp;
				int prop_id = gp.prop_id;//获得道具id
				float max_ratio = gp.max_ratio;//
				//setAttributeByPropId(i,max_level,((GoldProp)(GoldPropConfig.Instance.gold_prop_list[i])).max_ratio);
				setAttributeByPropId(prop_id,max_level,max_ratio);
			}
		}
		else
		{
			//首先遍历GoldProps.xml中的所有道具
			foreach(DictionaryEntry item in GoldPropConfig.Instance.gold_prop_list)
			{
				GoldProp gp = item.Value as GoldProp;
				int prop_id = gp.prop_id;//获得道具id
				int level = getAttributeLevelByPropId(gp.prop_id);//获得该道具所对应的在PlayerAttribute中的等级
				PropLevel pl = (PropLevel)GoldPropConfig.Instance.GetGoldPropByPropAndLevel(prop_id,level);//根据道具id及PlayerAttribute中的等级获得Player的所有Attribute的数值ratio
				setAttributeByPropId(prop_id,level,pl.ratio);//再将该数值存入PlayerAttribute中
				//Debug.Log(pl.ratio);
				//注意：这里的获取和赋值都通过函数，因为每个prop_id对应PlayerAttribute中的不同参数,所以需要通过函数转换一下
			}
		}
	}
	
	public void setAttributeByPropId(int prop_id,int level,float attr_value)
	{
		switch(prop_id)
		{
		case 11:
			propsAdsorptionLevel = level;
			propsAdsorption = attr_value;
			break;
		case 12:
			moveSpeedLevel = level;
			moveSpeed = attr_value;
			break;
		case 13:
			turretSpeedLevel = level;
			turretSpeed = attr_value;
			break;
		case 14:
			armorLevel = level;
			armor = attr_value;
			break;
		case 15:
			weaponsLevel = level;
			weapons = attr_value;
			break;
		case 16:
			attackSpeedLevel = level;
			attackSpeed = attr_value;
			break;
		}
	}
	
	public float getAttributeValueByPropId(int prop_id)
	{
		switch(prop_id)
		{
		case 11:
			return propsAdsorption;
		case 12:
			return moveSpeed;
		case 13:
			return turretSpeed;
		case 14:
			return armor;
		case 15:
			return weapons;
		case 16:
			return attackSpeed;
		default:
			return -1;
		}
	}
	
	public int getAttributeLevelByPropId(int prop_id)
	{
		switch(prop_id)
		{
		case 11:
			return propsAdsorptionLevel;
		case 12:
			return moveSpeedLevel;
		case 13:
			return turretSpeedLevel;
		case 14:
			return armorLevel;
		case 15:
			return weaponsLevel;
		case 16:
			return attackSpeedLevel;
		default:
			return -1;
		}
	}
	
	public bool isAllReachMaxLevel(int max_level)
	{
		for(int i=11;i<=16;i++)
		{
			if(getAttributeLevelByPropId(i) < max_level)
			{
				return false;
			}
		}
		return true;
	}
	
	public int minReachedLevel()
	{
		int min_level = 15;
		for(int i=11;i<=16;i++)
		{
			if(min_level > getAttributeLevelByPropId(i))
			{
				min_level = getAttributeLevelByPropId(i);
			}
		}
		return min_level;
	}

}