using UnityEngine;
using System.Collections;

[System.Serializable]
public class Attribute
{
	public MeshRenderer[] weapons ;
	public MeshRenderer[] armors ;
	//public GameObject  boxCollider ;
	public Animation[]      speedEffectAnima ;
	public MeshRenderer[] speedEffectRender ;
	public MeshRenderer weaponsFireRender ;
	public MeshRenderer armorsFireRender ;
	public int          bullet ;
	public Transform[]  bulletPos ;
	public TweenPosition[] gunTP ;
	public MuzzleFlashAnimate[] muzzle ;
}
public class Weapons : MonoBehaviour 
{
	public Attribute[]    attribute ;
	public Transform weapons ;
	public Transform armors ;
	public Transform[]  bulletPos ;
	public AudioClip[]   bulletClip ;
	public int      guide ;
	public int      bomb ;
	private float bombTime ;
	private bool startShoot = true ;
	private AudioClip    _currentClip ;
	private Transform  _thisTransform ;
	public MaterialFlash _flash ;
	void Start ()
	{
		_thisTransform = this.transform ;
		_flash = (MaterialFlash)_thisTransform.GetComponent<MaterialFlash>() ;
		WeaponsChange();
		ArmorsChange();
		AttackSpeedChange() ;
	}
	void OnEnable()
	{
		startShoot = true ;
	}
	void Update ()
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			AutoOrientation() ;
			Shoot() ;
			Shield() ;
			GoldAdsorb() ;
			FireProp() ;
			SpeedProp() ;
			/*
			 * 在级数较高情况下会多出1号坦克的原因了，是遗留代码会在护甲大于9级（最大10级，后来为12级了）的时候，会随机生成炸弹，
			 * 而在忘了给bomb赋值，导致bomb默认为0，生成了1号坦克，暂时屏蔽此处代码
			 * */
			/*
			if(UserData.Instance.player_attribute.armorLevel>=12)
			{
				if(Time.time >= bombTime)
				{
					bombTime = Time.time + 30 ;
					ObjectBuffer.InstantiateObject(bomb,_thisTransform.position,_thisTransform.rotation) ;
				}
			}
			else if(UserData.Instance.player_attribute.armorLevel>=8)
			{
				if(Time.time >= bombTime)
				{
					bombTime = Time.time + 50 ;
					ObjectBuffer.InstantiateObject(bomb,_thisTransform.position,_thisTransform.rotation) ;
				}
			}
			*/
		}
		else
		{
			if(audio.isPlaying)
				audio.Pause() ;
		}
	}
	void Shoot()
	{
		if(PlayerManager.playerTarget==null)
		{
			audio.Stop() ;
			return ;
		}
		if(!audio.isPlaying)
		{
			audio.volume = UserData.Instance.user_setting.sound_volume*0.5f ;
			audio.Play() ;
		}
		if(UIManager.firePowerProp>0)
		{
			if(audio.clip != bulletClip[3])
				audio.clip = bulletClip[3] ;
		}
		else
		{
			if(audio.clip != _currentClip)
				audio.clip = _currentClip ;
		}
		//if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=9)
		if(UserData.Instance.player_attribute.weaponsLevel>=12)
		{
			SendGuided() ;
		}
		if(startShoot)
		{
			startShoot = false ;
			if(UIManager.firePowerProp>0)
			{
				//如果使用火力道具,则在当前伤害和速度基础上增加一个加成，见MoneyProps.xml
				//float attackSpeed = PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeed * ((MoneyProp)MoneyPropConfig.Instance.money_prop_list[3]).ratio;
				//这里其实错误了,因为使用火力道具时候攻击速度也就是两次发射间隔时间被放大了,而两次间隔时间越短才是威力越大,先保留着,以后再改
				//int item_id = 3;
				float attackSpeed = UserData.Instance.player_attribute.attackSpeed * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("FirePower")).ratio;
				StartCoroutine(Shoot1(attackSpeed,attribute[WeaponsIndex].bullet,WeaponsIndex)) ;
				//StartCoroutine(Shoot1(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeed[10],attribute[WeaponsIndex].bullet,WeaponsIndex)) ;
			}
			else
			{
				float attackSpeed = UserData.Instance.player_attribute.attackSpeed;
				StartCoroutine(Shoot1(attackSpeed,attribute[WeaponsIndex].bullet,WeaponsIndex)) ;
				//StartCoroutine(Shoot1(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeed[PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel],attribute[WeaponsIndex].bullet,WeaponsIndex)) ;
			}
		}
		
	}
	IEnumerator Shoot1(float fireinterval,int bullet,int index)
	{
		yield return new WaitForSeconds(0) ;
		for(int i=0;i<attribute[index].gunTP.Length;i++)
		{
			attribute[index].gunTP[i].enabled = true ;
			//Debug.Log("attribute[index].gunTP[i].enabled:"+index+" "+i);
		}
		for(int i=0;i<attribute[index].muzzle.Length;i++)
		{
			attribute[index].muzzle[i].fireRender.enabled = true ;
			//Debug.Log("attribute[index].muzzle[i].fireRender.enabled:"+index+" "+i);
		}
		for(int i=0;i<5;i++)
		{
			if(WeaponsIndex == index)
			{
				for(int j=0;j<attribute[index].bulletPos.Length;j++)
				{
					ObjectBuffer.InstantiateObject(bullet,attribute[index].bulletPos[j].position,attribute[index].bulletPos[j].rotation) ;
					//Debug.Log("i:"+i+" index:"+index+" j:"+j);
				}
				yield return new WaitForSeconds(fireinterval) ;
			}
		}
		for(int i=0;i<attribute[index].gunTP.Length;i++)
			attribute[index].gunTP[i].enabled = false ;
		for(int i=0;i<attribute[index].muzzle.Length;i++)
			attribute[index].muzzle[i].fireRender.enabled = false ;
		if(WeaponsIndex == index)
			yield return new WaitForSeconds(0.2f) ;
		startShoot = true ;
	}
	/// <summary>
	/// 大招.
	/// </summary>
	public Transform[] skillPos ;
	private Vector3[] pos = new Vector3[8] ;
	public void Skill()
	{
		StartCoroutine(SendSkill()) ;
	}
	IEnumerator SendSkill()
	{
		NPCManager.destroyNPCBullet = true ;
		for(int i=0;i<8;i++)
		{
			pos[i] = skillPos[i].position ;
		}
		yield return new WaitForSeconds(0f) ;
		for(int i=0;i<8;i++)
		{
			ObjectBuffer.InstantiateObject(27, pos[i],Quaternion.identity) ;
			yield return new WaitForSeconds(0.15f) ;
		}
		for(int i=0;i<NPCManager.npcUpperLimit;i++)
		{
			if(NPCManager.SceneNpc[i]!=null)
			{
				if(NPCManager.SceneNpc[i].inScreen)
				{
					GameObject obj = ObjectBuffer.InstantiateObject(27,NPCManager.SceneNpc[i].transform.position+new Vector3(0,50,0),Quaternion.identity) ;
					obj.GetComponent<SkillBullet>().target = NPCManager.SceneNpc[i].transform ;
					if(NPCManager.SceneNpc[i].boss == 0)
					{
						yield return new WaitForSeconds(0.2f) ;
					}
				}
			}
		}
		/*
		if(NPCManager.BossObj!=null)
		{
			if(NPCManager.BossObj.active)
			{
				GameObject obj = ObjectBuffer.InstantiateObject(27,NPCManager.BossObj.transform.position+new Vector3(0,50,0),Quaternion.identity) ;
				obj.GetComponent<SkillBullet>().target = NPCManager.BossObj.transform ;
			}
		}
		*/
		NPCManager.destroyNPCBullet = false ;
	}
	/// <summary>
	/// 发射导弹.
	/// </summary>
	private float guidedTimel ;
	private float guidedTimer ;
	private void SendGuided()
	{
		if(Time.time >= guidedTimel)
		{
			guidedTimel = Time.time + 2f ;
			guidedTimer = Time.time + 1f ;
			UIManager.PlayMusic(bulletClip[4]) ;
			ObjectBuffer.InstantiateObject(guide,bulletPos[0].position,bulletPos[0].rotation) ;
		}
		if(Time.time >= guidedTimer)
		{
			guidedTimer = Time.time + 2f ;
			UIManager.PlayMusic(bulletClip[4]) ;
			ObjectBuffer.InstantiateObject(guide,bulletPos[1].position,bulletPos[1].rotation) ;
		}
	}
	/// <summary>
	/// 防护盾.
	/// </summary>
	public MeshRenderer shield ;
	private void Shield()
	{
		if(UIManager.shieldProp>0||UIManager.currentLevelInvincible)
		{
			if(!shield.enabled)
			{
				shield.enabled = true ;
			}
		}
		else
		{
			if(shield.enabled)
				shield.enabled = false ;
		}
	}
	/// <summary>
	/// 金币吸附.
	/// </summary>
	public Animation      magnetAnima ;
	public MeshRenderer[] magnetRender ;
	private void GoldAdsorb()
	{
		if(UIManager.magnetProp>0)
		{
			if(!magnetRender[0].enabled)
			{
				magnetAnima.Play("idle") ;
				for(int i=0;i<magnetRender.Length;i++)
				{
					magnetRender[i].enabled = true ;
				}
			}
		}
		else
		{
			if(magnetRender[0].enabled)
			{
				magnetAnima.Stop("idle") ;
				for(int i=0;i<magnetRender.Length;i++)
				{
					magnetRender[i].enabled = false ;
				}
			}
		}
	}
	/// <summary>
	/// 速度道具.
	/// </summary>
	private int      WeaponsIndex ;
	private int      ArmorsIndex ;
	private bool     playSpeedProp ;
	private void SpeedProp()
	{
		if(UIManager.speedProp>0)
		{
			if(playSpeedProp)
			{
				playSpeedProp = false ;
				for(int i=0;i<attribute[ArmorsIndex].speedEffectAnima.Length;i++)
					attribute[ArmorsIndex].speedEffectAnima[i].Play("idle") ;
				for(int i=0;i<attribute[ArmorsIndex].speedEffectRender.Length;i++)
					attribute[ArmorsIndex].speedEffectRender[i].enabled = true ;
			}
		}
		else
		{
			if(!playSpeedProp)
			{
				playSpeedProp = true ;
				for(int i=0;i<attribute[ArmorsIndex].speedEffectAnima.Length;i++)
					attribute[ArmorsIndex].speedEffectAnima[i].Stop("idle") ;
				for(int i=0;i<attribute[ArmorsIndex].speedEffectRender.Length;i++)
					attribute[ArmorsIndex].speedEffectRender[i].enabled = false ;
			}
		}
	}
	private void ChangeSpeedProp()
	{
		if(UIManager.speedProp>0)
		{
			for(int i=0;i<attribute.Length;i++)
			{
				if(i!=ArmorsIndex)
				{
					for(int z=0;z<attribute[ArmorsIndex].speedEffectAnima.Length;z++)
						attribute[ArmorsIndex].speedEffectAnima[z].Stop("idle") ;
					for(int j=0;j<attribute[i].speedEffectRender.Length;j++)
						attribute[i].speedEffectRender[j].enabled = false ;
				}
			}
			for(int z=0;z<attribute[ArmorsIndex].speedEffectAnima.Length;z++)
				attribute[ArmorsIndex].speedEffectAnima[z].Play("idle") ;
			for(int j=0;j<attribute[ArmorsIndex].speedEffectRender.Length;j++)
				attribute[ArmorsIndex].speedEffectRender[j].enabled = true ;
		}
	}
	/// <summary>
	/// 火力道具
	/// </summary>
	public Material fireMat ;
	private bool     playFireProp = true ;
	private void FireProp()
	{
		if(UIManager.firePowerProp>0)
		{
			 fireMat.SetTextureOffset ("_MainTex", new Vector2(Time.time*-1f,0));
			if(playFireProp)
			{
				playFireProp = false ;
				attribute[ArmorsIndex].armorsFireRender.enabled = true ;
				attribute[WeaponsIndex].weaponsFireRender.enabled = true ;
			}
		}
		else
		{
			if(!playFireProp)
			{
				playFireProp = true ;
				attribute[ArmorsIndex].armorsFireRender.enabled = false ;
				attribute[WeaponsIndex].weaponsFireRender.enabled = false ;
			}
		}
	}
	private void ChangeFireProp()
	{
		if(UIManager.firePowerProp>0)
		{
			for(int i=0;i<attribute.Length;i++)
			{
				if(i!=ArmorsIndex)
				{
					attribute[i].armorsFireRender.enabled = false ;
				}
				if(i!=WeaponsIndex)
				{
					attribute[i].weaponsFireRender.enabled = false ;
				}
			}
			attribute[ArmorsIndex].armorsFireRender.enabled = true ;
			attribute[WeaponsIndex].weaponsFireRender.enabled = true ;
		}
	}
	/// <summary>
	/// 自动瞄准.
	/// </summary>
	void AutoOrientation()
	{
		if(PlayerManager.playerTarget!=null)
		{
			Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTarget.position-weapons.position) ;
			if(UIManager.speedProp>0)
			{
				//weapons.rotation = Quaternion.Slerp(weapons.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeed[10]) ;
				//float t = Time.deltaTime*PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeed * ((MoneyProp)MoneyPropConfig.Instance.money_prop_list[6]).ratio;
		
				float t = Time.deltaTime*UserData.Instance.player_attribute.turretSpeed * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("Speed")).ratio;
				weapons.rotation = Quaternion.Slerp(weapons.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),t) ;
			}
			else
			{
				//weapons.rotation = Quaternion.Slerp(weapons.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeed[PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel]) ;
				//float t = Time.deltaTime*PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeed ;
				float t = Time.deltaTime*UserData.Instance.player_attribute.turretSpeed ;
				weapons.rotation = Quaternion.Slerp(weapons.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),t) ;
			}
		}
	}
	public void AttackSpeedChange()
	{
		//if(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel>=9)
		if(UserData.Instance.player_attribute.attackSpeedLevel>=12)
		{
			audio.clip = bulletClip[3] ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel>=6)
		else if(UserData.Instance.player_attribute.attackSpeedLevel>=8)
		{
			audio.clip = bulletClip[2] ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel>=3)
		else if(UserData.Instance.player_attribute.attackSpeedLevel>=4)
		{
			audio.clip = bulletClip[1] ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel>=0)
		else if(UserData.Instance.player_attribute.attackSpeedLevel>=0)
		{
			audio.clip = bulletClip[0] ;
		}
		_currentClip = audio.clip ;
	}
	public void WeaponsChange()
	{
		/*
		//if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=9)
		if(UserData.Instance.player_attribute.weaponsLevel>=12)
		{
			WeaponsIndex = 3 ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=6)
		else if(UserData.Instance.player_attribute.weaponsLevel>=8)
		{
			WeaponsIndex = 2 ;
			
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=3)
		else if(UserData.Instance.player_attribute.weaponsLevel>=4)
		{
			WeaponsIndex = 1 ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=0)
		else if(UserData.Instance.player_attribute.weaponsLevel>=0)
		{
			WeaponsIndex = 0 ;
		}
		*/
		if(UserData.Instance.player_attribute.minReachedLevel()>=12)
		{
			WeaponsIndex = 3 ;
		}
		else if(UserData.Instance.player_attribute.minReachedLevel()>=6)
		{
			WeaponsIndex = 1 ;
		}
		else if(UserData.Instance.player_attribute.minReachedLevel()>=0)
		{
			WeaponsIndex = 0 ;
		}

		ChangeFireProp() ;
		ChangeWeapons() ;
	}
	public void ArmorsChange()
	{
		/*
		//if(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel>=9)
		if(UserData.Instance.player_attribute.armorLevel>=12)
		{
			bombTime = Time.time + 30 ;
			armors.localScale = new Vector3(1f,1f,1f) ;
			weapons.localScale = new Vector3(1f,1f,1f) ;
			ArmorsIndex = 3 ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel>=6)
		else if(UserData.Instance.player_attribute.armorLevel>=8)
		{
			bombTime = Time.time + 50 ;
			armors.localScale = new Vector3(1f,1f,1f) ;
			weapons.localScale = new Vector3(1f,1f,1f) ;
			ArmorsIndex = 2 ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel>=3)
		else if(UserData.Instance.player_attribute.armorLevel>=4)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 1 ;
		}
		//else if(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel>=0)
		else if(UserData.Instance.player_attribute.armorLevel>=0)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 0 ;
		}
		*/
		if(UserData.Instance.player_attribute.minReachedLevel()>=12)
		{
			bombTime = Time.time + 30 ;
			armors.localScale = new Vector3(1f,1f,1f) ;
			weapons.localScale = new Vector3(1f,1f,1f) ;
			ArmorsIndex = 3 ;
		}
		else if(UserData.Instance.player_attribute.minReachedLevel()>=6)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 1 ;
		}
		else if(UserData.Instance.player_attribute.minReachedLevel()>=0)
		{
			armors.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			weapons.localScale = new Vector3(0.8f,0.8f,0.8f) ;
			ArmorsIndex = 0 ;
		}
		ChangeSpeedProp() ;
		ChangeFireProp() ;
		ChangeArmor() ;
	}
	void ChangeArmor()
	{
		for(int i=0;i<attribute.Length;i++)
		{
			if(i != ArmorsIndex)
			{
				for(int j=0;j<attribute[i].armors.Length;j++)
					attribute[i].armors[j].enabled = false ;
				//attribute[i].boxCollider.SetActiveRecursively(false) ;
			}
		}
		for(int j=0;j<attribute[ArmorsIndex].armors.Length;j++)
			attribute[ArmorsIndex].armors[j].enabled = true ;
	//	attribute[ArmorsIndex].boxCollider.SetActiveRecursively(true) ;
	}
	void ChangeWeapons()
	{
		for(int i=0;i<attribute.Length;i++)
		{
			if(i != WeaponsIndex)
			{
				for(int j=0;j<attribute[i].weapons.Length;j++)
					attribute[i].weapons[j].enabled = false ;
			}
		}
		for(int j=0;j<attribute[WeaponsIndex].weapons.Length;j++)
			attribute[WeaponsIndex].weapons[j].enabled = true ;
	}
}
