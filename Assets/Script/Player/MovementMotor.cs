using UnityEngine;
using System.Collections;

public class MovementMotor : MonoBehaviour 
{
	[HideInInspector]

	public Vector3 movementDirection  ;
	
	// Simpler motors might want to drive movement based on a target purely
	[HideInInspector]

	public Vector3 movementTarget  ;
	
	// The direction the character wants to face towards, in world space.
	[HideInInspector]

	public Vector3 facingDirection  ;
	
	[HideInInspector]

	public Vector3 turretDirection ;
	
}
