using UnityEngine;
using System.Collections;

public class TankMoveController : MonoBehaviour
{
	// Objects to drag in
	public MovementMotor motor  ;
	public Transform character  ;
	public GameObject cursorPrefab  ;
	public GameObject joystickPrefabl ;
	//public GameObject joystickPrefabr ;
	public GameObject joystickbgPrefabl ;
	//public GameObject joystickbgPrefabr ;
	//public var turret:Transform;
	// Settings
	public float cameraSmoothing   = 0.01f;
	public float cameraPreview  = 2.0f;
	
	// Cursor settings
	public float cursorPlaneHeight   = 0;
	public float cursorFacingCamera   = 0;
	public float cursorSmallerWithDistance   = 0;
	public float cursorSmallerWhenClose  = 1;
	
	// Private memeber data
	private Camera mainCamera  ;
	
	public Transform cursorObject  ;
	private Joystick joystickLeft  ;
	//private Joystick joystickRight  ;
	
	private Transform mainCameraTransform  ;
	private Vector3 cameraVelocity   = Vector3.zero;
//	private Vector3 cameraOffset   = Vector3.zero;
	private Vector3 initOffsetToPlayer  ;
	
	
	// Prepare a cursor point varibale. This is the mouse position on PC and controlled by the thumbstick on mobiles.
//	private Vector3 cursorScreenPosition  ;
	
//	private Plane playerMovementPlane  ;
	
	private GameObject joystickLeftGO ;
	private GameObject joystickLeftBG  ;
//	private GameObject joystickRightGO  ;
//	private GameObject joystickRightBG  ;
	
	private Quaternion screenMovementSpace  ;
	private Vector3 screenMovementForward  ;
	private Vector3 screenMovementRight  ;
	
	void Awake()
	{
		motor.movementDirection = Vector2.zero;
		motor.facingDirection = Vector2.zero;
		
		// Set main camera
		mainCamera = Camera.main;
		mainCameraTransform = mainCamera.transform;
		
		// Ensure we have character set
		// Default to using the transform this component is on
		if (!character)
			character = transform;
		
		initOffsetToPlayer = mainCameraTransform.position - character.position;
		
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
			if (joystickPrefabl) {
				// Create left joystick
				joystickLeftGO   = Instantiate (joystickPrefabl) as GameObject;
				joystickLeftGO.name = "Joystick Left";
				joystickLeft = joystickLeftGO.GetComponent<Joystick> ();
				joystickLeftBG = Instantiate (joystickbgPrefabl) as GameObject ;
				UIManager.Joystick[0] = joystickLeftGO ;
				UIManager.Joystick[1] = joystickLeftBG ;
				// Create right joystick
//				joystickRightGO = Instantiate (joystickPrefabr) as GameObject;
//				joystickRightGO.name = "Joystick Right";
//				joystickRight = joystickRightGO.GetComponent<Joystick> ();	
//				joystickRightBG = Instantiate (joystickbgPrefabr) as GameObject;
			}
		#elif !UNITY_FLASH
			if (cursorPrefab) {
				cursorObject = (Instantiate (cursorPrefab) as GameObject).transform;
			}
		#endif
		
		// Save camera offset so we can use it in the first frame
	//	cameraOffset = mainCameraTransform.position - character.position;
		
		// Set the initial cursor position to the center of the screen
     	//cursorScreenPosition = new Vector3 (0.5f * Screen.width, 0.5f * Screen.height, 0);
		
		// caching movement plane
		//playerMovementPlane = new Plane (character.up, character.position + character.up * cursorPlaneHeight);
	}
	void Start () 
	{
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
		// Move to right side of screen
		GUITexture guiTexL   = joystickLeftGO.GetComponent<GUITexture>();
		GUITexture guiTexLbg = joystickLeftBG.GetComponent<GUITexture>() ;
//		GUITexture guiTexR   = joystickRightGO.GetComponent<GUITexture> ();
//		GUITexture guiTexRbg = joystickRightBG.GetComponent<GUITexture>() ;
//		if(Screen.width<=480)
//		{
//			guiTexR.pixelInset = new Rect(Screen.width-63-50,110,50,50) ;
//			guiTexRbg.pixelInset = new Rect(Screen.width-48-80,95,80,80) ;
			guiTexL.pixelInset = new Rect(Screen.width*0.079f,Screen.height*0.177f,Screen.width*0.125f,Screen.width*0.125f) ;
			guiTexLbg.pixelInset = new Rect(Screen.width*0.06f,Screen.height*0.145f,Screen.width*0.163f,Screen.width*0.163f) ;
//		}
//		else
//		{
//			guiTexR.pixelInset = new Rect(Screen.width-72-100,146,100,100) ;
//			guiTexRbg.pixelInset = new Rect(Screen.width-57-130,131,130,130) ;
//			guiTexL.pixelInset = new Rect(72,146,100,100) ;
//			guiTexLbg.pixelInset = new Rect(57,131,130,130) ;
//		}
		
		
		#endif	
		
		// it's fine to calculate this on Start () as the camera is static in rotation
		
		screenMovementSpace = Quaternion.Euler (0, mainCameraTransform.eulerAngles.y, 0);
		screenMovementForward = screenMovementSpace * Vector3.forward;
		screenMovementRight = screenMovementSpace * Vector3.right;	
	}
	void OnDisable () 
	{
		if (joystickLeft) 
			joystickLeft.enabled = false;
		//if (joystickRight)
		//	joystickRight.enabled = false;
	}

	void  OnEnable () 
	{
		if (joystickLeft) 
			joystickLeft.enabled = true;
		
		//if (joystickRight)
		//	joystickRight.enabled = true;
	}
	
	void FixedUpdate () 
	{
		Vector3 cameraAdjustmentVector   = Vector3.zero;
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
			motor.movementDirection = (Vector3)(joystickLeft.position.x * screenMovementRight + joystickLeft.position.y * screenMovementForward) ;
			cameraAdjustmentVector = motor.facingDirection = motor.movementDirection;
		#endif
	
		if (motor.movementDirection.sqrMagnitude > 1)
			motor.movementDirection.Normalize();
	
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
		//	motor.turretDirection = (Vector3)( joystickRight.position.x * screenMovementRight + joystickRight.position.y * screenMovementForward );		
		#endif
		
		// Set the target position of the camera to point at the focus point
		Vector3 cameraTargetPosition   ;
		character.position = new Vector3(character.position.x,0.5f,character.position.z) ;
		Vector3 playerPos = character.position ;
		Vector3 mtp =  playerPos ;
		if(playerPos.z>38)
		{
			mtp = new Vector3(mtp.x,mtp.y,38) ;
			//cameraTargetPosition = new Vector3(character.position.x,character.position.y,38) + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		}
		if(playerPos.z<-60)
		{
			mtp = new Vector3(mtp.x,mtp.y,-60) ;
			//cameraTargetPosition = new Vector3(character.position.x,character.position.y,-45) + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		}
		if(playerPos.x>77)
		{
			mtp = new Vector3(77,mtp.y,mtp.z) ;
			//cameraTargetPosition = new Vector3(77,character.position.y,character.position.z) + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		}
		if(playerPos.x<-77)
		{
			mtp = new Vector3(-77,mtp.y,mtp.z) ;
			//cameraTargetPosition = new Vector3(-77,character.position.y,character.position.z) + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		}
		// Apply some smoothing to the camera movement
		cameraTargetPosition = mtp + initOffsetToPlayer + cameraAdjustmentVector * cameraPreview;
		mainCameraTransform.position = Vector3.SmoothDamp (mainCameraTransform.position, cameraTargetPosition,ref cameraVelocity, cameraSmoothing);
		
		
		
		// Save camera offset so we can use it in the next frame
	//	cameraOffset = mainCameraTransform.position - character.position;
	}
	public static Vector3 PlaneRayIntersection (Plane plane , Ray ray )  
	{
		float dist  ;
		plane.Raycast (ray, out dist);
		return ray.GetPoint (dist);
	}

	public static Vector3 ScreenPointToWorldPointOnPlane (Vector3 screenPoint  ,Plane plane  ,Camera camera ) 
	{
		// Set up a ray corresponding to the screen position
		Ray ray   = camera.ScreenPointToRay (screenPoint);
		
		// Find out where the ray intersects with the plane
		return PlaneRayIntersection (plane, ray);
	}

	void HandleCursorAlignment (Vector3 cursorWorldPosition )
	{
		if (!cursorObject)
			return;
		
		// HANDLE CURSOR POSITION
		
		// Set the position of the cursor object
		cursorObject.position = cursorWorldPosition;
		
		#if !UNITY_FLASH
			// Hide mouse cursor when within screen area, since we're showing game cursor instead
			Screen.showCursor = (Input.mousePosition.x < 0 || Input.mousePosition.x > Screen.width || Input.mousePosition.y < 0 || Input.mousePosition.y > Screen.height);
		#endif
		
		
		// HANDLE CURSOR ROTATION
		
		Quaternion cursorWorldRotation   = cursorObject.rotation;
		if (motor.facingDirection != Vector3.zero)
			cursorWorldRotation = Quaternion.LookRotation (motor.facingDirection);
		
		// Calculate cursor billboard rotation
		Vector3 cursorScreenspaceDirection  = Input.mousePosition - mainCamera.WorldToScreenPoint (transform.position + character.up * cursorPlaneHeight);
		cursorScreenspaceDirection.z = 0;
		Quaternion cursorBillboardRotation  = mainCameraTransform.rotation * Quaternion.LookRotation (cursorScreenspaceDirection, -Vector3.forward);
		
		// Set cursor rotation
		cursorObject.rotation = Quaternion.Slerp (cursorWorldRotation, cursorBillboardRotation, cursorFacingCamera);
		
		
		// HANDLE CURSOR SCALING
		
		// The cursor is placed in the world so it gets smaller with perspective.
		// Scale it by the inverse of the distance to the camera plane to compensate for that.
		float compensatedScale  = 0.1f * Vector3.Dot (cursorWorldPosition - mainCameraTransform.position, mainCameraTransform.forward);
		
		// Make the cursor smaller when close to character
		float cursorScaleMultiplier = Mathf.Lerp (0.7f, 1.0f, Mathf.InverseLerp (0.5f, 4.0f, motor.facingDirection.magnitude));
		
		// Set the scale of the cursor
		cursorObject.localScale = Vector3.one * Mathf.Lerp (compensatedScale, 1, cursorSmallerWithDistance) * cursorScaleMultiplier;
		
		// DEBUG - REMOVE LATER
		if (Input.GetKey(KeyCode.O)) cursorFacingCamera += Time.deltaTime * 0.5f;
		if (Input.GetKey(KeyCode.P)) cursorFacingCamera -= Time.deltaTime * 0.5f;
		cursorFacingCamera = Mathf.Clamp01(cursorFacingCamera);
		
		if (Input.GetKey(KeyCode.K)) cursorSmallerWithDistance += Time.deltaTime * 0.5f;
		if (Input.GetKey(KeyCode.L)) cursorSmallerWithDistance -= Time.deltaTime * 0.5f;
		cursorSmallerWithDistance = Mathf.Clamp01(cursorSmallerWithDistance);
	}
}
