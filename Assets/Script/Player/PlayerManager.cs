using UnityEngine;
using System.Collections;


public class PlayerManager : MonoBehaviour 
{
	public Transform[] playerPrefab ;
	public GameObject playerExplo ;
	public static Transform playerTransform ;    
	public static Weapons   WeaponsScript ;
	public static PlayerState    playerState ;
	public static PlayerAttribute[] playerAttribute = new PlayerAttribute[2] ;

	
	
	//public static int            playerGoldNum ;       /* 游戏中金币数量 */
	public static int            playerScoreNum ;      /* 游戏分数 */
	public static int            currentTank ;        /* 当前选择的坦克 */
	public static float           playerHP ;
	public static float           playerMaxHP ;
	public static Transform     playerTarget ;          /* 玩家的目标 */
	public static bool          tankInvincible ;
	private Vector3 birthPos = new Vector3(0f,0.5f,0f) ;   /* 玩家出生位置 */
	public static GameObject  gridTrans ;
	void Awake()
	{
		if(gridTrans==null)
			gridTrans = GameObject.FindWithTag("GridParentTag");
		UIManager.shieldProp = 0 ;
		UIManager.firePowerProp = 0 ;
		UIManager.magnetProp = 0 ;
		UIManager.speedProp = 0 ;
	}
	void Start () 
	{
		playerState = PlayerState.birth ;
	}
	public static void AddScore(int num)
	{
		playerScoreNum += num ;
	}
	public static void AddGold(int num)
	{
		UserData.Instance.player_gold_num += num ;
	}
	public static bool LessenGold(int num)
	{
		if(UserData.Instance.player_gold_num>=num)
		{
			UserData.Instance.player_gold_num -= num ;
		}
		else
		{
			return false ;
		}
		return true ;
	}
	public static void AddHP(float hp )
	{
		playerHP += hp ;
		if(playerHP>=playerMaxHP)
		{
			playerHP = playerMaxHP ;
		}
	}
	public static void ApplyNPCDamage(float damage)
	{
		if(damage<=0)
		{
			damage = 1;
		}
		if(UIManager.shieldProp<=0&&playerTransform.gameObject.active&&!tankInvincible&&!UIManager.currentLevelInvincible)
		{
			playerHP -= damage ;
			//Debug.Log("playermanager is damaged:"+damage);
			WeaponsScript._flash.Flash() ;
			if(playerHP<=0)
			{
				playerHP = 0 ;
				playerState = PlayerState.dead ;
			}
		}
	}
	
	void Update () 
	{
		if(UIManager.gameState != UIManager.GameState.run)
		{
			return ;
		}
		switch(playerState)
		{
		case PlayerState.birth:

			if( UserData.Instance.player_attribute.isAllReachMaxLevel(15) )
			{
				currentTank = 1;
			}
			else if(UserData.Instance.player_attribute.minReachedLevel()>=12)
			{
				currentTank = 0;
			}
			else if(UserData.Instance.player_attribute.minReachedLevel()>=9)
			{
				currentTank = 1;
			}
			else if(UserData.Instance.player_attribute.minReachedLevel()>=6)
			{
				currentTank = 0;
			}
			else if(UserData.Instance.player_attribute.minReachedLevel()>=3)
			{
				currentTank = 1;
			}
			else
			{
				currentTank = 0;
			}
			
			playerTransform = (Transform)Instantiate(playerPrefab[currentTank],birthPos,Quaternion.identity) ;
			WeaponsScript = playerTransform.GetComponent<Weapons>() ;
			playerHP = playerMaxHP = 100 ;
			playerState = PlayerState.wait ;
			
			break;
		case PlayerState.dead:
			playerState = PlayerState.wait ;
			Instantiate(playerExplo,playerTransform.position,playerTransform.rotation) ;
			ObjectBuffer.InstantiateObject(12,playerTransform.position,Quaternion.Euler(-90,0,0)) ;
			playerTransform.gameObject.SetActiveRecursively(false) ;
			//this.Invoke("GameOver",3) ;
			this.Invoke("Revive",3);
			break;
		case PlayerState.Resurrection:
			playerTransform.gameObject.SetActiveRecursively(true) ;
			playerHP = playerMaxHP = 100 ;
			if(UIManager.useSkill)
			{
				UIManager.useSkill = false ;
				PlayerManager.WeaponsScript.Skill() ;
			}
			playerState = PlayerState.wait ;
			break;
		}
		
	}
	
	void Revive()
	{
		UIManager.UIManagerScript.ShowReviveShop(UIManager.gameState,this.transform.localPosition.z-25) ;
	}
	
	void GameOver()
	{

		UIManager.UIManagerScript.ShowGameOver(this.transform.localPosition.z-25) ;
	}
	public enum PlayerState
	{
		wait,
		birth,
		dead,
		Resurrection,
	}
	
}
