using UnityEngine;
using System.Collections;

public class TankMovementMotor : MovementMotor
{
	//public var movement : MoveController;
	//private float[] walkingSpeed  = new float[]{10,} ;
	public float walkingSnappyness  = 50;
	public float turningSmoothing  = 0.3f;
	public Transform weapons ;
	public Transform armors ;
//	private Transform thisTransform  ;
//*	private Vector3 lastPosition  = Vector3.zero;
//*	private Vector3 velocity  = Vector3.zero;
//*	private Vector3 localVelocity  = Vector3.zero;
//*	private float speed   = 0;
//*	private float angle  = 0;
	
	void Awake()
	{
	//	thisTransform = transform;
		//*lastPosition = tr.position;
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(UIManager.gameState!=UIManager.GameState.run)
		{
			rigidbody.angularVelocity = Vector3.zero;
			return ;
		}
	// Handle the movement of the character
		Vector3 targetVelocity ;
		if(UIManager.speedProp>0)
		{
			//targetVelocity  = movementDirection * PlayerManager.playerAttribute[PlayerManager.currentTank].walkingSpeed[10];
			//speedProp item id = 6
			//targetVelocity = movementDirection * PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeed * ((MoneyProp)MoneyPropConfig.Instance.money_prop_list[6]).ratio;
			targetVelocity = movementDirection * UserData.Instance.player_attribute.moveSpeed * ((MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropName("Speed")).ratio;
		}
		else
		{
			//targetVelocity  = movementDirection * PlayerManager.playerAttribute[PlayerManager.currentTank].walkingSpeed[PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel];
			//targetVelocity = movementDirection * PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeed;
			targetVelocity = movementDirection * UserData.Instance.player_attribute.moveSpeed;
		}
		Vector3 deltaVelocity  = targetVelocity - rigidbody.velocity;
		if (rigidbody.useGravity)
			deltaVelocity.y = 0;
		rigidbody.AddForce (deltaVelocity * walkingSnappyness, ForceMode.Acceleration);
		
		// Setup player to face facingDirection, or if that is zero, then the movementDirection
		Vector3 faceDir  = facingDirection;
		if (faceDir == Vector3.zero)
			faceDir = movementDirection;
		// Make the character rotate towards the target rotation
		if (faceDir == Vector3.zero) 
		{
			NPCManager.stopSentRay = false ;
			rigidbody.angularVelocity = Vector3.zero;
		}
		else 
		{
			NPCManager.stopSentRay = true ;
		//	float rotationAngle  = AngleAroundAxis (transform.forward, faceDir, Vector3.up);
			armors.rotation = Quaternion.Slerp(armors.rotation,Quaternion.LookRotation(faceDir),Time.deltaTime*3) ;
		}
		
//		if(turretDirection == Vector3.zero)
//		{
//			PlayerManager.AutoOrientation = true ;
//			//turret.localRotation = Quaternion.Slerp (turret.localRotation, Quaternion.Euler(90, 0, 0), Time.deltaTime * 10.0f);
//		}
//		else
//		{
//			PlayerManager.AutoOrientation = false ;
//			weapons.rotation = Quaternion.LookRotation(turretDirection);
//		}
		
//		velocity = (tr.position - lastPosition) / Time.deltaTime;
//		localVelocity = tr.InverseTransformDirection (velocity);
//		localVelocity.y = 0;
//		speed = localVelocity.magnitude;
//		angle = HorizontalAngle (localVelocity);
//		
//		lastPosition = tr.position;
	}
	static float AngleAroundAxis (Vector3 dirA ,Vector3 dirB ,Vector3 axis ) 
	{
	    // Project A and B onto the plane orthogonal target axis
	    dirA = dirA - Vector3.Project (dirA, axis);
	    dirB = dirB - Vector3.Project (dirB, axis);
	   
	    // Find (positive) angle between A and B
	    float angle  = Vector3.Angle (dirA, dirB);
	   
	    // Return angle multiplied with 1 or -1
	    return angle * (Vector3.Dot (axis, Vector3.Cross (dirA, dirB)) < 0 ? -1 : 1);
	}
}
