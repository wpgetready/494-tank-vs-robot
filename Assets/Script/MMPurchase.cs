using UnityEngine;
using System.Collections;

public class MMPurchase : MonoBehaviour {
	
    public static Hashtable paycode_list;
	public static GameObject obj;
	
	void Awake() {
		DontDestroyOnLoad (transform.gameObject);
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void onMMBillingFinish(string message)
	{
		/**
		 *  09-11 23:22:43.710: I/System.out(4268): code=102
			09-11 23:22:43.710: E/onBillingfinish(4268): code=102.订购成功
			09-11 23:22:43.710: D/PowerManagerService(2453): enableUserActivity true
			09-11 23:22:43.715: D/IAPListener(4268): billing finish, status code = 102
			09-11 23:22:43.715: I/System.out(4268): 订购结果：订购成功,OrderID ： 11130911232239776623,Paycode:30000360200903,tradeID:1775D6A74E30D32301429A6899E874A0,ORDERTYPE:0
			09-11 23:22:43.715: D/KeyguardViewMediator(2453): adjustStatusBarLocked: mShowing=false mHidden=true isSecure=true --> flags=0x0
			09-11 23:22:43.715: D/STATUSBAR-StatusBarManagerService(2453): manageDisableList what=0x0 pkg=android
			09-11 23:22:43.720: I/ClipboardServiceEx(2453): Send intent for dismiss clipboard dialog inside hideCurrentInputLocked() !
			09-11 23:22:43.720: I/Unity(4268): onMMBillingFinish message:OrderId:11130911232239776623|Paycode:30000360200903|TradeID:1775D6A74E30D32301429A6899E874A0|OrderType:0|code:102
			
			09-12 08:15:35.070: I/System.out(19347): code=104
			09-12 08:15:35.070: E/onBillingfinish(19347): code=104.鉴权成功，该商品已经购买
			09-12 08:15:35.070: D/IAPListener(19347): billing finish, status code = 104
			09-12 08:15:35.070: I/System.out(19347): 订购结果：订购成功,OrderID ： 11130912063953116854,Paycode:30000360200901
			09-12 08:15:35.075: I/Unity(19347): onMMBillingFinish message:OrderId:11130912063953116854|Paycode:30000360200901|code:104
			
			
			**/
		
		Debug.Log("onMMBillingFinish message:"+message);
		string[] words = message.Split('|');
		Hashtable ht = new Hashtable();
		foreach (string word in words)
		{
		    string[] key_value = word.Split(':');
			ht.Add(key_value[0],key_value[1]);
		}
		

		if(((ht["code"].ToString() == "102"||ht["code"].ToString() == "104") && ht["OrderId"] != null && ht["TradeID"] != null && ht["OrderType"] != null) || (ht["code"].ToString() == "104" && ht["OrderId"] != null ))
		{
			string payCode = ht["Paycode"].ToString();
			string product_id = payCode;
			MoneyGold mg = MoneyGoldConfig.Instance.getMoneyGoldByProductId(product_id);
           //paycode > money -> gold
           //int money_code = (int)getMoneyCodeByPayCode(payCode);
           //Debug.Log("payCode:"+payCode);
           ////int money_gold_id = (int)getMoneyGoldIdByPayCode(payCode);
           //Debug.Log("money_gold_id:"+money_gold_id);
           ////MoneyGold mg = (MoneyGold)MoneyGoldConfig.Instance.money_gold_list[money_gold_id];
           //MoneyGold mg = MoneyGoldConfig.Instance.getMoneyGoldByMoneyCode(money_code);
		   int money_gold_id = mg.id;
           if(mg != null)
           {
                   //Debug.Log("mg is not null");
                   if(money_gold_id==1)
                   {
                           //unlock level 2, sublevel 1
                           UserData.Instance.UnLockLevel(2,1);
                   }
					if(money_gold_id >= 11 && money_gold_id <=15)
					{
						int prop_id = money_gold_id;
						MoneyProp mp = (MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropId(prop_id);
						UserData.Instance.AddUserProp(prop_id,4,mp.ratio);
					}
					if(money_gold_id == 10)
					{
						UIManager.shieldProp = 1 ;
						PlayerManager.playerState = PlayerManager.PlayerState.Resurrection ;
						UIManager.gameState = UIManager.GameState.run ;
						UIManager.useSkill = true ;
						Time.timeScale = 1 ;
					}
                   UserData.Instance.AddGold(mg.gold);
                   //Debug.Log("add gold:"+mg.gold);
           }
           GameData.Instance.WriteUserData();
           MoneyGoldShop mgs = obj.GetComponent<MoneyGoldShop>() as MoneyGoldShop;
           if(mgs != null)
           {
                   //Debug.Log("mgs close");
                   mgs.Close();
           }
           LevelShop ls = obj.GetComponent<LevelShop>() as LevelShop;
           if(ls!=null)
           {
                   //Debug.Log("ls close");
                   ls.Close();
           }
			HintShop hs = obj.GetComponent<HintShop>() as HintShop;
			if(hs != null)
			{
				hs.Close();
			}
			ReviveShop rs = obj.GetComponent<ReviveShop>() as ReviveShop;
			if(rs != null)
			{
				rs.Close();
			}
		}
		else
		{
			Debug.Log("code is not 102 or 104");
		}
		//SpinWheel.stop_spinwheel = true;
	}
	
	public static object getMoneyCodeByPayCode(string paycode)
	{
	       foreach(DictionaryEntry item in paycode_list)
	       {
	               if(paycode.Equals(item.Value.ToString()))
	               {
	                       return item.Key;
	               }
	       }
	       return null;
	}

   public static object getMoneyGoldIdByPayCode(string paycode)
   {
           foreach(DictionaryEntry item in paycode_list)
           {
                   if(paycode.Equals(item.Value.ToString()))
                   {
                           return item.Key;
                   }
           }
           return null;
   }
}
