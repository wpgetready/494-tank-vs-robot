﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]

public class UIManager : MonoBehaviour
{
	public Camera       uiCamera ;
//	public static int      BombNum ;
//	public static int      SkillNum ;
//	public static int      DoubleGoldNum ;
	public static int      ResurrectionNum ;
	//public static int      GodNum ;
	public static GameState gameState ;
	public GameState        preGameState ;
	public static GameObject SpecialEffect ;
	//public static float SoundVulome ;
	//public static float MusicVulome ;
	public AudioClip[]  backClip ;
	/// <summary>
	/// The first run.
	/// </summary>
	
	public static int        showQuality ;
	//public static int  gameQuality ;
	#region 道具消失的时间
	public static float  firePowerProp ;
	public static float  magnetProp ;
	public static float  shieldProp ;
	public static float  speedProp ;
	#endregion
	public static int[]    level = new int[50] ;    /* 关卡是否解锁 */
	public static int[]    levelStarNum = new int[50] ;/* 每一关星星的数量 */
	public static int[]    levelStarNum02 = new int[50] ;/* 每一关星星的数量 */
	public static int[]    oneLevelStarNum = new int[50] ;
	
	public static int[]    tank = new int[2] ;    /* 坦克是否解锁 */
	/// <summary>
	/// 每日登录
	/// </summary>
	public static int      preDay ;
	public static int      dayLogIndex ;
	public static int[]    firstDayLogIn = new int[3] ;
	
	public static string currentLang ;
	public static int   gameModeVal ;
	
	public static bool  currentLevelInvincible ; /* 当前关卡无敌 */
	public static bool isShowWaveTip ;
	public static bool  bossInvincible ; /* boss无敌一段时间 */
	public static GameObject UIMANAGER ;
	public static UIManager  UIManagerScript ;
	public GameObject UIParent ;
	public static  bool  useSkill ;
	public static Transform selectPlayerTarget ;
	public static GameObject[] Joystick = new GameObject [2] ;
	
	public static GameObject MultiLanguageMgr;
	
	void Awake()
	{
		
		
		DontDestroyOnLoad (transform.gameObject);
		PlayerManager.playerAttribute = new PlayerAttribute[2] ;
		PlayerManager.playerAttribute[0] = new PlayerAttribute() ;
		PlayerManager.playerAttribute[1] = new PlayerAttribute() ;
		GetData() ;
		UIManagerScript = this ;
		InitTankAtt() ;
		gameState = GameState.Init ;
		preGameState = gameState ;
	}
	void InitTankAtt()
	{
		
		/*
		for(int i=0;i<PlayerManager.playerAttribute[1].armorPercentage.Length;i++)
			PlayerManager.playerAttribute[1].armorPercentage[i] = PlayerManager.playerAttribute[0].armorPercentage[i]*1.5f ;
		for(int i=0;i<PlayerManager.playerAttribute[1].weaponsPercentage.Length;i++)
			PlayerManager.playerAttribute[1].weaponsPercentage[i] = PlayerManager.playerAttribute[0].weaponsPercentage[i]*1.5f ;
		*/
	}
	
	
	void Start () 
	{
		
		//Debug.Log("UIManager start");
		MultiLanguageMgr = GameObject.FindGameObjectWithTag("MultiLanguageMgr");
		Localization localization = MultiLanguageMgr.GetComponent<Localization>();
		localization.currentLanguage = GameConfig.Instance.lang;
		
		LevelConfig.Instance.Load();
		NpcPropConfig.Instance.Load();
		GoldPropConfig.Instance.Load();
		MoneyPropConfig.Instance.Load();
		MoneyGoldConfig.Instance.Load();
		GameData.Instance.CreateUserData();
		GameData.Instance.ReadUserData();
		SpecialEffect = GameObject.FindGameObjectWithTag("specialEffect");
		Invoke("ShowGame",1);
	}
	
	public void ShowGame()
	{
		ShowBackGround() ;
		ShowGameMenu();
	}
	void MoreGame()
	{
		Application.OpenURL("") ;
	}
	void Grade()
	{
		Application.OpenURL("") ;
	}
	#region 声音的管理
	public void SoundChange(float val)
	{
		UserData.Instance.setSoundVolume(val);
	}
	public void MusicChange(float val)
	{
		UserData.Instance.setMusicVolume(val);
		PlayBackSound(-1,true) ;
	}
	
	void PlayBackSound(int index,bool isLoop)
	{
		if(index!=-1)
		{
			audio.clip = backClip[index] ;
		}
		if(UserData.Instance.user_setting.music_volume>0)
		{
			if(!audio.isPlaying)
				audio.Play() ;
			audio.loop = isLoop ;
			audio.volume = UserData.Instance.user_setting.music_volume ;
		}
		else
		{
			audio.Stop() ;
		}
	}
	public static void PlayMusic(AudioClip clip)
	{
		if(UserData.Instance.user_setting.sound_volume>0)
		{
			NGUITools.PlaySound(clip,UserData.Instance.user_setting.sound_volume) ;
		}
	}
	
	#endregion
	#region 所有UI界面

	
	/// <summary>
	/// 背景
	/// </summary>
	public GameObject  backGroundPrefab ;
	private GameObject  backGround ;
	public void ShowBackGround(int state=0,float nextZ=0f)
	{
		if(backGround==null)
		{
			backGround = NGUITools.AddChild(UIParent,backGroundPrefab,nextZ);
			
		}
		backGround.transform.localPosition = new Vector3(backGround.transform.localPosition.x,backGround.transform.localPosition.y,nextZ);
		UISprite bg = (UISprite)(backGround.transform.Find("Sprite").GetComponent<UISprite>());
		switch(state)
		{
		case 1:
			bg.color = new Color(0.25f,0.25f,0.25f,1.0f);
			break;
		case 0:
			bg.color = new Color(1f,1f,1f,1f);
			break;
		}
		PlayBackSound(0,true);
	}
	
	public void CloseBackGround()
	{
		NGUITools.Destroy(backGround) ;
	}
	
	public GameObject gameMenuPrefab;
	private GameObject gameMenu;
	public void ShowGameMenu()
	{
		if(gameMenu==null)
		{
			gameMenu = NGUITools.AddChild(UIParent,gameMenuPrefab,-1);
		}
		gameState = GameState.GameMenu;
	}
	
	
	/// <summary>
	/// 主菜单
	/// </summary>
	/// /
	/// 
	/// 
	/*
	 * public GameObject mainMenuPrefab ;
	private GameObject  mainMenu ;
	public void ShowMainMenu()
	{
		if(mainMenu==null)
			mainMenu = NGUITools.AddChild(UIParent,mainMenuPrefab,-1) ;
		gameState = GameState.MainMenu ;
	}*/
	
	/// <summary>
	/// 设置选项
	/// </summary>
	public GameObject gameSettingPrefab ;
	private GameObject gameSetting ;
	public void ShowGameSetting()
	{
		if(gameSetting==null)
		{
			gameSetting = NGUITools.AddChild(UIParent,gameSettingPrefab,-1) ;
		}
		gameState = GameState.GameSetting ;
	}
	
	/// <summary>
	/// 帮助关于
	/// </summary>
	public GameObject infoPrefab ;
	private GameObject info ;
	public void ShowInfo()
	{
		if(info==null)
			info = NGUITools.AddChild(UIParent,infoPrefab,-1) ;
		gameState = GameState.Info ;
	}
	public void CloseInfo()
	{
		NGUITools.Destroy(info) ;
	}
	
	public GameObject GameLevelPrefab ;
	private GameObject gameLevel ;
	public void ShowGameLevel()
	{
		if(gameLevel==null)
		{
			//CloseBackGround();
			ShowBackGround(1);
			gameLevel = NGUITools.AddChild(UIParent,GameLevelPrefab,-1) ;
		}
		
		UIManager.gameState = UIManager.GameState.SelectLevel;
	}

	/// <summary>
	/// 游戏加载
	/// </summary>
	public GameObject loadingPrefab ;
	private GameObject loading ;
	public void ShowLoading(float nextZ=-1)
	{
		if(loading==null)
		{
			loading = NGUITools.AddChild(UIParent,loadingPrefab,nextZ);
		}
	}
	/// <summary>
	/// 游戏场景
	/// </summary>
	public GameObject GameScenePrefab ;
	private GameObject gameScene ;
	public void ShowGameScene(float nextZ=-1)
	{
		if(gameScene==null)
		{
			//gameScene = NGUITools.AddChild(UIParent,GameScenePrefab,1) ;
			gameScene = NGUITools.AddChild(UIParent,GameScenePrefab,nextZ) ;
		}
		else
		{
			UIManager.gameState = UIManager.GameState.run ;
		}
		PlayBackSound(Random.Range(1,5),true);
		
	}
	public void CloseGameScene()
	{
		NGUITools.Destroy(gameScene);
	}
	/// <summary>
	/// 游戏暂停
	/// </summary>
	public GameObject pausePrefab ;
	private GameObject pause ;
	public void ShowGamePause(float nextZ=-1)
	{
		if(pause==null)
			pause = NGUITools.AddChild(UIParent,pausePrefab,nextZ) ;
		gameState = GameState.Pause ;
		Time.timeScale = 0 ;
	}
	/// <summary>
	/// 游戏结束.
	/// </summary>
	public GameObject gameOverPrefab ;
	private GameObject gameOver ;
	public void ShowGameOver(float nextZ=-1)
	{
		if(gameOver==null)
			gameOver = NGUITools.AddChild(UIParent,gameOverPrefab,nextZ) ;
		PlayBackSound(5,false) ;
		Debug.Log ("WIn");
		AdmobControl.Instance.showInterstitial ();
		UIManager.gameState = UIManager.GameState.GameOver ;
	}
	public void ShowGameWin(float nextZ=-1)
	{
		if(gameOver==null)
			gameOver = NGUITools.AddChild(UIParent,gameOverPrefab,nextZ) ;
		PlayBackSound(6,false) ;
		Debug.Log ("WIn");
		AdmobControl.Instance.showInterstitial ();
		UIManager.gameState = UIManager.GameState.GameWin ;
	}
	/// <summary>
	/// 原地复活
	/// </summary>
	/*public GameObject  resurrectionPrefab ;
	private GameObject  resurrection ;
	public void ShowResurrection()
	{
		if(resurrection==null)
			resurrection = NGUITools.AddChild(UIParent,resurrectionPrefab,-1) ;
		UIManager.gameState = UIManager.GameState.Resurrection ;
		Time.timeScale = 0 ;
	}
	*/
	/// <summary>
	/// 游戏商店
	/// </summary>
	/*public GameObject  ShopPrefab ;
	private GameObject  Shop ;
	public void ShowShop()
	{
		if(Shop==null)
			Shop = NGUITools.AddChild(UIParent,ShopPrefab,-2) ;
		
	}
	*/
	
	public GameObject PopUpShopPrefab;
	private GameObject popupShop;
	public void ShowPopUpShop(string shouldGoto,string lastUI, GameState gameState,float nextZ=-10)
	{
		if(popupShop == null)
		{
			popupShop = NGUITools.AddChild(UIParent,PopUpShopPrefab,nextZ);
			popupShop.GetComponent<PopUpShop>().shouldGoto = shouldGoto;
			popupShop.GetComponent<PopUpShop>().lastUI = lastUI;
			popupShop.GetComponent<PopUpShop>().preGameState = gameState;
		}
	}
	
	
	public GameObject ReviveShopPrefab;
	private GameObject reviveShop;
	public void ShowReviveShop(GameState gameState,float nextZ=-10)
	{
		if(reviveShop == null)
		{
			reviveShop = NGUITools.AddChild(UIParent,ReviveShopPrefab,nextZ);
			reviveShop.GetComponent<ReviveShop>().preGameState = gameState;
		}
	}
	
	public GameObject HintShopPrefab;
	private GameObject hintShop;
	public void ShowHintShop(string lastUI, GameState gameState,string prop_name,float nextZ=-10)
	{
		if(hintShop == null)
		{
			hintShop = NGUITools.AddChild(UIParent,HintShopPrefab,nextZ);
			hintShop.GetComponent<HintShop>().prop_name = prop_name;
			hintShop.GetComponent<HintShop>().lastUI = lastUI;
			hintShop.GetComponent<HintShop>().preGameState = gameState;
		}
	}
	
	public GameObject LevelShopPrefab;
	private GameObject levelShop;
	public void ShowLevelShop()
	{
		if(levelShop == null)
		{
			levelShop = NGUITools.AddChild(UIParent,LevelShopPrefab,-2);
		}
	}
	
	public GameObject MoneyGoldShopPrefab;
	private GameObject moneyGoldShop;
	public void ShowMoneyGoldShop(string lastUI, GameState gameState,float nextZ=-2)
	{
		//Debug.Log("nextZ:"+nextZ);
		if(moneyGoldShop==null)
		{
			moneyGoldShop = NGUITools.AddChild(UIParent,MoneyGoldShopPrefab,nextZ);
			moneyGoldShop.GetComponent<MoneyGoldShop>().preGameState = gameState; 
			moneyGoldShop.GetComponent<MoneyGoldShop>().lastUI = lastUI;
		}
		else
		{
			//Debug.Log("moneyGoldShop.transform.localPosition:"+moneyGoldShop.transform.localPosition);
			Vector3 pos = moneyGoldShop.transform.localPosition;
			pos.z = nextZ;
			moneyGoldShop.transform.localPosition = pos;
			//Debug.Log("moneyGoldShop is not null");
		}
	}
	
	public GameObject GoldShopPrefab;
	private GameObject GoldShop;
	public void ShowGoldShop(float nextZ=-2)
	{
		if(GoldShop==null)
		{
			GoldShop = NGUITools.AddChild(UIParent,GoldShopPrefab,nextZ);
		}
		else{
			GoldShop.GetComponent<GoldShop>().Refresh();
		}
	}
	
	public GameObject MoneyShopPrefab;
	private GameObject MoneyShop;
	public void ShowMoneyShop(float nextZ=-2)
	{
		if(MoneyShop==null)
		{
			MoneyShop = NGUITools.AddChild(UIParent,MoneyShopPrefab,nextZ);
		}
		else{
			MoneyShop.GetComponent<MoneyShop>().Refresh();
		}
	}
	
	public GameObject GameHelpPrefab;
	private GameObject gameHelp;
	public void ShowGameHelp()
	{
		if(gameHelp==null)
		{
			gameHelp = NGUITools.AddChild(UIParent,GameHelpPrefab,-1);
		}
		UIManager.gameState = UIManager.GameState.Help;
	}
	
	public GameObject GameInfoPrefab;
	private GameObject gameInfo;
	public void ShowGameInfo()
	{
		if(gameInfo==null)
		{
			gameInfo = NGUITools.AddChild(UIParent,GameInfoPrefab,-1);
		}
		UIManager.gameState = UIManager.GameState.Info;
	}
	
	public GameObject paySuccess;
	private GameObject pay_success;
	
	public void ShowPaySuccess()
	{
		pay_success = NGUITools.AddChild(UIParent,paySuccess,-100);
		pay_success.GetComponent<UILabel>().text = UIManager.MultiLanguageMgr.GetComponent<Localization>().Get("GameShop.PaySuccess");
		TweenScale ts = pay_success.AddComponent<TweenScale>();
		ts.method = UITweener.Method.Linear;
		ts.style = UITweener.Style.Once;
		ts.ignoreTimeScale = true;
		ts.delay = 0;
		ts.duration = 2;
		ts.eventReceiver = UIManager.UIManagerScript.gameObject;
		ts.callWhenFinished = "OnFinished";
		ts.from = new Vector3(30,30,1);
		ts.to = new Vector3(0,0,0);
	}
	
	public void OnFinished()
	{
		NGUITools.Destroy(pay_success);
	}
	
	#endregion
	
	#region 数据读取和保存
	public static void SaveData()
	{
		for(int i=0;i<2;i++)
		{
			PlayerPrefs.SetInt("armorLevel"+i,PlayerManager.playerAttribute[i].armorLevel) ;
			PlayerPrefs.SetInt("weaponsLevel"+i,PlayerManager.playerAttribute[i].weaponsLevel) ;
			PlayerPrefs.SetInt("moveSpeedLevel"+i,PlayerManager.playerAttribute[i].moveSpeedLevel) ;
			PlayerPrefs.SetInt("attackSpeedLevel"+i,PlayerManager.playerAttribute[i].attackSpeedLevel) ;
			PlayerPrefs.SetInt("turretSpeedLevel"+i,PlayerManager.playerAttribute[i].turretSpeedLevel) ;
			PlayerPrefs.SetInt("propsAdsorptionLevel"+i,PlayerManager.playerAttribute[i].propsAdsorptionLevel) ;
		}
//		PlayerPrefs.SetInt("BombNum",UIManager.BombNum) ;
//		PlayerPrefs.SetInt("DoubleGoldNum",UIManager.DoubleGoldNum) ;
//		PlayerPrefs.SetInt("GodNum",UIManager.GodNum) ;
//		PlayerPrefs.SetInt("ResurrectionNum",UIManager.ResurrectionNum) ;
//		PlayerPrefs.SetInt("SkillNum",UIManager.SkillNum) ;
		
//		PlayerPrefs.SetInt("currentTank",PlayerManager.currentTank) ;
//		PlayerPrefs.SetInt("currentWaveNum",NPCManager.currentWaveNum) ;
		////PlayerPrefs.SetInt("playerGoldNum",PlayerManager.playerGoldNum) ;
		PlayerPrefs.SetInt("playerGoldNum",UserData.Instance.player_gold_num);
		PlayerPrefs.SetInt("playerScoreNum",PlayerManager.playerScoreNum) ;
		PlayerPrefs.SetFloat("SoundVulome",UserData.Instance.user_setting.sound_volume) ;
		PlayerPrefs.SetFloat("MusicVulome",UserData.Instance.user_setting.music_volume) ;
		//PlayerPrefs.SetInt("gameQuality",UIManager.gameQuality) ;
		for(int i=0;i<firstDayLogIn.Length;i++)
			PlayerPrefs.SetInt("firstDayLogIn"+i,firstDayLogIn[i]) ;
		
		PlayerPrefs.SetInt("showQuality",UIManager.showQuality) ;
		PlayerPrefs.SetString("currentLang",currentLang) ;
		PlayerPrefs.SetInt("preDay",UIManager.preDay) ;
		PlayerPrefs.SetInt("dayLogIndex",UIManager.dayLogIndex) ;
		for(int i=0;i<UIManager.levelStarNum.Length;i++)
			PlayerPrefs.SetInt("levelStarNum"+i,UIManager.levelStarNum[i]) ;
		for(int i=0;i<UIManager.levelStarNum02.Length;i++)
			PlayerPrefs.SetInt("levelStarNum02"+i,UIManager.levelStarNum02[i]) ;
//		for(int i=0;i<level.Length;i++)
//			PlayerPrefs.SetInt("level"+i,level[i]) ;
//		for(int i=0;i<tank.Length;i++)
//			PlayerPrefs.SetInt("tank"+i,tank[i]) ;
	}
	public static void GetData()
	{
		for(int i=0;i<2;i++)
		{
			PlayerManager.playerAttribute[i].armorLevel = PlayerPrefs.GetInt("armorLevel"+i,0) ;
			PlayerManager.playerAttribute[i].weaponsLevel = PlayerPrefs.GetInt("weaponsLevel"+i,0) ;
			PlayerManager.playerAttribute[i].moveSpeedLevel = PlayerPrefs.GetInt("moveSpeedLevel"+i,0) ;
			PlayerManager.playerAttribute[i].attackSpeedLevel = PlayerPrefs.GetInt("attackSpeedLevel"+i,0) ;
			PlayerManager.playerAttribute[i].turretSpeedLevel = PlayerPrefs.GetInt("turretSpeedLevel"+i,0) ;
			PlayerManager.playerAttribute[i].propsAdsorptionLevel = PlayerPrefs.GetInt("propsAdsorptionLevel"+i,0) ;
		}
//		UIManager.BombNum = PlayerPrefs.GetInt("BombNum",0) ;
//		UIManager.DoubleGoldNum = PlayerPrefs.GetInt("DoubleGoldNum",0) ;
//		UIManager.GodNum = PlayerPrefs.GetInt("GodNum",0) ;
//		UIManager.ResurrectionNum = PlayerPrefs.GetInt("ResurrectionNum",0) ;
//		UIManager.SkillNum = PlayerPrefs.GetInt("SkillNum",0) ;
		
//		NPCManager.currentWaveNum = PlayerPrefs.GetInt("currentWaveNum",0) ;
//		PlayerManager.currentTank = PlayerPrefs.GetInt("currentTank",0) ;
		/*
		PlayerManager.playerGoldNum = PlayerPrefs.GetInt("playerGoldNum",0) ;
		PlayerManager.playerScoreNum = PlayerPrefs.GetInt("playerScoreNum",0) ;
		UserData.Instance.user_setting.sound_volume = PlayerPrefs.GetFloat("SoundVulome",1) ;
		UserData.Instance.user_setting.music_volume = PlayerPrefs.GetFloat("MusicVulome",1) ;
		*/
		//UIManager.gameQuality = PlayerPrefs.GetInt("gameQuality",0) ;
		for(int i=0;i<firstDayLogIn.Length;i++)
			firstDayLogIn[i] = PlayerPrefs.GetInt("firstDayLogIn"+i,0) ;
		UIManager.showQuality = PlayerPrefs.GetInt("showQuality",0) ;
		currentLang = PlayerPrefs.GetString("currentLang",Application.systemLanguage.ToString()) ;
		UIManager.preDay = PlayerPrefs.GetInt("preDay",-1) ;
		UIManager.dayLogIndex = PlayerPrefs.GetInt("dayLogIndex",2) ;
		for(int i=0;i<UIManager.levelStarNum.Length;i++)
			UIManager.levelStarNum[i] = PlayerPrefs.GetInt("levelStarNum"+i,0) ;
		for(int i=0;i<UIManager.levelStarNum02.Length;i++)
			UIManager.levelStarNum02[i] = PlayerPrefs.GetInt("levelStarNum02"+i,0) ;
//		for(int i=0;i<level.Length;i++)
//		{
//			if(i==0)
//				level[i] = PlayerPrefs.GetInt("level"+i,1) ;
//			else
//				level[i] = PlayerPrefs.GetInt("level"+i,0) ;
//			level[i] = 1 ;
//		}
//		for(int i=0;i<tank.Length;i++)
//		{
//			if(i==0)
//				tank[i] = PlayerPrefs.GetInt("tank"+i,1) ;
//			else
//				tank[i] = PlayerPrefs.GetInt("tank"+i,0) ;
//		}
//		tank[1] =1 ;
		////PlayerManager.playerGoldNum = 100000 ;
		
	}
	#endregion
	
	public enum GameState
	{
		Init,
		MainMenu,
		SelectTank,
		SelectLevel,
		Guide,
		Loading,
		run,
		Shop,
		Pause,
		Bomb,
		WaitStart,
		GameOver,
		GameWin ,
		Resurrection ,
		Buy,
		GameSetting,
		Info,
		GameMenu,
		About,
		Help,
	}
	
}
