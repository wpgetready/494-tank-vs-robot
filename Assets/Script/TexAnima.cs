using UnityEngine;
using System.Collections;

public class TexAnima : MonoBehaviour
{
	private int _currentIndex ;
	void Start ()
	{
		_currentIndex = Random.Range(0,9) ;
		 Vector2 size = new Vector2 (0.33f, 0.33f);
   		 int uIndex = _currentIndex % 3;
   		 int vIndex = _currentIndex / 3;
  		 Vector2 offset = new Vector2 (uIndex * size.x, 1.0f - size.y - vIndex * size.y);
		 renderer.material.SetTextureOffset ("_MainTex", offset);
   		 renderer.material.SetTextureScale ("_MainTex",size);
	}
	void OnEnable()
	{
		a = 1 ;
	}
	private float a ;
	void Update ()
	{
		a -= Time.deltaTime*0.1f ;
		if(a<=0)
		{
			a = 0 ;
			DestroyThis() ;
		}
		renderer.material.color = new Color(1,1,1,a);
	}
	void DestroyThis()
	{
		ObjectBuffer.DestroyBufferObject(this.gameObject) ;
	}
}
