using UnityEngine;
using System.Collections;

public class ChangeSpriteSize : MonoBehaviour
{
	public BoxCollider thisCollider ;
	public Vector3 sizeCN ;
	public Vector3 sizeEN ;
	void OnEnable () 
	{
		Change() ;
	}
	public void Change()
	{
		if(UIManager.currentLang == "Chinese")
		{
			transform.localScale = sizeCN ;
			if(thisCollider!=null)
				thisCollider.size = new Vector3(sizeCN.x,sizeCN.y,0) ;
		}
		else
		{
			transform.localScale = sizeEN ;
			if(thisCollider!=null)
				thisCollider.size = new Vector3(sizeEN.x,sizeEN.y,0) ;
		}
	}
}
