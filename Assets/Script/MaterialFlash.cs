using UnityEngine;
using System.Collections;

public class MaterialFlash : MonoBehaviour 
{
	public MeshRenderer []myRender ;
	public SkinnedMeshRenderer[] skinRender ;
	private bool _isFlash;
	
	void OnEnable () 
	{
		_isFlash = false;
		for(int cnt = 0; cnt < skinRender.Length; cnt++)
		{
			skinRender[cnt].material.SetColor("_Color1",new Color(0.6f,0.6f,0.6f,1));
		}
		for(int cnt = 0; cnt < myRender.Length; cnt++)
		{
			myRender[cnt].material.SetColor("_Color1",new Color(0.6f,0.6f,0.6f,1));
		}
	}
	private Color[] color = new Color[]{Color.white,Color.red,Color.blue,Color.green,Color.magenta} ;
	public void ChangeColor(int colorIndex)
	{
		for(int cnt = 0; cnt < skinRender.Length; cnt++)
		{
			skinRender[cnt].material.SetColor("_Color",color[colorIndex]);
		}
		for(int cnt = 0; cnt < myRender.Length; cnt++)
		{
			myRender[cnt].material.SetColor("_Color",color[colorIndex]);
		}
	}
	public void Flash()
	{
		if(!_isFlash)
		{
			_isFlash = true;
			StartCoroutine("FlashChange");
		}
	}
	IEnumerator FlashChange()
	{
		for(int cnt = 0; cnt < skinRender.Length; cnt++)
		{
			skinRender[cnt].material.SetColor("_Color1",new Color(1f,1f,1f,1));
		}
		for(int cnt = 0; cnt < myRender.Length; cnt++)
		{
			myRender[cnt].material.SetColor("_Color1",new Color(1f,1f,1f,1));
		}
		yield return new WaitForSeconds(0.1f);
		for(int cnt = 0; cnt < skinRender.Length; cnt++)
		{
			skinRender[cnt].material.SetColor("_Color1",new Color(0.6f,0.6f,0.6f,1));
		}
		for(int cnt = 0; cnt < myRender.Length; cnt++)
		{
			myRender[cnt].material.SetColor("_Color1",new Color(0.6f,0.6f,0.6f,1));
		}
		_isFlash = false;
	}
}
