using UnityEngine;
using System.Collections;

public class NPCAI : MonoBehaviour 
{
	public NPC       npc   ;
	public string     npcMovement ;
	public bool[]     direction = new bool[5] ; /* 用来判断周围方向有没有障碍物 */
	public Transform[] target ;        /*NPC左上和右下的位置 */
	private Transform _thisTransform ;
	public Collider[] c ;
	void Start () 
	{
		_thisTransform = this.transform ;
		c = Physics.OverlapSphere(_thisTransform.position,15,1<<15) ;
	}
	void Update () 
	{
		if(UIManager.gameState != UIManager.GameState.run)
		{
			return ;
		}
		ChangeDirection() ;
		switch(npcMovement)
		{
		case "FM":
			if(PlayerManager.playerTransform!=null)
			{
				Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*2.0f) ;
				_thisTransform.Translate(Vector3.forward*Time.deltaTime * npc.moveSpeed) ;
			}
			break;
		case "LF":
			Quaternion rotate = Quaternion.LookRotation(target[0].position-_thisTransform.position) ;
			_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*2.0f) ;
			_thisTransform.Translate(Vector3.forward*Time.deltaTime * npc.moveSpeed) ;
			break;
		case "RF":
			Quaternion rotate1 = Quaternion.LookRotation(target[1].position-_thisTransform.position) ;
			_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate1.eulerAngles.y,0),Time.deltaTime*2.0f) ;
			_thisTransform.Translate(Vector3.forward*Time.deltaTime * npc.moveSpeed) ;
			break;
		}
	}
	void OnEnable()
	{
		this.InvokeRepeating("ResetDirection",0,5f) ;
	}
	void OnDisable()
	{
		this.CancelInvoke("ResetDirection") ;
		ResetDirection() ;
	}
	/// <summary>
	/// 重置方向数据.
	/// </summary>
	void ResetDirection()
	{
		for(int i=0;i<direction.Length;i++)
		{
			direction[i] = false ;
		}
	}
	void ChangeDirection()
	{
		if(npc.npcDis <= npc.stopDistance)
		{
			npcMovement = "Stop" ;
		}
		else
		{
			if(direction[0])
			{
				if(!direction[3]&&!direction[1])
				{
					npcMovement = "LF" ;
				}
				else if(!direction[4]&&!direction[2])
				{
					npcMovement = "RF" ;
				}
				else
				{
					npcMovement = "Stop" ;
				}
			}
			else
			{
				if(direction[1])
				{
					npcMovement = "RF" ;
				}
				else if(direction[2])
				{
					npcMovement = "LF" ;
				}
				else if(direction[3])
				{
					npcMovement = "RF" ;
				}
				else if(direction[4])
				{
					npcMovement = "LF" ;
				}
			}
		}
		
	}
	void OnTriggerEnter(Collider other )
	{
		switch(other.gameObject.name)
		{
		case "front":
			other.transform.parent.GetComponent<NPCAI>().direction[0]=true;
			break;
		case "left":
			other.transform.parent.GetComponent<NPCAI>().direction[1]=true;
			break;
		case "right":
			other.transform.parent.GetComponent<NPCAI>().direction[2]=true;
			break;
		case "leftfront":
			other.transform.parent.GetComponent<NPCAI>().direction[3]=true;
			break;
		case "rightfront":
			other.transform.parent.GetComponent<NPCAI>().direction[4]=true;
			break;
		}
	}
	void OnTriggerExit(Collider other )
	{
		switch(other.gameObject.name)
		{
		case "front":
			other.transform.parent.GetComponent<NPCAI>().direction[0]=false;
			break;
		case "left":
			other.transform.parent.GetComponent<NPCAI>().direction[1]=false;
			break;
		case "right":
			other.transform.parent.GetComponent<NPCAI>().direction[2]=false;
			break;
		case "leftfront":
			other.transform.parent.GetComponent<NPCAI>().direction[3]=false;
			break;
		case "rightfront":
			other.transform.parent.GetComponent<NPCAI>().direction[4]=false;
			break;
		}
	}
}
