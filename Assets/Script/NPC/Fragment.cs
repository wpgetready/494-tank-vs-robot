using UnityEngine;
using System.Collections;

public class Fragment : MonoBehaviour
{
	void OnEnable()
	{
		this.Invoke("DestroyThis",3) ;
	}
	
	void DestroyThis () 
	{
		ObjectBuffer.DestroyBufferObject(this.gameObject) ;
	}
}
