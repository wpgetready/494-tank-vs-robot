using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour
{
	public NPCAI	npcAI ;
	public MaterialFlash	flash ;
	public BulletFlower	bulletFlower ;
	public int	coinNum = 1 ;
	public float		moveSpeed = 6 ;
	public float		stopDistance = 30 ;
	public int	npc_id;//用于确定NPC类型
	public float		damage;//NPC对玩家的伤害
	public float		npcDis = 1000 ;        // NPC到玩家的距离 
	public float		maxHealth ;     // NPC的生命值 
	public Transform	explosions ;
	public GameObject[]	deadlySpecialEffect ;
	public Animation	mAnima ;
	public bool		inScreen ;
	public int boss ;
	
	private Vector3	_screenPos ;
	private bool[]	_once = new bool[2] ;
	private float	_currentHealth ;
	private Transform	_thisTransform ;
	private float	bombTime ;
	
	//for boss
	
	public Transform[] bulletPosL ;
	public Transform[] bulletPosR ;
	public Transform[] guidePosL ;
	public Transform[] guidePosR ;
	public MuzzleFlashAnimate[] muzzle ;
	private int bulletIndex ;
	private int guideIndex ;
	private float[] fireTime = new float[3] ;
	//private float   Distance = 1000 ;
	//private bool[]    _once = new bool[3] ;
	private MaterialFlash _flash ;
	//
	
	public void InitData(int index,int npc_id,float blood,float damage,int boss)
	{
		this.index = index;
		this.npc_id = npc_id;
		this.maxHealth = blood;
		this.damage = damage;
		this._currentHealth = blood;
		this.boss = boss;
	}
	
	void Start ()
	{
		
	}
	public int index = -1;
	//private float gameModel ;
	//private float bossBlood ;
	void OnEnable()
	{
		/*
		if(UIManager.gameModeVal==1)
		{
			gameModel = 1.5f ;
		}
		else if(UIManager.gameModeVal==0)
		{
			gameModel = 1f ;
		}
		if(isBoss)
		{
			bossBlood = 2;
		}
		else
		{
			bossBlood = 1;
		}
		*/
		/*
		if(type==0)
		{
			maxHealth = NPCAttribute.NpcBlood01[NPCManager.currentWaveNum-1]*gameModel*bossBlood ;
		}
		else if(type==1)
		{
			maxHealth = NPCAttribute.NpcBlood02[NPCManager.currentWaveNum-1]*gameModel*bossBlood ;
		}
		else if(type==2)
		{
			maxHealth = NPCAttribute.NpcBlood03[NPCManager.currentWaveNum-1]*gameModel*bossBlood ;
		}
		else if(type==3)
		{
			maxHealth = NPCAttribute.NpcBlood04[NPCManager.currentWaveNum-1]*gameModel*bossBlood ;
		}
		else if(type==4)
		{
			maxHealth = NPCAttribute.NpcBlood05[NPCManager.currentWaveNum-1]*gameModel*bossBlood ;
		}
		*/
		if(_thisTransform==null)
		{
			_thisTransform = this.transform ;
		}
		//for boss
		if(flash==null)
		{
			flash = (MaterialFlash)_thisTransform.GetComponent<MaterialFlash>() ;
		}
		//
		_currentHealth = maxHealth ;
		
		//index = NPCManager.AddSceneNpc(this) ;
		this.InvokeRepeating("DistanceAndPlayer",0,0.5f) ;
		Init() ;
		flash.ChangeColor(0) ;
	}
	void OnDisable()
	{
		this.CancelInvoke("DistanceAndPlayer") ;
		//不是通过InitData初始化的对象不归NPCManager管理,也就不用在SceneNPC中删掉
		if(this.npc_id != 0 && this.index != -1)
		{
			NPCManager.RemoveSceneNpc(index) ;
		}
		//if(type==4)
		if(npc_id==5)
		{
			audio.loop = false ;
		}
		if(_bloodBar!=null)
		{
			GameObject.Destroy(_bloodBar.gameObject);
		}
		if(_npcDire!=null)
		{
			GameObject.Destroy(_npcDire.gameObject);
		}
		isShowBlood = false ;
		showBloodTime = 0 ;
		Reset() ;
	}
	void OnDestroy()
	{
		if(_bloodBar!=null)
		{
			GameObject.Destroy(_bloodBar.gameObject);
		}
		if(_npcDire!=null)
		{
			GameObject.Destroy(_npcDire.gameObject);
		}
	}
	
	void Init()
	{
		for(int i=0;i<_once.Length;i++)
		{
			_once[i] = false ;
		}
		//if(type == 4)
		if(npc_id == 5)
		{
			mAnima.Play("walk");
		}
		else if(npc_id == 4)//else if(type == 3)
		{
			bombTime = Time.time + 10;
		}
		
	}
	
	public int PropIdToObjectBufferId(int prop_id)
	{
		switch(prop_id)
		{
		case 1:
			//Coin
			return 8;
		case 2:
			//FixBox
			return 9;
		case 3:
			//FirePower
			return 20;
		case 4:
			//Magnet
			return 21;
		case 5:
			//Shield
			return 22;
		case 6:
			//Speed
			return 23;
		}
		return -1;
	}
	
	void InitProp()
	{
		Prop prop = new Prop();
		prop = NpcPropConfig.Instance.RandomNpcProp(npc_id);
		if(prop == null)
		{
			//Boss不掉落物品
			return;
		}
		if(prop.num == 1)
		{	
			ObjectBuffer.InstantiateObject(PropIdToObjectBufferId(prop.prop_id),_thisTransform.position,Quaternion.identity);
		}
		else if(prop.num >1)
		{
			//多个道具掉落坐标散开
			for(int i=0;i<prop.num;i++)
			{
				ObjectBuffer.InstantiateObject(PropIdToObjectBufferId(prop.prop_id),_thisTransform.position+new Vector3(Random.Range(-5,5),0,Random.Range(-5,5)),Quaternion.identity);
			}
		}
	}
	
	
	
	void DeadSpecialEffect()
	{
		GameObject obj = NGUITools.AddChild(UIManager.SpecialEffect,deadlySpecialEffect[Random.Range(0,2)]);
		obj.GetComponent<SpecialEffect>().createPos = _thisTransform.position ;
	}
	void ApplyPlayDamage(int damage)
	{
		//Debug.Log("ApplyPlayDamage:"+damage);
		//Debug.Log("_currentHealth:"+_currentHealth);
		if(damage<=0)
		{
			damage = 1;
		}
		_currentHealth -= damage ;
		flash.Flash() ;
		isShowBlood = true ;
		showBloodTime = 0 ;
		if(_currentHealth<=0)
		{
			//if(type==4)
			if(npc_id == 5)
			{
				if(npcDis<=20)
				{
					//当TNT类型坦克爆炸时,如果player在TNT爆炸范围内,则也会受到TNT爆炸时候的伤害
					//float _currentDamage = damage -PlayerManager.playerAttribute[PlayerManager.currentTank].armorPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel];
					float _currentDamage = damage - UserData.Instance.player_attribute.armor;
					PlayerManager.ApplyNPCDamage(_currentDamage) ;
				}
			}
			ChangeUnitsNum() ;
			DeadSpecialEffect() ;
			Instantiate(explosions,_thisTransform.position+new Vector3(0,5,0),explosions.rotation) ;
			InitProp() ; 
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
//			if(UIManager.gameQuality==0)
				ObjectBuffer.InstantiateObject(12,_thisTransform.position,Quaternion.Euler(-90,0,0)) ;
		///	if(UIManager.gameQuality==0)
				ObjectBuffer.InstantiateObject(Random.Range(28,30),_thisTransform.position,Quaternion.Euler(-90,0,0)) ;
			PlayerManager.AddScore(10) ;
		}
	}
	
	/// <summary>
	/// 显示血条
	/// </summary>
	public GameObject   bloodBarPrefab ;
	private Transform  _bloodBar ;
	private bool       isShowBlood ;
	private float      showBloodTime ;
	private void ShowBloodBar()
	{
		if(_bloodBar==null)
		{
			_bloodBar = NGUITools.AddChild(UIManager.SpecialEffect,bloodBarPrefab).transform;
		}
		Vector3 screenPos = Camera.main.WorldToScreenPoint(_thisTransform.position) ;
		Vector3 Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(screenPos);
		_bloodBar.position = new Vector3(Pos.x,Pos.y,1) ;
		_bloodBar.localPosition += new Vector3(0,50,0) ;

		if(boss==1)
		{
			_bloodBar.localScale = new Vector3(_currentHealth/maxHealth*50f,10,1) ;
		}
		else
		{
			_bloodBar.localScale = new Vector3(_currentHealth/maxHealth*50f,5,1) ;
		}
		NGUITools.SetActive(_bloodBar.gameObject,true);
	}
	private void CloseBloodBar()
	{
		NGUITools.SetActive(_bloodBar.gameObject,false);
	}
	/// <summary>
	/// 确定屏幕外NPC方向
	/// </summary>
	public GameObject  npcDirePrefab ;
	private Transform _npcDire ;
	void ShowDirection()
	{
		_screenPos = Camera.main.WorldToScreenPoint(_thisTransform.position) ;
		Vector3 Pos = new Vector3();
		if(_screenPos.x>0&&_screenPos.x<Screen.width&&_screenPos.y>0&&_screenPos.y<Screen.height)
		{
			if(_npcDire!=null)
			{
				if(_npcDire.gameObject.active)
					NGUITools.SetActive(_npcDire.gameObject,false);
			}
			inScreen = true ;
		}
		else
		{
			inScreen = false ;
			if(_npcDire==null)
			{
				_npcDire = NGUITools.AddChild(UIManager.SpecialEffect,npcDirePrefab).transform;
			}
			if(!_npcDire.gameObject.active)
				NGUITools.SetActive(_npcDire.gameObject,true);
			
			if(_screenPos.x<0&&_screenPos.y<Screen.height&&_screenPos.y>0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(0,_screenPos.y,0));
				_npcDire.localEulerAngles = new Vector3(0,0,-90) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.x>Screen.width&&_screenPos.y<Screen.height&&_screenPos.y>0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(Screen.width,_screenPos.y,0));
				_npcDire.localEulerAngles = new Vector3(0,0,90) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.y<0&&_screenPos.x<Screen.width&&_screenPos.x>0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(_screenPos.x,0,0));
				_npcDire.localEulerAngles = new Vector3(0,0,0) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.y>Screen.height&&_screenPos.x<Screen.width&&_screenPos.x>0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(_screenPos.x,Screen.height,0));
				_npcDire.localEulerAngles = new Vector3(0,0,180) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.x<=0&&_screenPos.y<=0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(0,0,0));
				_npcDire.localEulerAngles = new Vector3(0,0,-45) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.x>=Screen.width&&_screenPos.y<=0)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(Screen.width,0,0));
				_npcDire.localEulerAngles = new Vector3(0,0,45) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.x<=0&&_screenPos.y>=Screen.height)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(0,Screen.height,0));
				_npcDire.localEulerAngles = new Vector3(0,0,-135) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
			if(_screenPos.x>=Screen.width&&_screenPos.y>=Screen.height)
			{
				Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0));
				_npcDire.localEulerAngles = new Vector3(0,0,135) ;
				_npcDire.position = new Vector3(Pos.x,Pos.y,1) ;
			}
		}
	}
	
	/// <summary>
	/// 当为小boss的时候改变属性.
	/// </summary>
	private bool  isBoss ;
	
	public void ChangeAttribute(Vector3 scale)
	{
		isBoss = true ;
		flash.ChangeColor(Random.Range(1,5)) ;
		if(_thisTransform==null)
			_thisTransform = this.transform ;
		_thisTransform.localScale = scale ;
		//if(type!=4)
		if(npc_id != 5)
		{
			bulletFlower.paraIndex = 1 ;
		}
	}
	private void Reset()
	{
		if(isBoss)
		{
			isBoss = false ;
			_thisTransform.localScale = Vector3.one ;
			//if(type!=4)
			if(npc_id != 5)
			{
				bulletFlower.paraIndex = 0 ;
			}
		}
	}
	void DistanceAndPlayer()
	{
		if(PlayerManager.playerTransform!=null)
		{
			npcDis = Vector3.Distance(_thisTransform.position,PlayerManager.playerTransform.position) ;
			if(npcDis <= stopDistance)
			{
				//for boss
				if(boss == 1)
				{
					_once[1] = false ;
					_once[0] = false ;
					bossState = BossState.Shoot ;
				}
				else
				{
					//if(type == 4)
					if(npc_id == 5)
					{
						if(!_once[0])
						{
							_once[0] = true ;
							mAnima.Play("stop") ;
							if(UserData.Instance.user_setting.sound_volume>0)
							{
								audio.loop = true ;
								audio.Play() ;
							}
							this.Invoke("TNTExplodeWait",0.5f) ;
						}
					}
				}
			}
			else
			{
				
				//for boss
				if(boss ==1)
				{
					if(npcDis<=40)
					{
						_once[1] = true ;
						if(!mAnima.IsPlaying("attack"))
						{
							_once[0] = false ;
							bossState = BossState.Move ;
						}
					}
					else
					{
						_once[1] = true ;
						if(!mAnima.IsPlaying("attack"))
						{
							bossState = BossState.Fly ;
						}
					}
				}
				else
				{
					//if(type == 4)
					if(npc_id == 5)
					{
						if(!_once[0])
							npcAI.npcMovement = "FM" ;
						if(UserData.Instance.user_setting.sound_volume>0)
							audio.Play() ;
					}
					else
					{
						npcAI.npcMovement = "FM" ;
					}
				}
			}
		}
	}

	public Transform tntExplode ;
	private void TNTExplodeWait()
	{
		mAnima.Play("explode") ;
		if(gameObject.active)
			this.Invoke("TNTExplode",0.5f) ;
	}
	private void TNTExplode()
	{
		if(npcDis<=20)
		{
			//当TNT类型坦克爆炸时,如果player在TNT爆炸范围内,则也会受到TNT爆炸时候的伤害
			//float _currentDamage = damage -PlayerManager.playerAttribute[PlayerManager.currentTank].armorPercentage[PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel];
			float _currentDamage = damage - UserData.Instance.player_attribute.armor;
			PlayerManager.ApplyNPCDamage(_currentDamage) ;
		}
		Instantiate(tntExplode,_thisTransform.position+new Vector3(0,5,0),tntExplode.rotation) ;
		ChangeUnitsNum() ;
		ObjectBuffer.DestroyBufferObject(this.gameObject) ;
	//	if(UIManager.gameQuality==0)
			ObjectBuffer.InstantiateObject(12,_thisTransform.position,Quaternion.Euler(-90,0,0)) ;
	//	if(UIManager.gameQuality==0)
			ObjectBuffer.InstantiateObject(28,_thisTransform.position,Quaternion.Euler(0,0,0)) ;
	}
	
	/// <summary>
	/// 改变兵种图标数量
	/// </summary>
	private void ChangeUnitsNum()
	{
		if(gameObject.active)
		{
			NPCManager.currentSceneNpcSum -- ;
			NPCManager.leftNpcNum[npc_id] = (int)(NPCManager.leftNpcNum[npc_id]) - 1;
			
		}
		
	}
	void Update ()
	{
		if(UIManager.gameState != UIManager.GameState.run)
		//if(UIManager.gameState != UIManager.GameState.run||PlayerManager.playerTransform==null)
		{
			//if(type==4)
			if(npc_id == 5)
			{
				if(audio.isPlaying)
					audio.Pause() ;
			}
			if(bulletFlower!=null)
				bulletFlower.enabled = false ;
			return ;
		}
		
		if(isShowBlood)
		{
			showBloodTime += Time.deltaTime ;
			if(showBloodTime>3)
			{
				CloseBloodBar() ;
				isShowBlood  = false ;
			}
			else
			{
				ShowBloodBar() ;
			}
		}

		ShowDirection() ;

		if(boss == 0)
		{
			if(npcDis<=50)
			{
				if(bulletFlower!=null)
					bulletFlower.enabled = true ;
				//if(type == 3)
				if(npc_id == 4)
				{
					if(Time.time>bombTime)
					{
						bombTime = Time.time + 30 ;
						ObjectBuffer.InstantiateObject(11,_thisTransform.position,_thisTransform.rotation) ;
					}
				}
				
			}
		}
		
		if(boss == 1)
		{
			switch(bossState)
			{
			case BossState.Fly:
				muzzle[0].fireRender.enabled = false ;
				muzzle[1].fireRender.enabled = false ;
				if(!mAnima.IsPlaying("fly")&&!_once[0])
				{
					_once[0] = true ;
					mAnima.Play("fly") ;
				}
				if(npcDis<=100)
				{
					Shoot3() ;
				}
				Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*10.0f) ;
				_thisTransform.Translate(Vector3.forward*Time.deltaTime * 40) ;
				break;
			case BossState.Move:
				muzzle[0].fireRender.enabled = false ;
				muzzle[1].fireRender.enabled = false ;
				if(!mAnima.IsPlaying("walk"))
				{
					mAnima.Play("walk") ;
				}
				Shoot3() ;
				Quaternion rotate1 = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate1.eulerAngles.y,0),Time.deltaTime*4.0f) ;
				_thisTransform.Translate(Vector3.forward*Time.deltaTime * 10) ;
				break;
			case BossState.Shoot:
				if(!mAnima.IsPlaying("attack")&&!_once[1])
				{
					mAnima.Play("attack") ;
				}
				else
				{
					muzzle[0].fireRender.enabled = true ;
					muzzle[1].fireRender.enabled = true ;
					Shoot1() ;
					Shoot2() ;
				}
				Quaternion rotate2 = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
				_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate2.eulerAngles.y,0),Time.deltaTime*10.0f) ;
	
				break;
			}
		}
	}
	
	void Shoot1()
	{
		if(Time.time>=fireTime[0])
		{
			fireTime[0] = Time.time+0.2f ;
			GameObject objBulletL = ObjectBuffer.InstantiateObject(26,bulletPosL[bulletIndex].position,bulletPosL[bulletIndex].rotation) ;
			objBulletL.GetComponent<Bullet>().setDamage(damage);
			GameObject objBulletR = ObjectBuffer.InstantiateObject(26,bulletPosR[bulletIndex].position,bulletPosR[bulletIndex].rotation) ;
			objBulletR.GetComponent<Bullet>().setDamage(damage);
			
			if(bulletIndex>=bulletPosL.Length-1)
			{
				bulletIndex = 0 ;
			}
			else
			{
				bulletIndex ++ ;
			}
			
		}
	}
	void Shoot2()
	{
		if(Time.time>=fireTime[1])
		{
			fireTime[1] = Time.time+1.5f ;
			GameObject objBulletL = ObjectBuffer.InstantiateObject(24,guidePosL[guideIndex].position,guidePosL[guideIndex].rotation) ;
			objBulletL.GetComponent<Bullet>().setDamage(damage);
			GameObject objBulletR = ObjectBuffer.InstantiateObject(24,guidePosR[guideIndex].position,guidePosR[guideIndex].rotation) ;
			objBulletR.GetComponent<Bullet>().setDamage(damage);
			
			if(guideIndex>=guidePosL.Length-1)
			{
				guideIndex = 0 ;
			}
			else
			{
				guideIndex ++ ;
			}
		}
	}
	void Shoot3()
	{
		if(Time.time>=fireTime[2])
		{
			fireTime[2] = Time.time+1.5f ;
			GameObject objBulletL = ObjectBuffer.InstantiateObject(25,guidePosL[guideIndex].position,guidePosL[guideIndex].rotation) ;
			objBulletL.GetComponent<Bullet>().setDamage(damage);
			GameObject objBulletR = ObjectBuffer.InstantiateObject(25,guidePosR[guideIndex].position,guidePosR[guideIndex].rotation) ;
			objBulletR.GetComponent<Bullet>().setDamage(damage);
			if(guideIndex>=guidePosL.Length-1)
			{
				guideIndex = 0 ;
			}
			else
			{
				guideIndex ++ ;
			}
		}
	}
	private BossState bossState ;
	enum BossState
	{
		Fly,
		Move,
		Shoot,
		Dead,
	}
}
