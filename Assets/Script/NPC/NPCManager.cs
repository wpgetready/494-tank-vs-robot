using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCManager : MonoBehaviour
{
	public Transform[]	createPos ;
	public AudioClip	bossWain ;
	public GameObject	warnSpecialEffect ;
	public GameObject	bossSpecialEffect ;
	public static bool destroyNPCBullet ;
	
	public static int	currentSceneNpcSum ;      // 场景中NPC的数量 old: npcNum
	public static NPC[] SceneNpc =  new NPC[30] ;  // 场景中的NPC，用来判断玩家攻击的目标 
	public static int	currentWaveNum = 1 ;
	public static bool  stopSentRay ;
	public static int	npcUpperLimit = 30 ;
	public static List<NpcInfo> npc_info_list;
	public static Hashtable leftNpcNum;
	
	void Start ()
	{
		leftNpcNum = new Hashtable();
		npc_info_list = new List<NpcInfo>();
		currentSceneNpcSum = 0 ;
		currentWaveNum = 1 ;
		enemyState = EnemyState.NPCUnits ;
//		ChangeScene();
	}
	public static EnemyState enemyState = EnemyState.NPCUnits ;
	private bool   warnTip = true ;
	private int[] npcUnits ; // 这一波将出现的NPC , replaced by npc_info_list
	private int   minBossNum ;
	private int    currentWaveCreatedNpcSum = 0 ;//当前波次已经创建的Npc数量 old: _createNPCSum
	void Update () 
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			if(changeAmbient)
			{
				AmbientLight() ;
			}
			switch(enemyState)
			{
			case EnemyState.NPCUnits:
				UIManager.bossInvincible = false ;
				UIManager.currentLevelInvincible = false ;
				NPCManager.destroyNPCBullet = false ;
				
				currentWaveCreatedNpcSum = 0 ;
				waveNpcNum = GameLevel.current_sub_level_info.npc_num;//每波出怪数
				//初始化该波次的NPC,放到List中
				npc_info_list = null;
				npc_info_list = new List<NpcInfo>();
				foreach(DictionaryEntry item in GameLevel.current_sub_level_info.npcs)
				{
					NpcInfo npc = item.Value as NpcInfo;
					for(int i=0;i<npc.num;i++)
					{
						npc_info_list.Add(npc);
						if(leftNpcNum[npc.npc_id]==null)
						{
							leftNpcNum[npc.npc_id] = 1;
						}
						else
						{
							leftNpcNum[npc.npc_id] = (int)leftNpcNum[npc.npc_id]+1;
						}
					}
				}
				//对List中的NPC出场顺序做Shuffle
				Utils.Shuffle<NpcInfo>(npc_info_list);
				
				UIManager.isShowWaveTip = true ;
				warnTip = true ;
				enemyState = EnemyState.CNPC ;
				
				PlayerManager.tankInvincible = false ;
				break ;
			case EnemyState.CNPC:
				CreateNpc() ;
				if(warnTip&&currentSceneNpcSum>0)
				{
					warnTip = false ;
					NGUITools.AddChild(UIManager.SpecialEffect,warnSpecialEffect);
				}
				GetPlayerTarget() ;
				break ;
			case EnemyState.WaitNextWave:
				if(isAllowNextWave())
				{
					
					if(currentWaveNum>=GameLevel.current_sub_level_info.wave_num)
					{
						//如果当前波次已经达到当前sublevel的最大波次，则判断是否本关通过
						enemyState = EnemyState.WaitResult;
					}
					else
					{
						currentWaveNum++;
						NPCManager.enemyState = NPCManager.EnemyState.NPCUnits ;
					}
				}
				GetPlayerTarget();
				break;
			case EnemyState.WaitResult:
				if(isSceneNpcAllKilled())
				{
					UIManager.level[currentWaveNum] = 1 ;
				
					this.Invoke("GameOver",5) ;
					enemyState = EnemyState.Win ;
					NPCManager.destroyNPCBullet = true ;
				}
				else
				{
					GetPlayerTarget();
				}
				break;
			case EnemyState.WaitNPC:
				if(GetNpcNum())
				{
					if(currentWaveNum>=7)
					{
						UIManager.PlayMusic(bossWain) ;
						NGUITools.AddChild(UIManager.SpecialEffect,bossSpecialEffect);
						enemyState = EnemyState.CBoss ;
					}
					else
					{
						NPCManager.currentWaveNum ++ ;
						NPCManager.enemyState = NPCManager.EnemyState.NPCUnits ;
					}
					
				}
				GetPlayerTarget() ;
				break ;
			case EnemyState.CBoss:
				UIManager.bossInvincible = true ;
				this.Invoke("OverBossInvi",2);
			
				CreateBoss() ;
				enemyState = EnemyState.WaitBoss ;
				break;
			case EnemyState.WaitMinBoss:
				
				if(GetNpcNum())
				{
					PlayerManager.tankInvincible = true ;
					enemyState = EnemyState.AddWave ;
				}
				GetPlayerTarget() ;
				break ;
			case EnemyState.WaitBoss:
				if(!BossObj.active)
				{
					PlayerManager.tankInvincible = true ;
					enemyState = EnemyState.AddWave ;
					PlayerManager.playerTarget = null ;
				}
				break;
			case EnemyState.AddWave:
				GetStarNum() ;
				if(UIManager.levelStarNum[NPCManager.currentWaveNum-1]>0)
				{
					if(currentWaveNum<50)
						UIManager.level[currentWaveNum] = 1 ;
				}
				this.Invoke("GameOver",3) ;
				
				enemyState = EnemyState.Win ;
				NPCManager.destroyNPCBullet = true ;
				break;
			}
		}
	}

	void OverBossInvi()
	{
		UIManager.bossInvincible = false ;
	}
	void GameOver()
	{
		UIManager.UIManagerScript.ShowGameWin(this.transform.localPosition.z-25) ;
	}

	private void GetStarNum()
	{
		if(PlayerManager.playerHP>90)
		{
			UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] = 3 ;
		}
		else if(PlayerManager.playerHP>70)
		{
			UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] = 2 ;
		}
		else if(PlayerManager.playerHP>40)
		{
			UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] = 1 ;
		}
		else
		{
			UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] = 0 ;
		}
		if(UIManager.gameModeVal==0)
		{
			if(UIManager.levelStarNum[NPCManager.currentWaveNum-1]<UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1])
				UIManager.levelStarNum[NPCManager.currentWaveNum-1] = UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] ;
		}
		else if(UIManager.gameModeVal==1)
		{
			if(UIManager.levelStarNum02[NPCManager.currentWaveNum-1]<UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1])
				UIManager.levelStarNum02[NPCManager.currentWaveNum-1] = UIManager.oneLevelStarNum[NPCManager.currentWaveNum-1] ;
		}
	}
	
	public float nextWaveTime;
	public bool isAllowNextWave()
	{
		if( Time.time > nextWaveTime && nextWaveTime > 0)
		{
			nextWaveTime = Time.time+GameLevel.current_sub_level_info.wave_interval;
			return true;
		}
		else if(nextWaveTime == 0)
		{
			nextWaveTime = Time.time+GameLevel.current_sub_level_info.wave_interval;
		}
		return false;
	}
	//该函数判断场景中是否已经没有了Npc
	private bool GetNpcNum()
	{
		for(int i=1;i<npcUpperLimit;i++)
		{
			if(SceneNpc[i]!=null)
			{
				return false ;
			}
		}
		return true ;
	}
	//该函数判断场景中是否已经没有了Npc
	public bool isSceneNpcAllKilled()
	{
		//for(int i=1;i<npcUpperLimit;i++)
		for(int i=0;i<npcUpperLimit;i++)
		{
			if(SceneNpc[i]!=null)
			{
				return false ;
			}
		}
		return true ;
	}
	public Ray ray  ;
	public RaycastHit hit;
	public Transform hitArrowsPrefab ;
	private Transform hitArrows ;
	private void GetPlayerTarget()
	{
		if(Input.touchCount>1)
		{
			if(Input.GetTouch(1).phase == TouchPhase.Began)
			{
				ray = Camera.main.ScreenPointToRay(Input.GetTouch(1).position);
				if (Physics.Raycast(ray, out hit, 100,1<<15))
				{
					if(hit.collider.gameObject.tag=="Enemy")
					{
						if(hitArrows==null)
							hitArrows = Instantiate(hitArrowsPrefab) as Transform ;
						hitArrows.gameObject.SetActiveRecursively(true) ;
						hitArrows.parent = hit.transform ;
						hitArrows.localPosition = new Vector3(0,9,0);
						UIManager.selectPlayerTarget = hit.transform ;
					}
				}
			}
		}
		else if(Input.touchCount>0)
		{
			if(Input.GetTouch(0).phase == TouchPhase.Began)
			{
				ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
				if (Physics.Raycast(ray, out hit, 100,1<<15))
				{
					if(hit.collider.gameObject.tag=="Enemy")
					{
						if(hitArrows==null)
							hitArrows = Instantiate(hitArrowsPrefab) as Transform ;
						hitArrows.gameObject.SetActiveRecursively(true) ;
						hitArrows.parent = hit.transform ;
						hitArrows.localPosition = new Vector3(0,9,0);
						UIManager.selectPlayerTarget = hit.transform ;
					}
				}
			}
				
		}
			
		
		if(UIManager.selectPlayerTarget!=null)
		{
			if(!UIManager.selectPlayerTarget.gameObject.active)
			{
				UIManager.selectPlayerTarget = null ;
				hitArrows.parent = null ;
				hitArrows.gameObject.SetActiveRecursively(false) ;
			}
			else
			{
				PlayerManager.playerTarget = UIManager.selectPlayerTarget ;
			}
			return ;
		}
		int idx = GetNearestNPCIdx();
		if(idx != -1)
		{
			PlayerManager.playerTarget = SceneNpc[idx].transform;
		}
		else
		{
			PlayerManager.playerTarget = null;
		}
	}
	
	public int GetNearestNPCIdx()
	{
		int idx = 0;
		for(int i=1;i<npcUpperLimit;i++)
		{
			if(SceneNpc[i]!=null&&SceneNpc[idx]!=null)
			{
				if(SceneNpc[idx].npcDis-SceneNpc[i].npcDis>3)
				{
					idx=i;
				}
			}
			if(SceneNpc[i]!=null&&SceneNpc[idx]==null)
			{
				idx = i;
			}
		}
		if(SceneNpc[idx]!=null)
		{
			if(SceneNpc[idx].inScreen)
			{
				return idx;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return -1;
		}
	}
	
	public static int AddSceneNpc(NPC npc)
	{
		for(int i=0;i<npcUpperLimit;i++)
		{
			if(NPCManager.SceneNpc[i]==null)
			{
				NPCManager.SceneNpc[i] = npc ;
				return i ;
			}
		}
		return -1 ;
	}
	
	public static void RemoveSceneNpc(int index)
	{
		if(SceneNpc[index]!=null)
		{
			SceneNpc[index] = null ;
		}
	}
	
	private int   waveNpcNum;
	public static GameObject BossObj ;
	
	private bool changeAmbient ;
	private Color ambientValue ;
	private float ambientTime = 3 ;
	private void AmbientLight()
	{
		RenderSettings.ambientLight = new Color(Mathf.Lerp(RenderSettings.ambientLight.r,ambientValue.r,Time.deltaTime*0.5f),
			                                       Mathf.Lerp(RenderSettings.ambientLight.g,ambientValue.g,Time.deltaTime*0.5f),
			Mathf.Lerp(RenderSettings.ambientLight.b,ambientValue.b,Time.deltaTime*0.5f));
		ambientTime -= Time.deltaTime ;
		if(ambientTime<0)
		{
			ambientTime = 3 ;
			changeAmbient = false ;
		}
	}
	
	public int NpcIdToObjectBufferId(int npc_id)
	{
		switch(npc_id)
		{
		case 1:
			return 0;
		case 2:
			return 1;
		case 3:
			return 2;
		case 4:
			return 3;
		case 5:
			return 4;
		case 101:
		case 102:
		case 103:
		case 104:
		case 105:
		case 106:
		case 107:
			return npc_id-100+35;
		}
		return -1;
	}
	
	/// <summary>
	/// 创建NPC.
	/// </summary>
	private float nextCreate ;  //  下次创建NPC时间
	private float creatTime = 1f ;
	private void CreateNpc()
	{
		if(Time.time > nextCreate&&currentSceneNpcSum<npcUpperLimit)
		{
			nextCreate = Time.time + creatTime ;
			int index = Random.Range(0,createPos.Length) ;
			NpcInfo npc_info = npc_info_list[currentWaveCreatedNpcSum];
			
			GameObject obj = ObjectBuffer.InstantiateObject(NpcIdToObjectBufferId(npc_info.npc_id),createPos[index].position,createPos[index].rotation) ;
			NPC npc = obj.GetComponent<NPC>();
			//以下两步操作替代以前的在NPC.cs中的AddSceneNpc(this)操作
			int idx = NPCManager.AddSceneNpc(npc);
			SceneNpc[idx].InitData(idx,npc_info.npc_id,npc_info.blood,npc_info.damage,npc_info.boss);

			currentSceneNpcSum ++ ;
			currentWaveCreatedNpcSum ++ ;
			if((float)currentWaveCreatedNpcSum/waveNpcNum>0.8f)
			{
				changeAmbient = true ;
				ambientValue = new Color(0.3f,0.3f,1);
			}
			else if((float)currentWaveCreatedNpcSum/waveNpcNum>0.6f)
			{
				changeAmbient = true ;
				ambientValue = new Color(0.6f,0.4f,0);
			}
			else if((float)currentWaveCreatedNpcSum/waveNpcNum>0.4f)
			{
				changeAmbient = true ;
				ambientValue = new Color(1f,1f,1f);
			}
			else if((float)currentWaveCreatedNpcSum/waveNpcNum>0.2f)
			{
				changeAmbient = true ;
				ambientValue = new Color(1f,0.8f,0.6f);
			}
			else if((float)currentWaveCreatedNpcSum/waveNpcNum>0f)
			{
				changeAmbient = true ;
				ambientValue = new Color(0.9f,0.4f,0f);
			}
			//该波次的Npc都已经出完,则等待下一波Npc
			if(currentWaveCreatedNpcSum >= waveNpcNum)
			{
				enemyState = EnemyState.WaitNextWave;
			}
		}
	}
	/// <summary>
	/// 创建Boss.
	/// </summary>
	private void CreateBoss()
	{
		int index = Random.Range(0,createPos.Length) ;
		BossObj = ObjectBuffer.InstantiateObject(5,createPos[index].position,createPos[index].rotation) ;
		PlayerManager.playerTarget = BossObj.transform ;
	}
	
	public enum EnemyState
	{
		AddWave,
		NPCUnits,
		CNPC,
		WaitNPC,
		WaitNextWave,
		CBoss,
		WaitBoss,
		WaitMinBoss,
		WaitResult,
		Win,
	}
}
