using UnityEngine;
using System.Collections;

public class DeathFlash : MonoBehaviour 
{
	public UISprite deadthSprite ;
	
	private float a ;
	void Update ()
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			if(PlayerManager.playerHP<=0)
			{
				deadthSprite.alpha = 0 ;
			}
			else if(PlayerManager.playerHP<20)
			{
				a = Mathf.PingPong(Time.time*3,1) ;
				deadthSprite.alpha = a ;
			}
			else
			{
				deadthSprite.alpha = 0 ;
			}
		}
		else if(UIManager.gameState == UIManager.GameState.GameOver)
		{
			deadthSprite.alpha = 0 ;
		}
		
	}
}
