using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;

public class MoneyGold
{
	public int id;
	public string name;
	public int gold;
	public double money;
	public string money_unit;
	public string gold_unit;
	public double usd;
	public string product_id;
	public string desc;
}

public class MoneyGoldConfig {

	public Hashtable money_gold_list;

	private static MoneyGoldConfig instance = new MoneyGoldConfig();
	
	public static MoneyGoldConfig Instance
	{
		get
		{
			return instance;
		}
	}
	
	public bool Load()
	{
		
		string xmlRaw = Utils.LoadConfig("Config/"+GameConfig.Instance.lang+"/MoneyGolds.xml");
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(xmlRaw);

		XmlNode root = xmlDoc.ChildNodes.Item(1);
		money_gold_list = new Hashtable();
		for(int i=0;i<root.ChildNodes.Count;i++)
		{
			XmlNode money_gold_node = root.ChildNodes.Item(i);
			MoneyGold mg = new MoneyGold();
			mg.id = int.Parse(money_gold_node.Attributes["Id"].Value);
			mg.name = money_gold_node.Attributes["Name"].Value.ToString();
			mg.gold = int.Parse(money_gold_node.Attributes["Gold"].Value);
			mg.money = double.Parse(money_gold_node.Attributes["Money"].Value);
			mg.money_unit = money_gold_node.Attributes["MoneyUnit"].Value.ToString();
			mg.gold_unit = money_gold_node.Attributes["GoldUnit"].Value.ToString();
			mg.usd = double.Parse(money_gold_node.Attributes["USD"].Value);
			mg.product_id = money_gold_node.Attributes["ProductId"].Value.ToString();
			mg.desc = money_gold_node.Attributes["Desc"].Value.ToString();
			
			money_gold_list.Add(mg.id,mg);
		}
		return true;
	}
	
	public MoneyGold getMoneyGoldByItemId(int item_id)
	{
		return (MoneyGold)money_gold_list[item_id];
	}
	
	public MoneyGold getMoneyGoldByMoneyCode(int moneycode)
	{
		foreach(DictionaryEntry item in money_gold_list)
		{
			MoneyGold mg = (MoneyGold)item.Value;
			if(moneycode == mg.money)
			{
				return mg;
			}
		}
		return null;
	}
	
	
	public MoneyGold getMoneyGoldByProductId(string product_id)
	{
		foreach(DictionaryEntry item in money_gold_list)
		{
			MoneyGold mg = (MoneyGold)item.Value;
			if(product_id == mg.product_id)
			{
				return mg;
			}
		}
		return null;
	}
}
