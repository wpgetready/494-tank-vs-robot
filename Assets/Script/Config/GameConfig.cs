using UnityEngine;
using System.Collections;

public class GameConfig 
{
	private static GameConfig instance = new GameConfig();
	
	public static GameConfig Instance
	{
		get
		{
			return instance;
		}
	}
	
	public string lang = "cn";//for Config sub-directory & Localization
}

