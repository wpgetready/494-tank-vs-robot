using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;

public class PropLevel
{
	public int level;//属性等级
	public int cost;//花费
	public float ratio;//效果数值
}

public class GoldProp
{
	public int prop_id;//属性id
	public string name;//属性名称
	public int max_level;//属性最大等级
	public float max_ratio;//终极状态最大数值
	public string unit;//货币单位
	public Hashtable prop_levels;//属性各等级参数
}

public class GoldPropConfig 
{
	
	public Hashtable gold_prop_list;

	private static GoldPropConfig instance = new GoldPropConfig();
	
	public static GoldPropConfig Instance
	{
		get
		{
			return instance;
		}
	}

	public bool Load()
	{
		/*
		string path = Utils.getRootPath() + "/Config/GoldProps.xml";
		
		FileInfo fi = new FileInfo(path);
		if(!fi.Exists)
		{
			Debug.Log("filepath:"+path);
			return false;
		}
		else {
			FileStream fs = new FileStream(path,FileMode.Open);
			StreamReader sr = new StreamReader(fs);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(sr);
		*/
		{
			string xmlRaw = Utils.LoadConfig("Config/"+GameConfig.Instance.lang+"/GoldProps.xml");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlRaw);
			

			XmlNode root = xmlDoc.ChildNodes.Item(1);
			gold_prop_list = new Hashtable();
			for(int i=0;i<root.ChildNodes.Count;i++)
			{
				XmlNode gold_props_node = root.ChildNodes.Item(i);
				GoldProp gp = new GoldProp();
				gp.prop_id = int.Parse(gold_props_node.Attributes["PropId"].Value);
				gp.max_level = int.Parse(gold_props_node.Attributes["MaxLevel"].Value);
				gp.max_ratio = float.Parse(gold_props_node.Attributes["MaxRatio"].Value);
				gp.name = gold_props_node.Attributes["Name"].Value.ToString();
				gp.unit = gold_props_node.Attributes["Unit"].Value.ToString();
				gp.prop_levels = new Hashtable();

				for(int j=0;j<gold_props_node.ChildNodes.Count;j++)
				{
					XmlNode prop_level_node = gold_props_node.ChildNodes.Item(j);
					PropLevel prop_level = new PropLevel();
					prop_level.level = int.Parse(prop_level_node.Attributes["Level"].Value);
					prop_level.cost = int.Parse(prop_level_node.Attributes["Cost"].Value);
					prop_level.ratio = float.Parse(prop_level_node.Attributes["Ratio"].Value);
					
					gp.prop_levels.Add(prop_level.level,prop_level);
				}
				gold_prop_list.Add(gp.prop_id,gp);
			}
			return true;
		}
	}
	
	public PropLevel GetGoldPropByPropAndLevel(int prop_id,int level)
	{
		PropLevel pl = new PropLevel();
		if(gold_prop_list[prop_id]!=null)
		{
			GoldProp gp = new GoldProp();
			gp = (GoldProp)gold_prop_list[prop_id];
			if(gp.prop_levels != null && gp.prop_levels[level] != null)
			{
				pl = (PropLevel)gp.prop_levels[level];
				return pl;
			}
		}
		return null;
	}
}

