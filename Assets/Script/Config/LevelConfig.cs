using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Text;
using System;


public class LevelInfo
{
	public int level_id;
	public string level_name;
	public int sub_level_num;
	public float wave_interval;
	public Hashtable sub_levels;
}

public class SubLevelInfo
{
	public int sub_level_id;
	public string sub_level_name;
	public int wave_num;
	public int npc_num;
	public float wave_interval;
	public Hashtable npcs;
}

public class NpcInfo
{
	public int npc_id;
	public float blood;
	public float damage;
	public int num;
	public int boss;
}

public class LevelConfig
{
	public int max_sub_level_num = 3;
	public int levels_num;
	public Hashtable levels;
	private static LevelConfig instance = new LevelConfig();
	
	public static LevelConfig Instance
	{
		get
		{
			return instance;
		}
	}
	
	
	public bool Load()
	{
		/*
		string path = Utils.getRootPath() + "/Config/Levels.xml";
		
		FileInfo fi = new FileInfo(path);
		if(!fi.Exists)
		{
			Debug.Log("filepath:"+path);
			
			return false;
		}
		else {
		
			
			FileStream fs = new FileStream(path,FileMode.Open);
			Debug.Log("fs.Length:"+fs.Length);
			StreamReader sr = new StreamReader(fs);
			
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(sr);
		 */
		{
			string xmlRaw = Utils.LoadConfig("Config/"+GameConfig.Instance.lang+"/Levels.xml");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlRaw);
			
			XmlNode levels_node = xmlDoc.ChildNodes.Item(1);
			levels_num = levels_node.ChildNodes.Count;
			levels = new Hashtable();
			for(int i=0;i<levels_node.ChildNodes.Count;i++)
			{
				XmlNode level = levels_node.ChildNodes.Item(i);
				LevelInfo li = new LevelInfo();
				li.level_id = int.Parse(level.Attributes["LevelId"].Value);
				li.level_name = level.Attributes["Name"].Value.ToString();
				li.sub_level_num = level.ChildNodes.Count;
				li.sub_levels = new Hashtable();
				li.wave_interval = float.Parse(level.Attributes["WaveInterval"].Value);

				for(int j=0;j<level.ChildNodes.Count;j++)
				{
					XmlNode sub_level = level.ChildNodes.Item(j);
					SubLevelInfo sli = new SubLevelInfo();
					sli.sub_level_id = int.Parse(sub_level.Attributes["SubLevelId"].Value);
					sli.sub_level_name = sub_level.Attributes["Name"].Value.ToString();
					sli.npc_num = int.Parse(sub_level.Attributes["NpcNum"].Value);
					sli.wave_num = int.Parse(sub_level.Attributes["WaveNum"].Value);
					sli.wave_interval = float.Parse(sub_level.Attributes["WaveInterval"].Value);
					sli.npcs = new Hashtable();
					for(int k=0;k<sub_level.ChildNodes.Count;k++)
					{
						XmlNode npc = sub_level.ChildNodes.Item(k);
						NpcInfo ni = new NpcInfo();
						ni.npc_id = int.Parse(npc.Attributes["NpcId"].Value);
						ni.blood = float.Parse(npc.Attributes["Blood"].Value);
						ni.damage = float.Parse(npc.Attributes["Damage"].Value);
						ni.num = int.Parse(npc.Attributes["Num"].Value);
						ni.boss = int.Parse(npc.Attributes["Boss"].Value);
						sli.npcs.Add(ni.npc_id,ni);
					}
					li.sub_levels.Add(sli.sub_level_id,sli);
				}
				levels.Add(li.level_id,li);
			}
			return true;
		}
	}
	
	public Hashtable getNextLevelAndSubLevelId(int currentLevelId,int currentSubLevelId)
	{
		Hashtable ht = new Hashtable();
		int nextLevelId=0;
		int nextSubLevelId=0;
		if(currentLevelId==0&&currentSubLevelId==0)
		{
			nextLevelId = 1;
			nextSubLevelId = 1;
		}
		else if(currentLevelId!=0)
		{
			if(currentSubLevelId==0)
			{
				nextLevelId = currentLevelId;
				nextSubLevelId = currentSubLevelId+1;
			}
			else if(currentSubLevelId==max_sub_level_num)
			{
				nextLevelId = currentLevelId+1;
				nextSubLevelId = 1;
			}
			else
			{
				nextLevelId = currentLevelId;
				nextSubLevelId = currentSubLevelId+1;
			}
		}
		ht.Add("next_level_id",nextLevelId);
		ht.Add("next_sub_level_id",nextSubLevelId);
		return ht;
	}
	
	public LevelInfo getLevel(int level_id)
	{
		return (LevelInfo)levels[level_id];
	}
	
	public SubLevelInfo getSubLevel(int level_id,int sub_level_id)
	{
		return (SubLevelInfo)(getLevel(level_id).sub_levels[sub_level_id]);
	}

}

