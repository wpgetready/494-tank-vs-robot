using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using System.Xml;


public class GameData
{
	private static GameData instance = new GameData();
	
	public static GameData Instance
	{
		get
		{
			return instance;
		}
		
	}	
	
	/* The following metods came from the referenced URL */ 
	string UTF8ByteArrayToString(byte[] characters) 
	{      
	  	UTF8Encoding encoding = new UTF8Encoding(); 
	  	string constructedString = encoding.GetString(characters); 
	  	return (constructedString); 
	} 
		
	byte[] StringToUTF8ByteArray(string pXmlString) 
	{ 
	  	UTF8Encoding encoding = new UTF8Encoding(); 
	  	byte[] byteArray = encoding.GetBytes(pXmlString); 
	  	return byteArray; 
	} 
	
	// Here we serialize our UserData object of myData 
	string SerializeObject(object pObject) 
	{ 
	  	string XmlizedString = null; 
	  	MemoryStream memoryStream = new MemoryStream(); 
	  	XmlSerializer xs = new XmlSerializer(typeof(UserData)); 
	  	XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8); 
	  	xs.Serialize(xmlTextWriter, pObject); 
	  	memoryStream = (MemoryStream)xmlTextWriter.BaseStream; 
	  	XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray()); 
	  	return XmlizedString; 
	} 
	
	// Here we deserialize it back into its original form 
	object DeserializeObject(string pXmlizedString) 
	{ 
	  	XmlSerializer xs = new XmlSerializer(typeof(UserData)); 
	  	MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString)); 
	  	//XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8); 
	  	return xs.Deserialize(memoryStream); 
	} 
	
	public void CreateUserData()
	{
		string path = Utils.getDataPath() + "/UserData.xml";
		FileInfo fi = new FileInfo(path);
		if(fi.Exists)
		{
			return;
		}
		
		UserData.Instance.AddGold(1800);//init gold 1800
		//UserData.Instance.AddLockedLevel(2,1);//leve 2 sublevel 1 need to be unlocked
		
		foreach(DictionaryEntry item in MoneyPropConfig.Instance.money_prop_list)
		{
			MoneyProp mp = (MoneyProp)item.Value;
			UserData.Instance.AddUserProp(mp.prop_id,2,mp.ratio);
		}
		
		StreamWriter sw = new StreamWriter(path,false);
		string _data = SerializeObject(UserData.Instance);
		if(_data.Length>0)
		{
			sw.Write(_data);
		}
		sw.Close();
	}
	
	public void WriteUserData()
	{
		string path = Utils.getDataPath() + "/UserData.xml";		
		
		StreamWriter sw = new StreamWriter(path,false);		
		string _data = SerializeObject(UserData.Instance);
		if(_data.Length>0)
		{
			sw.Write(_data);
		}
		sw.Close();
	}
	
	public void ReadUserData()
	{
		string path = Utils.getDataPath() + "/UserData.xml";		
		
		FileStream fs = new FileStream(path,FileMode.Open,FileAccess.Read);
		StreamReader sr = new StreamReader(fs);
		string _data = sr.ReadToEnd();
		UserData.Instance = (UserData)DeserializeObject(_data);
		sr.Close();
		fs.Close();
	}
	
}

