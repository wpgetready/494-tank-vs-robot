using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;

public class Prop
{
	public int prop_id;
	public string name;
	public int num;
	public int ratio;
}

public class NpcProp
{
	public int npc_id;
	public List<Prop> prop_list;
}

public class NpcPropConfig 
{
	
	public Hashtable npc_prop_list;

	private static NpcPropConfig instance = new NpcPropConfig();
	
	public static NpcPropConfig Instance
	{
		get
		{
			return instance;
		}
	}
	
	public bool Load()
	{
		/*
		string path = Utils.getRootPath() + "/Config/NpcProps.xml";
		
		FileInfo fi = new FileInfo(path);
		if(!fi.Exists)
		{
			Debug.Log("filepath:"+path);
			return false;
		}
		else {
			FileStream fs = new FileStream(path,FileMode.Open);
			StreamReader sr = new StreamReader(fs);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(sr);
		*/
		{
			string xmlRaw = Utils.LoadConfig("Config/"+GameConfig.Instance.lang+"/NpcProps.xml");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlRaw);

			XmlNode root = xmlDoc.ChildNodes.Item(1);
			npc_prop_list = new Hashtable();
			for(int i=0;i<root.ChildNodes.Count;i++)
			{
				XmlNode npc_props_node = root.ChildNodes.Item(i);
				NpcProp np = new NpcProp();
				np.npc_id = int.Parse(npc_props_node.Attributes["NpcId"].Value);
				np.prop_list = new List<Prop>();

				for(int j=0;j<npc_props_node.ChildNodes.Count;j++)
				{
					XmlNode props_node = npc_props_node.ChildNodes.Item(j);
					Prop prop = new Prop();
					prop.prop_id = int.Parse(props_node.Attributes["PropId"].Value);
					prop.name = props_node.Attributes["Name"].Value.ToString();
					prop.num = int.Parse(props_node.Attributes["Num"].Value);
					prop.ratio = int.Parse(props_node.Attributes["Ratio"].Value);
					np.prop_list.Add(prop);
				}
				npc_prop_list.Add(np.npc_id,np);
			}
			return true;
		}
	}
	
	public Prop RandomNpcProp(int npc_id)
	{
		NpcProp np = new NpcProp();
		if(npc_prop_list[npc_id] != null)
		{
			np = (NpcProp)npc_prop_list[npc_id];
			int sum = 0;
			foreach(Prop prop in np.prop_list)
			{
				sum = sum + prop.ratio;
			}
			int rnd = Random.Range(0,sum);
			foreach(Prop prop in np.prop_list)
			{
				if(rnd - prop.ratio < 0)
				{
					return prop;
				}
				else
				{
					rnd = rnd - prop.ratio;
				}
			}
		}
		return null;
	}
}

