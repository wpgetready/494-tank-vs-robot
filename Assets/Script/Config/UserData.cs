using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UserLevelsInfo
{
	public List<UserLevelInfo> passed_levels;
	public bool ExistsLevelId(int level_id)
	{
		foreach(UserLevelInfo uli in passed_levels)
		{
			if(uli.level_id == level_id)
			{
				return true;
			}	
		}
		return false;
	}
	
	public void Add(UserLevelInfo user_level_info)
	{
		if(!ExistsLevelId(user_level_info.level_id))
		{
			passed_levels.Add(user_level_info);
		}	
	}
	
}

public class UserLevelInfo
{
	public int level_id;
	public List<UserSubLevelInfo> passed_sub_levels;
	public bool ExistsSubLevelId(int sub_level_id)
	{
		foreach(UserSubLevelInfo usli in passed_sub_levels)
		{
			if(usli.sub_level_id == sub_level_id)
			{
				return true;
			}
		}
		return false;
	}
	public void Add(UserSubLevelInfo user_sub_level_info)
	{
		if(!ExistsSubLevelId(user_sub_level_info.sub_level_id))
		{
			passed_sub_levels.Add(user_sub_level_info);
		}
	}
}

public class UserLockedLevelInfo
{
	public int level_id;
	public int sub_level_id;
	public bool isLocked;
}

public class UserSubLevelInfo
{
	public int level_id;
	public int sub_level_id;
	public List<UserNpcInfo> killed_npcs;
	public int stars;
}

public class UserItemInfo
{
	public int item_id;
	public int num;
}

public class UserNpcInfo
{
	public int npc_id;
	public int npc_num;
}

public class UserSetting
{
	public float sound_volume;
	public float music_volume;
	public UserSetting()
	{
		sound_volume = 1;
		music_volume = 1;
	}
}

public class UserProp
{
	public int prop_id;
	public int num;
	public float ratio;
}

public class UserData 
{
	
	public int max_passed_level_id;
	public int max_passed_sub_level_id;
	public List<UserItemInfo> user_items_info;
	public UserLevelsInfo user_levels_info;
	public PlayerAttribute player_attribute;
	public int player_gold_num;
	public int player_money_num;
	public UserSetting user_setting;
	public List<UserProp> user_props_list;
	public List<UserLockedLevelInfo> user_locked_level_info;
	
	private static UserData instance = new UserData();
	
	public static UserData Instance
	{
		get
		{
			return instance;
		}
		set
		{
			instance = value;
		}
	}
	
	UserData()
	{
		user_items_info = new List<UserItemInfo>();
		user_levels_info = new UserLevelsInfo();
		player_attribute = new PlayerAttribute();
		user_setting = new UserSetting();
		user_props_list = new List<UserProp>();
		user_locked_level_info = new List<UserLockedLevelInfo>();
		max_passed_level_id = 0;
		max_passed_sub_level_id = 0;
		player_gold_num = 0;
		player_money_num = 0;
	}
	
	public void setMusicVolume(float volume)
	{
		user_setting.music_volume = volume;
	}
	
	public void setSoundVolume(float volume)
	{
		user_setting.sound_volume = volume;
	}
	
	public void setPassedUserLevelAndSubLevelId(int passed_level_id,int passed_sub_level_id)
	{
		if(passed_level_id > max_passed_level_id)
		{
			max_passed_level_id = passed_level_id;
			max_passed_sub_level_id = passed_sub_level_id;
		}
		else if(passed_level_id == max_passed_level_id && passed_sub_level_id > max_passed_sub_level_id)
		{
			max_passed_sub_level_id = passed_sub_level_id;
		}
	}
	
	public bool ExistsLevelId(int level_id)
	{
		foreach(UserLevelInfo uli in user_levels_info.passed_levels)
		{
			if(uli.level_id == level_id)
			{
				return true;
			}	
		}
		return false;
	}
	
	public bool ExistsLevelAndSubLevelId(int level_id,int sub_level_id)
	{
		foreach(UserLevelInfo uli in user_levels_info.passed_levels)
		{
			if(uli.level_id == level_id)
			{
				foreach(UserSubLevelInfo usli in uli.passed_sub_levels)
				{
					if(usli.sub_level_id == sub_level_id)
					{
						return true;
					}
				}
			}	
		}
		return false;
	}
	
	public bool isPassedLevelAndSubLevel(int level_id,int sub_level_id)
	{
		if(level_id > max_passed_level_id)
		{
			return false;
		}
		else if(level_id == max_passed_level_id && sub_level_id > max_passed_sub_level_id)
		{
			return false;
		}
		return true;
	}
	
	public void AddGold(int gold)
	{
		player_gold_num += gold;
	}
	
	public UserProp AddUserProp(int prop_id,int num,float ratio=0)
	{
		bool isNewOne = true;
		UserProp up = new UserProp();
		for(int i=0;i<user_props_list.Count;i++)
		{
			if(user_props_list[i].prop_id == prop_id)
			{
				user_props_list[i].num += num;
				if(ratio!=0)
				{
					user_props_list[i].ratio = ratio;
					
					isNewOne = false;
				}
				up = (UserProp)user_props_list[i];
			}
		}
		if(isNewOne)
		{
			up.prop_id = prop_id;
			up.num = num;
			up.ratio = ratio;
			user_props_list.Add(up);
		}
		return up;
	}
	
	public UserProp SubUserProp(int prop_id,int num)
	{
		UserProp up = new UserProp();
		for(int i=0;i<user_props_list.Count;i++)
		{
			if(user_props_list[i].prop_id == prop_id)
			{
				if(user_props_list[i].num>=num)
				{
					user_props_list[i].num -= num;
					up = (UserProp)user_props_list[i];
					return up;
				}
			}
		}
		return null;
	}
	
	public UserProp GetUserProp(int prop_id)
	{
		for(int i=0;i<user_props_list.Count;i++)
		{
			if(user_props_list[i].prop_id == prop_id)
			{
				return (UserProp)user_props_list[i];
			}
		}
		return null;
	}
	
	public bool AddLockedLevel(int level_id,int sub_level_id)
	{
		UserLockedLevelInfo ulli = new UserLockedLevelInfo();
		ulli.level_id = level_id;
		ulli.sub_level_id = sub_level_id;
		ulli.isLocked = true;
		
		bool hasAdded = false;
		for(int i=0;i<user_locked_level_info.Count;i++)
		{
			if(user_locked_level_info[i].level_id == level_id && user_locked_level_info[i].sub_level_id == sub_level_id)
			{
				user_locked_level_info[i].isLocked = true;
				hasAdded = true;
			}
		}
		if(hasAdded == false)
		{
			user_locked_level_info.Add(ulli);
		}
		return hasAdded;
	}
	
	public bool UnLockLevel(int level_id,int sub_level_id)
	{
		bool isUnlocked = false;
		for(int i=0;i<user_locked_level_info.Count;i++)
		{
			if(user_locked_level_info[i].level_id == level_id && user_locked_level_info[i].sub_level_id == sub_level_id)
			{
				user_locked_level_info[i].isLocked = false;
				isUnlocked = true;
			}
		}
		return isUnlocked;
	}
	
	public bool isLockedLevel(int level_id,int sub_level_id)
	{
		for(int i=0;i<user_locked_level_info.Count;i++)
		{
			if(user_locked_level_info[i].level_id == level_id && user_locked_level_info[i].sub_level_id == sub_level_id)
			{
				return user_locked_level_info[i].isLocked ;
			}
		}
		return false;
	}
	
}

