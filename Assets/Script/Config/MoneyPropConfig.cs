using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;



public class MoneyProp
{
	public int item_id;
	public int prop_id;
	public string name;//标题
	public string desc;//描述
	public int num;//数量
	public float ratio;//加成效果,例如 0.85为该物品所对应当前玩家数值乘以0.85
	public int cost;//花费
	public string unit;//货币单位
}


public class MoneyPropConfig 
{
	
	public Hashtable money_prop_list;

	private static MoneyPropConfig instance = new MoneyPropConfig();
	
	public static MoneyPropConfig Instance
	{
		get
		{
			return instance;
		}
	}
	
	public bool Load()
	{
		/*
		string path = Utils.getRootPath() + "/Config/MoneyProps.xml";
		
		FileInfo fi = new FileInfo(path);
		if(!fi.Exists)
		{
			Debug.Log("filepath:"+path);
			return false;
		}
		else {
			FileStream fs = new FileStream(path,FileMode.Open);
			StreamReader sr = new StreamReader(fs);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(sr);
			
		*/
		{
			string xmlRaw = Utils.LoadConfig("Config/"+GameConfig.Instance.lang+"/MoneyProps.xml");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlRaw);
		
			XmlNode root = xmlDoc.ChildNodes.Item(1);
			money_prop_list = new Hashtable();
			for(int i=0;i<root.ChildNodes.Count;i++)
			{
				XmlNode money_props_node = root.ChildNodes.Item(i);
				
				MoneyProp mp = new MoneyProp();
				mp.item_id = int.Parse(money_props_node.Attributes["ItemId"].Value);
				mp.prop_id = int.Parse(money_props_node.Attributes["PropId"].Value);
				mp.name = money_props_node.Attributes["Name"].Value.ToString();
				mp.desc = money_props_node.Attributes["Desc"].Value.ToString();
				mp.num = int.Parse(money_props_node.Attributes["Num"].Value);
				mp.ratio = float.Parse(money_props_node.Attributes["Ratio"].Value);
				mp.cost = int.Parse(money_props_node.Attributes["Cost"].Value);
				mp.unit = money_props_node.Attributes["Unit"].Value.ToString();
			
				money_prop_list.Add(mp.item_id,mp);	
			}
			return true;
		}
	}
	
	public MoneyProp getMoneyPropByPropId(int prop_id)
	{
		foreach(DictionaryEntry item in money_prop_list)
		{
			MoneyProp mp = (MoneyProp)item.Value;
			if(mp.prop_id == prop_id)
			{
				return mp;
			}
		}
		return null;
	}
	
	public int StringToPropId(string name)
	{
		switch(name)
		{
		case "FirePower":
			return 15;
		case "FixBox":
			return 11;
		case "Shield":
			return 14;
		case "Speed":
			return 12;
		}
		return -1;
	}
	
	public MoneyProp getMoneyPropByPropName(string name)
	{
		int prop_id = StringToPropId(name);
		return getMoneyPropByPropId(prop_id);
	}
	
}

