using UnityEngine;
using System.Collections;

public class SpecialEffect : MonoBehaviour 
{
	public Vector3 createPos ;
	public UISprite  warnSprite ;
	private Vector3 screenPos ;
	private Vector3 Pos ;
	public string   type ;
	public UILabel gold ;
	private Transform _thisTransform ;
	void Start ()
	{
		_thisTransform = this.transform ;
		if(type == "boss")
		{
			_thisTransform.localPosition = new Vector3(0,106,2);
		}
		else if(type == "warn")
		{
			this.Invoke("DestroyThis",2);
			_thisTransform.localPosition = new Vector3(0,-69,2);
		}
		else if(type == "gold")
		{
			gold.text = "+10" ;
		}
	}
	private float time ;
	void Update () 
	{
		if(UIManager.gameState != UIManager.GameState.run)
		{
			if(UIManager.gameState == UIManager.GameState.MainMenu)
			{
				DestroyThis() ;
			}
			if(UIManager.gameState == UIManager.GameState.GameMenu)//
			{
				DestroyThis();
			}
			return ;
		}
		if(type == "warn")
		{
			time += Time.deltaTime*10 ;
			if(Time.time%0.2f > 0.1f)
			{
				if(UIManager.currentLang == "Chinese")
					warnSprite.spriteName = "UI_TextEffect_Warning_CN02" ;
				else
					warnSprite.spriteName = "UI_TextEffect_Warning_EN02" ;
			}
			else
			{
				if(UIManager.currentLang == "Chinese")
					warnSprite.spriteName = "UI_TextEffect_Warning_CN01" ;
				else
					warnSprite.spriteName = "UI_TextEffect_Warning_EN01" ;
			}
		}
		else if(type == ""||type == "gold")
		{
			screenPos = Camera.mainCamera.WorldToScreenPoint(createPos);
			Vector3 Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(screenPos);
			_thisTransform.position = new Vector3(Pos.x,Pos.y,1) ;
		}
	}
	void DestroyThis()
	{
		Destroy(gameObject);
	}
}
