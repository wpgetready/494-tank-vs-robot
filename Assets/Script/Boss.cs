using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour 
{
	public Transform[] bulletPosL ;
	public Transform[] bulletPosR ;
	public Transform[] guidePosL ;
	public Transform[] guidePosR ;
	private float      maxHealth ;     /* Boss的生命值 */
	public Transform explosions ;
	public Animation  mAnima ;
	public MuzzleFlashAnimate[] muzzle ;
	private int bulletIndex ;
	private int guideIndex ;
	private float[] fireTime = new float[3] ;
	private float   Distance = 1000 ;
	private float   moveSpeed ;
	private bool[]    _once = new bool[3] ;
	private float     _currentHealth ;
	private Transform  _thisTransform ;
	private MaterialFlash _flash ;
	void Start () 
	{
		
		
		
	}
	void OnEnable()
	{
		if(UIManager.gameModeVal==1)
		{
			maxHealth = NPCAttribute.BossBlood01[NPCManager.currentWaveNum/5]*1.5f ;
		}
		else if(UIManager.gameModeVal==0)
		{
			maxHealth = NPCAttribute.BossBlood01[NPCManager.currentWaveNum/5] ;
		}
		if(_thisTransform==null)
			_thisTransform = this.transform ;
		if(_flash==null)
			_flash = (MaterialFlash)_thisTransform.GetComponent<MaterialFlash>() ;
//		_flash.ChangeColor(Random.Range(0,5)) ;
		_currentHealth = maxHealth ;
		this.InvokeRepeating("DistanceAndPlayer",0,0.5f) ;
	}
	void OnDisable()
	{
		this.CancelInvoke("DistanceAndPlayer") ;
		if(_bloodBar!=null)
			GameObject.Destroy(_bloodBar.gameObject);
	}
	void OnDestroy()
	{
		if(_bloodBar!=null)
		{
			GameObject.Destroy(_bloodBar.gameObject);
		}
	}
	
	void ApplyPlayDamage(int damage)
	{
		_currentHealth -= damage ;
		_flash.Flash() ;
		if(_currentHealth<=0)
		{
			ObjectBuffer.DestroyBufferObject(this.gameObject) ;
//			if(UIManager.gameQuality==0)
			ObjectBuffer.InstantiateObject(12,_thisTransform.position,Quaternion.Euler(-90,0,0)) ;
			Instantiate(explosions,_thisTransform.position+new Vector3(0,5,0),explosions.rotation) ;
		//	if(UIManager.gameQuality==0)
			ObjectBuffer.InstantiateObject(Random.Range(28,30),_thisTransform.position,Quaternion.Euler(-90,0,0)) ;
			PlayerManager.AddScore(100) ;
		}
	}
	public GameObject   bloodBarPrefab ;
	private Transform  _bloodBar ;
	private void ShowBloodBar()
	{
		if(_bloodBar==null)
		{
			_bloodBar = NGUITools.AddChild(UIManager.SpecialEffect,bloodBarPrefab).transform;
		}
		Vector3 screenPos = Camera.main.WorldToScreenPoint(_thisTransform.position) ;
		Vector3 Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(screenPos);
		_bloodBar.position = new Vector3(Pos.x,Pos.y,1) ;
		_bloodBar.localPosition += new Vector3(0,50,0) ;
		_bloodBar.localScale = new Vector3(_currentHealth/maxHealth*50f,10,1) ;
	}
	void DistanceAndPlayer()
	{
		if(PlayerManager.playerTransform!=null)
		{
			Distance = Vector3.Distance(_thisTransform.position,PlayerManager.playerTransform.position) ;
			if(Distance<=30)
			{
				_once[1] = false ;
				_once[0] = false ;
				bossState = BossState.Shoot ;
			}
			else if(Distance<=40)
			{
				_once[1] = true ;
				if(!mAnima.IsPlaying("attack"))
				{
					_once[0] = false ;
					bossState = BossState.Move ;
				}
			}
			else
			{
				_once[1] = true ;
				if(!mAnima.IsPlaying("attack"))
				{
					bossState = BossState.Fly ;
				}
			}
		}
	}
	void Update () 
	{
		if(UIManager.gameState != UIManager.GameState.run||PlayerManager.playerTransform==null)
		{
			return ;
		}
		ShowBloodBar() ;
		switch(bossState)
		{
		case BossState.Fly:
			muzzle[0].fireRender.enabled = false ;
			muzzle[1].fireRender.enabled = false ;
			if(!mAnima.IsPlaying("fly")&&!_once[0])
			{
				_once[0] = true ;
				mAnima.Play("fly") ;
			}
			if(Distance<=100)
			{
				Shoot3() ;
			}
			Quaternion rotate = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
			_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate.eulerAngles.y,0),Time.deltaTime*10.0f) ;
			_thisTransform.Translate(Vector3.forward*Time.deltaTime * 40) ;
			break;
		case BossState.Move:
			muzzle[0].fireRender.enabled = false ;
			muzzle[1].fireRender.enabled = false ;
			if(!mAnima.IsPlaying("walk"))
			{
				mAnima.Play("walk") ;
			}
			Shoot3() ;
			Quaternion rotate1 = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
			_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate1.eulerAngles.y,0),Time.deltaTime*4.0f) ;
			_thisTransform.Translate(Vector3.forward*Time.deltaTime * 10) ;
			break;
		case BossState.Shoot:
			if(!mAnima.IsPlaying("attack")&&!_once[1])
			{
				mAnima.Play("attack") ;
			}
			else
			{
				muzzle[0].fireRender.enabled = true ;
				muzzle[1].fireRender.enabled = true ;
				Shoot1() ;
				Shoot2() ;
			}
			Quaternion rotate2 = Quaternion.LookRotation(PlayerManager.playerTransform.position-_thisTransform.position) ;
			_thisTransform.rotation = Quaternion.Slerp(_thisTransform.rotation,Quaternion.Euler(0,rotate2.eulerAngles.y,0),Time.deltaTime*10.0f) ;

			break;
		}
		
	}
	void Shoot1()
	{
		if(Time.time>=fireTime[0])
		{
			fireTime[0] = Time.time+0.2f ;
			ObjectBuffer.InstantiateObject(26,bulletPosL[bulletIndex].position,bulletPosL[bulletIndex].rotation) ;
			ObjectBuffer.InstantiateObject(26,bulletPosR[bulletIndex].position,bulletPosR[bulletIndex].rotation) ;
			if(bulletIndex>=bulletPosL.Length-1)
			{
				bulletIndex = 0 ;
			}
			else
			{
				bulletIndex ++ ;
			}
			
		}
	}
	void Shoot2()
	{
		if(Time.time>=fireTime[1])
		{
			fireTime[1] = Time.time+1.5f ;
			ObjectBuffer.InstantiateObject(24,guidePosL[guideIndex].position,guidePosL[guideIndex].rotation) ;
			ObjectBuffer.InstantiateObject(24,guidePosR[guideIndex].position,guidePosR[guideIndex].rotation) ;
			if(guideIndex>=guidePosL.Length-1)
			{
				guideIndex = 0 ;
			}
			else
			{
				guideIndex ++ ;
			}
		}
	}
	void Shoot3()
	{
		if(Time.time>=fireTime[2])
		{
			fireTime[2] = Time.time+1.5f ;
			ObjectBuffer.InstantiateObject(25,guidePosL[guideIndex].position,guidePosL[guideIndex].rotation) ;
			ObjectBuffer.InstantiateObject(25,guidePosR[guideIndex].position,guidePosR[guideIndex].rotation) ;
			if(guideIndex>=guidePosL.Length-1)
			{
				guideIndex = 0 ;
			}
			else
			{
				guideIndex ++ ;
			}
		}
	}
	private BossState bossState ;
	enum BossState
	{
		Fly,
		Move,
		Shoot,
		Dead,
	}
}
