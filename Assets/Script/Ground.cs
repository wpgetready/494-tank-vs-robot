using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {
	
	public Material[] mats;
	
	// Use this for initialization
	void Start () {
		
		int idx = (GameLevel.current_level_id-1)%4;
		this.transform.renderer.material = mats[idx];
			
	}
	
	void Awake () {
		int idx = (GameLevel.current_level_id-1)%4;
		this.transform.renderer.material = mats[idx];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
