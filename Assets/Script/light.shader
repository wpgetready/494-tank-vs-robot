Shader "Custom/light" {
	  Properties {
        _MainTex ("Texture", 2D) = "white" { }
        _MainTex2("Texture", 2D) = "white"{}
         _Color ("Base Color 1", Color) = (1,1,1,1)
         _Color1 ("hit", color) = (0.7,0.7,0.7,1)
      
    }

    SubShader
    {
        pass
        {
       		 
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
          
            #include "UnityCG.cginc"
       

            sampler2D _MainTex;
            sampler2D _MainTex2;
            fixed4 _Color;
            fixed4 _Color1;
           

            struct v2f {
             float4 pos : SV_POSITION;
                float2  uv : TEXCOORD0;
            }; 
           
            //顶点函数没什么特别的，和常规一样
            v2f vert (appdata_base v)
            {
                v2f o;
                o.pos = mul(UNITY_MATRIX_MVP,v.vertex);
                o.uv =  v.texcoord.xy;
                return o;
            }
           
           
            float4 frag (v2f i) : COLOR
            {
                 float4 outp;
                 outp = tex2D(_MainTex,i.uv);
                 
                 float4 tempColor;
                 tempColor = tex2D(_MainTex2, i.uv);
                 
                 if(tempColor.x != 0 && tempColor.y != 0 && tempColor.z != 0 )
                 {
                 	outp *= _Color * 2 * _Color1;
                 }
                // if( i.uv.x > 0.5 && i.uv.y < 0.5)
               //  {
                 	outp *= _Color1* 1.8;
               //  }
                
                 
       

                return outp;
            }
            ENDCG
        }
    }
    
    //for lower graphic card ,such as Samsung i909, Coolpad 539,at which display pink tanks
    SubShader {
    	Pass {
    		Material {
				Diffuse [_Color]
				Ambient (1,1,1,1)
			}
			Lighting On
			SetTexture [_MainTex] {
				Combine texture * primary DOUBLE, texture * primary
			}
    	}
    }
}
