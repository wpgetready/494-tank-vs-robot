using UnityEngine;
using System.Collections;

[System.Serializable]
public class ExplosionPart
{
	public GameObject gameObject ;
	public float delay ;	
	public bool hqOnly ;
	public float yOffset ;
}
public class ExploControl : MonoBehaviour 
{
	public ExplosionPart[] ambientEmitters ;
	public AudioClip       exploClip ;
	void Start () 
	{
		float maxTime = 0 ;
		if(exploClip)
			UIManager.PlayMusic(exploClip) ;
		for(int i=0;i<ambientEmitters.Length;i++)
		{
			StartCoroutine(InstantiateDelayed(ambientEmitters[i])) ;
			if (ambientEmitters[i].gameObject.particleEmitter)
				maxTime = Mathf.Max (maxTime, ambientEmitters[i].delay + ambientEmitters[i].gameObject.particleEmitter.maxEnergy);
		}
		Destroy (gameObject, maxTime + 0.5f);
	}
	IEnumerator InstantiateDelayed(ExplosionPart go)
	{
		yield return new WaitForSeconds(go.delay) ;
		Instantiate(go.gameObject,transform.position+Vector3.up*go.yOffset,Quaternion.identity);
	}
}
