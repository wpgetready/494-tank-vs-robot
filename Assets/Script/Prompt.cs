using UnityEngine;
using System.Collections;

public class Prompt : MonoBehaviour
{
	public float desTime ;
	private float _desTime ;
	void OnEnable () 
	{
		_desTime = Time.time+desTime ; 
	}
	
	void Update () 
	{
		if(Time.time>_desTime)
		{
			gameObject.active =  false ;
		}
	}
}
