using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class Utils {
	
	public static void Shuffle<T>(this IList<T> list)
	{  
	    int n = list.Count;  
	    while (n > 1) {  
	        n--;  
	        //int k = rng.Next(n + 1);
			int k = Random.Range(0,n+1);
	        T value = list[k];  
	        list[k] = list[n];  
	        list[n] = value;  
	    }  
	}
	
	public static string getDataPath()
	{
		/*
		Debug.Log("Application.dataPath:"+Application.dataPath);
		Debug.Log("Application.persistentDataPath:"+Application.persistentDataPath);
		*/
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER 
		//Debug.Log("UNITY_EDITOR");
		return Application.dataPath;
#elif UNITY_ANDROID || UNITY_IPHONE
		//Debug.Log("UNITY_ANDROID || UNITY_IPHONE");
		return Application.persistentDataPath;
#endif
	}
	
	public static string LoadConfig(string assetFile)
	{
		if(assetFile.EndsWith(".xml"))
		{
			assetFile = assetFile.Replace(".xml","");
		}
		TextAsset textAsset = (TextAsset)Resources.Load(assetFile, typeof(TextAsset));
		if(textAsset == null)
		{
			return null;
		}
		else
		{
			return textAsset.text;
		}		
	}
}
