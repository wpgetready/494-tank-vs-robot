﻿using UnityEngine;
using System.Collections;

public class UnlockTip : MonoBehaviour 
{
	public UILabel[] tipLabel ;
	public string type ;
	
	void Start () 
	{
		if(type == "UnlockTank")
		{
			tipLabel[0].enabled = true ; 
		}
		else if(type == "GetGold")
		{
			tipLabel[1].enabled = true ; 
		}
		else if(type == "LackGold0")
		{
			tipLabel[2].enabled = true ; 
		}
		else if(type == "LackGold1")
		{
			tipLabel[3].enabled = true ; 
		}
	}
	
}
