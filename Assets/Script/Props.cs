using UnityEngine;
using System.Collections;

public class Props : MonoBehaviour 
{
	public GameObject[]  propEffect ;
	public float       maxBlood ;
	
	public  Transform  childTransform ;
	public GameObject explosion ;
	public GameObject textEffect ;
	public string   type ;
	public MaterialFlash flash ;
	private float     _currentHealth ;
	private Transform _thisTransform ;
	private int       _rotateDire ;
	void Start ()
	{
		_thisTransform = this.transform ;
		if(type=="PC_Bomb")
		{
			this.enabled =false ;
		}
	}
	void OnEnable()
	{
		if(type=="PC_Coin"||type=="PC_FixBox")
		{
			if(Random.Range(0,10)<5)
			{
				_rotateDire = -100 ;
			}
			else
			{
				_rotateDire = 100 ;
			}
			this.Invoke("DestroyThis",20) ;
		}
		else if(type=="Prop_Magnet"||type == "Prop_Shield"||type == "Prop_Speed"||type == "Prop_FirePower")
		{
			this.Invoke("DestroyThis",20) ;
		}
		else if(type == "NPC_Bomb")
		{
			_currentHealth = maxBlood ;
		}
	}
	void OnDisable()
	{
		if(type=="PC_Coin"||type=="PC_FixBox"||type=="Prop_Magnet"||type == "Prop_Shield"||type == "Prop_Speed"||type == "Prop_FirePower")
		{
			this.CancelInvoke("DestroyThis") ;
		}
		else if(type == "NPC_Bomb")
		{
			isShowBlood = false ;
			showBloodTime = 0 ;
			if(_bloodBar!=null)
			{
				GameObject.Destroy(_bloodBar.gameObject);
			}
		}
		
	}
	void OnDestroy()
	{
		if(_bloodBar!=null)
		{
			GameObject.Destroy(_bloodBar.gameObject);
		}
	}
	void DestroyThis()
	{
		ObjectBuffer.DestroyBufferObject(this.gameObject) ;
	}
	/// <summary>
	/// 显示血条
	/// </summary>
	public GameObject   bloodBarPrefab ;
	private Transform  _bloodBar ;
	private bool       isShowBlood ;
	private float      showBloodTime ;
	private void ShowBloodBar()
	{
		if(_bloodBar==null)
		{
			_bloodBar = NGUITools.AddChild(UIManager.SpecialEffect,bloodBarPrefab).transform;
		}
		Vector3 screenPos = Camera.main.WorldToScreenPoint(_thisTransform.position) ;
		Vector3 Pos = UIManager.UIManagerScript.uiCamera.ScreenToWorldPoint(screenPos);
		_bloodBar.position = new Vector3(Pos.x,Pos.y,1) ;
		Debug.Log("Props.cs 92 maxBlood:"+maxBlood);
		_bloodBar.localScale = new Vector3(_currentHealth/maxBlood*50f,5,1) ;
		
		NGUITools.SetActive(_bloodBar.gameObject,true);
	}
	private void CloseBloodBar()
	{
		NGUITools.SetActive(_bloodBar.gameObject,false);
	}
	private float dis ;
	private float adsorbDis ;
	private string state ;
	void DistanceAndPlayer()
	{
		if(PlayerManager.playerTransform.gameObject.active)
		{
			dis = Vector3.Distance(_thisTransform.position,PlayerManager.playerTransform.position) ;

			if(UIManager.magnetProp>0)
			{
				//adsorbDis = PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionDis[10] ;
				//11 is prop_id for magnet
				GoldProp gp = (GoldProp)GoldPropConfig.Instance.gold_prop_list[11];
				int max_level = gp.max_level;
				float max_ratio = ((PropLevel)gp.prop_levels[max_level]).ratio;
				//adsorbDis  =  max_ratio * ((MoneyProp)MoneyPropConfig.Instance.money_prop_list[4]).ratio;
				adsorbDis  =  max_ratio ;
			}
			else
			{
				//adsorbDis = PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionDis[PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel] ;
				adsorbDis  = UserData.Instance.player_attribute.propsAdsorption;
			}
			if(dis<5)
			{
				DestroyThis() ;
				if(type=="PC_Coin")
				{
					PlayerManager.AddGold(10) ;
				}
				else if(type=="PC_FixBox")
				{
					PlayerManager.AddHP(10) ;
				}
				else if(type == "Prop_Magnet")
				{
					UIManager.magnetProp = 1 ;
				}
				else if(type == "Prop_Shield")
				{
					UIManager.shieldProp = 1 ;
				}
				else if(type == "Prop_Speed")
				{
					UIManager.speedProp = 1 ;
				}
				else if(type == "Prop_FirePower")
				{
					UIManager.firePowerProp = 1 ;
				}
				TextSpecialEffect() ;
			}
			else if(dis<adsorbDis)
			{
				state = "move" ;
			}
			else
			{
				state = "stop" ;
			}
		}
	}
	/// <summary>
	/// 文字特效
	/// </summary>
	void TextSpecialEffect()
	{
		GameObject obj = NGUITools.AddChild(UIManager.SpecialEffect,textEffect);
		obj.GetComponent<SpecialEffect>().createPos = _thisTransform.position ;
	}
	void Update () 
	{
		if(UIManager.gameState != UIManager.GameState.run)
		{
			return ;
		}
		if(type == "NPC_Bomb")
		{
			if(isShowBlood)
			{
				showBloodTime += Time.deltaTime ;
				if(showBloodTime>3)
				{
					CloseBloodBar() ;
					isShowBlood  = false ;
				}
				else
				{
					ShowBloodBar() ;
				}
			}
		}
		else
		{
			DistanceAndPlayer() ;
		}
		if(type=="PC_Coin"||type=="PC_FixBox")
		{
			if(PlayerManager.playerTransform.gameObject.active)
			{
				childTransform.Rotate(Vector3.up*Time.deltaTime*_rotateDire,Space.World);
				if(state=="move")
				{
					_thisTransform.LookAt(PlayerManager.playerTransform.position);
					_thisTransform.Translate(Vector3.forward*Time.deltaTime*30) ;
				}
			}
		}
		if(type=="Prop_Magnet"||type == "Prop_Shield"||type == "Prop_Speed"||type == "Prop_FirePower")
		{
			if(PlayerManager.playerTransform.gameObject.active)
			{
				if(state=="move")
				{
					if(propEffect[0].active&&_thisTransform.gameObject.active)
					{
						for(int i=0;i<propEffect.Length;i++)
						{
							propEffect[i].active = false ;
						}
					}
					_thisTransform.LookAt(PlayerManager.playerTransform.position);
					_thisTransform.Translate(Vector3.forward*Time.deltaTime*30) ;
				}
				if(state=="stop")
				{
					if(!propEffect[0].active&&_thisTransform.gameObject.active)
					{
						for(int i=0;i<propEffect.Length;i++)
						{
							propEffect[i].active = true ;
						}
					}
				}
			}
		}
	}
	void ApplyPlayDamage(float damage)
	{
		if(type=="NPC_Bomb")
		{
			isShowBlood = true ;
			showBloodTime = 0 ;
			_currentHealth -= damage ;
			flash.Flash() ;
			if(_currentHealth<=0)
			{
				Instantiate(explosion,_thisTransform.position+new Vector3(0,1,0),Quaternion.identity) ;
				DestroyThis() ;
			}
		}
	}
	void OnTriggerEnter(Collider other )
	{
		if(type == "PC_Bomb"&&other.tag=="Enemy")
		{
			DestroyThis() ;
			other.gameObject.SendMessage("ApplyPlayDamage",50,SendMessageOptions.DontRequireReceiver) ;
			Instantiate(explosion,_thisTransform.position+new Vector3(0,1,0),Quaternion.identity) ;
		}
		if(type == "NPC_Bomb"&&other.tag=="PC")
		{
			DestroyThis() ;
			Debug.Log("?PlayerManager.ApplyNPCDamage(30) in Props");
			PlayerManager.ApplyNPCDamage(30) ;
			Instantiate(explosion,_thisTransform.position+new Vector3(0,1,0),Quaternion.identity) ;
		}
	}
}
