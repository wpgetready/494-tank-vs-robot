﻿namespace UnityEngine.Advertisements
{
	public enum PlacementState
	{
		Ready,
		NotAvailable,
		Disabled,
		Waiting,
		NoFill
	}
}