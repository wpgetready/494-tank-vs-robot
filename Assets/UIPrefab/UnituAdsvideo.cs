﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;
public class UnituAdsvideo : MonoBehaviour {
	public Text textCoins;
	public int  timeWait=15;
	Coroutine coroutine;
	public bool m_checkend;
	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
		Debug.Log ("Start");
	}
	// Use this for initialization
	void Start () {
		ShowCoins();
		#if UNITY_ANDROID
		string adUnitId = "1012798";
		#elif UNITY_IPHONE
		string adUnitId = "1012894";
		
		#endif
		      Advertisement.Initialize(adUnitId);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ShowCoins(){
		//textCoins.text = GameTalkingManager.Instance.coins.ToString();
	}
	public void Btn_ShowAds()
	{
		if (coroutine!=null)
		{
			StopCoroutine(coroutine);
		}
		coroutine = StartCoroutine(WaitingAds());
	}
	public void ShowAds(bool isgoldshop)
	{
		Debug.Log ("SHow");
		m_checkend = false;
		Advertisement.Show(null, new ShowOptions
		                   {
			resultCallback = result =>
			{
				if (result.ToString()==ShowResult.Finished.ToString() )
				{
					//textCoins.text = result.ToString();
					//GameTalkingManager.Instance.UpdateCoin(-MinionTalking.Constrains.Coin_VideoAds_Reward);
					ShowCoins();
					Debug.Log ("COIN");
					UserData.Instance.AddGold(200);
					if(isgoldshop)
					FindObjectOfType<GoldShop> ().gold_num_label.text=UserData.Instance.player_gold_num.ToString();
					else
					{
						FindObjectOfType<MoneyShop> ().gold_num_label.text=UserData.Instance.player_gold_num.ToString();
					}
				}                
			}
		});
	}

	IEnumerator WaitingAds()
	{
		int t = 0;
		while (t < timeWait && !Advertisement.IsReady())
		{
			yield return new WaitForSeconds(1);
			t++;
		}
		//ShowAds();
	}
}
