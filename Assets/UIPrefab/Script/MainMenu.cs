using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour 
{
	private bool  isClickButton  ;
	
	void AnimateFinished()
	{
		isClickButton=true ;
	}
	void ButtonMessage(GameObject obj)
	{
		if(!isClickButton)
		{
			return ;
		}
		switch(obj.name)
		{
		case "StartGame":
			NGUITools.Destroy(this.gameObject) ;
			//UIManager.UIManagerScript.ShowLoading() ;
			UIManager.UIManagerScript.ShowGameLevel() ;
			break;
		case "SetAnOption":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowGameSetting() ;
			break;
		case "HelpAbout":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowInfo() ;
			break;
		case "ExitGame":
			UIManager.SaveData() ;
			Application.Quit() ;
			break;
		}
	}
	
}
