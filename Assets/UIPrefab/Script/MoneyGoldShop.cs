using UnityEngine;
using System.Collections;


public class MoneyGoldShop : MonoBehaviour {
	public UIPanel[] items;
	public UILabel[] price_labels;
	public UILabel gold_num_label;	
	public UILabel[] title_labels;
	public UIManager.GameState preGameState ;
	public string lastUI;
	public UIPanel[] panels;
	public UILabel[] confirm_info_labels;
	public int  money_gold_id;
	public UISprite spin_wheel;
	public static float dt;
	public GameObject add_gold;
	public UILabel gold_label;
	
	
	public int MoneyGoldShopIdxToId(int idx)
	{
		return idx+2;
	}
	
	// Use this for initialization
	void Start () {
		gold_label.transform.localScale = Vector3.zero;
		gold_label.GetComponent<TweenScale>().enabled = false;
		gold_label.GetComponent<TweenAlpha>().enabled = false;
		dt = 0;
		money_gold_id = 0;
		if(UIManager.gameState == UIManager.GameState.run)
		{
			preGameState = UIManager.gameState ;
			UIManager.gameState = UIManager.GameState.Shop ;
			Time.timeScale = 0 ;
			//Debug.Log("set Time.timeScale=0");
		}
		panels[1].transform.localScale = Vector3.zero;
		panels[1].enabled = false;
		panels[0].transform.localScale = new Vector3(1,1,1);
		panels[0].enabled = true;
		panels[2].transform.localScale = Vector3.zero;
		panels[2].enabled = false;
		
		Refresh();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			if(panels[0].enabled == true)
			{
				Debug.Log("close()");
				Close();
			}
			else if(panels[1].enabled == true)
			{
				Debug.Log("cancel()");
				Cancel();
			}
		}
	}
	
	void OnDestroy()
	{
		//Debug.Log("OnDestroy in MoneyGoldShop.cs");
		/*
		if(preGameState == UIManager.GameState.run)
		{
			Debug.Log("set Time.timeScale=1");
			Time.timeScale = 1 ;
		}
		*/
	}
	
	void Refresh() {
		for(int i=0;i<items.Length;i++)
		{
			int id = MoneyGoldShopIdxToId(i);
			MoneyGold mg = (MoneyGold)MoneyGoldConfig.Instance.money_gold_list[id];
			if(mg == null)
			{
				items[i].enabled = false;
				title_labels[i].enabled = false;
				price_labels[i].enabled = false;
			}
			else
			{
				items[i].enabled = true;
				title_labels[i].enabled = true;
				price_labels[i].enabled = true;
				
				title_labels[i].text = mg.name.ToString();
				price_labels[i].text = mg.money_unit.ToString()+mg.money.ToString();
			}
		}
		gold_num_label.text = UserData.Instance.player_gold_num.ToString();
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
			// ButtonX == items[X-1] 
		case "Button1":
		case "Button2":
		case "Button3":
		case "Button4":
		case "Button5":
		case "Button6":	
			//根据ButtonX中X作为idx，由于X从1开始，则X-1作为初始
				int idx = int.Parse(objname.Substring(6))+1;// moneygold id=1 is special,not show
				MoneyToGold(idx);
				Refresh();
				break;
		case "Close":
			Close();
			break;
		case "Cancel":
			Cancel();
			break;
		case "Confirm":
			Confirm();
			break;
		case "SpinWheelBackground":
			StopSpinWheel();
			break;
		}
	}
	
	public void StopSpinWheel()
	{
		stopSpinWheel = true;
	}
	
	void OnFinished(UITweener tween)
	{
		//Debug.Log("OnFinished");
		gold_label.GetComponent<TweenScale>().enabled = false;
		gold_label.GetComponent<TweenAlpha>().enabled = false;
	}
	
	public void ShowAddGold(int gold_num)
	{
		StopSpinWheel();
		gold_label.text = "+"+gold_num.ToString();
		gold_label.GetComponent<TweenScale>().enabled = true;
		gold_label.GetComponent<TweenAlpha>().enabled = true;
		
		gold_label.GetComponent<TweenScale>().Reset();
		gold_label.GetComponent<TweenScale>().Play(true);
		gold_label.GetComponent<TweenAlpha>().Reset();
		gold_label.GetComponent<TweenAlpha>().Play(true);
		if(UserData.Instance.user_setting.sound_volume>0)
		{
			gold_label.GetComponent<AudioSource>().Play();
		}
		Refresh();
	}
	
	
	void MoneyToGold(int idx)
	{	
		panels[0].enabled = false;
		panels[0].transform.localScale = Vector3.zero;
		panels[1].enabled = true;
		panels[1].transform.localScale = Vector3.one;
		if(idx<=MoneyGoldConfig.Instance.money_gold_list.Count)
		{
			money_gold_id = idx;
			MoneyGold mg = (MoneyGold)MoneyGoldConfig.Instance.money_gold_list[idx];
			confirm_info_labels[0].text = mg.money.ToString()+mg.money_unit.ToString();
			confirm_info_labels[1].text = mg.name.ToString();
			//Debug.Log("MoneyToGold money:"+money+" gold:"+gold);
		}
	}
	
	float timeOut=300;//5min
	float lastTime = 0;
	public bool stopSpinWheel = false;
	IEnumerator SpinWheelLoading()
	{		
		int i=1;
		while(i>0){
			int k= i%12;
			if(k<10)
			{
				spin_wheel.ChangeSprite("loading000"+k);
			}
			else{
				spin_wheel.ChangeSprite("loading00"+k);	
			}
			i++;
			if(lastTime == 0)
			{
				lastTime = Time.realtimeSinceStartup;
			}
			dt+= Time.realtimeSinceStartup-lastTime;
			lastTime = Time.realtimeSinceStartup;
			//Debug.Log("dt:"+dt);
			if(dt>timeOut)
			{
				//Debug.Log("dt>timeOut");
				//Debug.Log("stopSpinWheel:true");
				StopSpinWheel();
			}else{
				//Debug.Log("yield return null");
				yield return null;
			}
			if(stopSpinWheel)
			{
				//Debug.Log("yield break");
				panels[2].enabled = false;
				panels[2].transform.localScale = Vector3.zero;
				dt = 0;
				stopSpinWheel = false;
				yield break;	
			}
		}
	}
		
	void Confirm()
	{
		MoneyGold mg = (MoneyGold)MoneyGoldConfig.Instance.money_gold_list[money_gold_id];
		MMPurchase.obj = this.gameObject;
		#if UNITY_ANDROID
			using ( AndroidJavaClass unityplayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
				using ( AndroidJavaObject activity = unityplayer.GetStatic<AndroidJavaObject>("currentActivity")) {
					activity.CallStatic("makeOrder",activity,mg.product_id,1);
				}
			}
		#endif
		/*
		UIManager.UIManagerScript.ShowPaySuccess();
		UserData.Instance.AddGold(mg.gold);
		panels[0].enabled = true;
		panels[0].transform.localScale = Vector3.one;
		panels[1].enabled = false;
		panels[1].transform.localScale = Vector3.zero;
		Refresh();
		GameData.Instance.WriteUserData();
		*/
	}	
	
	public void Close()
	{
		StopSpinWheel();
		if(panels[0].enabled == true)
		{
			UIManager.gameState = preGameState;
			Debug.Log("lastUI:"+lastUI);
			switch(lastUI)
			{
			case "MoneyShop":
				UIManager.UIManagerScript.ShowMoneyShop(this.transform.localPosition.z-5);
				break;
			case "GoldShop":
				UIManager.UIManagerScript.ShowGoldShop(this.transform.localPosition.z-5);
				break;
			}
			NGUITools.Destroy(this.gameObject) ;
		}else if(panels[1].enabled == true){
			Cancel();
		}
	}
	
	public void Cancel()
	{
		StopSpinWheel();
		panels[2].enabled = false;
		panels[2].transform.localScale = Vector3.zero;
		panels[1].enabled = false;
		panels[1].transform.localScale = Vector3.zero;
		panels[0].enabled = true;
		panels[0].transform.localScale = new Vector3(1,1,1);
		money_gold_id = 0;
		Refresh();
	}
	

}
