using UnityEngine;
using System.Collections;

public class MoneyShop : MonoBehaviour {
	
	public UIPanel[] items;
	public UILabel[] price_labels;
	public UILabel[] title_labels;
	public UILabel[] desc_labels;
	public UILabel[] prop_num_labels;
	public UILabel gold_num_label;	
	public UISprite[] icons;
	private UIManager.GameState preGameState ;
	
	public int MoneyShopIdxToItemId(int idx)
	{
		return idx+1;
	}
	
	void Awake() {
		Debug.Log("MoneyShop awake");
	}
	public void Showads()
	{
		FindObjectOfType<UnituAdsvideo> ().ShowAds (false);

	}

	// Use this for initialization
	void Start () {
		Debug.Log("MoneyShop start");
		if(UIManager.gameState == UIManager.GameState.run)
		{
			preGameState = UIManager.gameState ;
			UIManager.gameState = UIManager.GameState.Shop ;
			Time.timeScale = 0 ;
		}
		Refresh();

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void OnDestroy()
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			Debug.Log("set Time.timeScale 1 in MoneyShop OnDestroy");
			Time.timeScale = 1 ;

		}
	}
	
	string PropIdToSpriteName(int prop_id)
	{
		switch(prop_id)
		{
		case 15:
			return "firepower";
		case 11:
			return "fixbox";
		case 12:
			return "speed";
		case 14:
			return "shield";
		}
		return "";
	}
	
	public void Refresh() 
	{
		for(int i=0;i<items.Length;i++)
		{
			int item_id = MoneyShopIdxToItemId(i);
			MoneyProp mp = (MoneyProp)MoneyPropConfig.Instance.money_prop_list[item_id];
			if(mp == null)
			{
				items[i].enabled = false;
				title_labels[i].enabled = false;
				desc_labels[i].enabled = false;
				price_labels[i].enabled = false;
				prop_num_labels[i].enabled = false;
				icons[i].enabled = false;
			}
			else
			{
				title_labels[i].text = mp.name;
				desc_labels[i].text = mp.desc;
				price_labels[i].text = mp.cost.ToString()+"\n"+mp.unit.ToString();
				icons[i].ChangeSprite(PropIdToSpriteName(mp.prop_id));
				//Debug.Log("prop_id:"+mp.prop_id);
				if(UserData.Instance.GetUserProp(mp.prop_id) != null)
				{
					prop_num_labels[i].text = UserData.Instance.GetUserProp(mp.prop_id).num.ToString();
					
					//Debug.Log("UserData.Instance.GetUserProp(mp.prop_id).num.ToString():"+UserData.Instance.GetUserProp(mp.prop_id).num.ToString());
				}
			}
		}
		gold_num_label.text = UserData.Instance.player_gold_num.ToString();
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Button1":
		case "Button2":
		case "Button3":
		case "Button4":
		case "Button5":
		case "Button6":	
			//根据ButtonX中X作为idx，由于X从1开始，则X-1作为初始
				int idx = int.Parse(objname.Substring(6));
				Purchase(idx);
				Refresh();
				break;
		case "Close":
			Close();
			break;
		case "MoreGold":
			MoreGold();
			break;
		}
	}
	
	void MoreGold()
	{
		////NGUITools.Destroy(this.gameObject);
		UIManager.UIManagerScript.ShowMoneyGoldShop("MoneyShop",UIManager.gameState,this.transform.localPosition.z-5);
	}
	
	void Purchase(int idx)
	{
		int item_id = MoneyShopIdxToItemId(idx-1);
		MoneyProp mp = (MoneyProp)MoneyPropConfig.Instance.money_prop_list[item_id];
		if(UserData.Instance.player_gold_num >= mp.cost)
		{
			UserData.Instance.player_gold_num -= mp.cost;
			UserProp up = (UserProp)UserData.Instance.AddUserProp(mp.prop_id,mp.num,mp.ratio);
			GameData.Instance.WriteUserData();
		}
		else
		{
			UIManager.UIManagerScript.ShowPopUpShop("MoneyGoldShop","MoneyShop", UIManager.gameState,this.transform.localPosition.z-5);
		}
	}
	
	void Close()
	{
		if(preGameState == UIManager.GameState.run)
		{
			UIManager.gameState = UIManager.GameState.run ;
			UIManager.UIManagerScript.CloseBackGround() ;
			NGUITools.Destroy(this.gameObject) ;
			
		}
		else
		{

			//UIManager.UIManagerScript.CloseBackGround() ;

			NGUITools.Destroy(this.gameObject) ;
			/*
			UIManager.UIManagerScript.ShowBackGround();
			UIManager.UIManagerScript.ShowGameLevel();
			*/
		}
	}
}
