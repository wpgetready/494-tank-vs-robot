using UnityEngine;
using System.Collections;

public class PopUpShop : MonoBehaviour {
	public string shouldGoto;// MoneyShop , GoldShop
	public string lastUI;//
	public UILabel[] labels;
	public UILabel[] labels2;
	public UIManager.GameState preGameState ;

	// Use this for initialization
	void Start () {
		Debug.Log("popupshop gamestate:"+UIManager.gameState);
		if(UIManager.gameState == UIManager.GameState.run)
		{
			preGameState = UIManager.gameState ;
			UIManager.gameState = UIManager.GameState.Shop ;
			Time.timeScale = 0 ;
		}
		for(int i=0;i<labels.Length;i++)
		{
			labels[i].enabled = false;
		}
		for(int i=0;i<labels.Length;i++)
		{
			labels2[i].enabled = false;
		}		
		
		if(shouldGoto.Equals("MoneyGoldShop"))
		{
			labels[0].enabled = true;
			labels2[0].enabled = true;
		}
		if(shouldGoto.Equals("MoneyShop"))
		{
			labels[1].enabled = true;
			labels2[1].enabled = true;
		}
		Debug.Log("popupshop pregamestate:"+preGameState);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void OnDestroy()
	{
		/* *
		 * 		如果是通过Close关闭,且前一个状态是run,则在OnDestroy的时候如果当前状态已经是run，则timescale=1，
		 * 		如果不加这种判断，在多个商店之间切换，会导致timescale=1先于gameState被赋值为run，会出现提前动的情况
		 * */
		if(UIManager.gameState == UIManager.GameState.run)
		{
			if(lastUI.Equals("GameScene"))
			{
				Time.timeScale = 1 ;
			}
		}
	} 
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Confirm":
			Confirm();
			break;
		case "Cancel":
		case "Close":
			Close();
			break;

		}
	}
	
	void Confirm()
	{
		float nextZ = this.transform.localPosition.z -5;
		NGUITools.Destroy(this.gameObject);
		//UIManager.gameState = UIManager.GameState.run ;
		UIManager.UIManagerScript.ShowBackGround(1);
		if(preGameState == UIManager.GameState.run)
		{
			UIManager.gameState = UIManager.GameState.run ;
		}
		if(shouldGoto.Equals("MoneyShop"))
		{
			UIManager.UIManagerScript.ShowMoneyShop(nextZ);
		}
		if(shouldGoto.Equals("MoneyGoldShop"))
		{
			UIManager.UIManagerScript.ShowMoneyGoldShop(lastUI,preGameState,nextZ);
		}
	}
	
	void Close()
	{
		Debug.Log("preGameState:"+preGameState);
		Debug.Log("lastUI:"+lastUI);
		Debug.Log("shouldGoto:"+shouldGoto);
		if(preGameState == UIManager.GameState.run)
		{
			UIManager.gameState = UIManager.GameState.run ;
			UIManager.UIManagerScript.CloseBackGround() ;
			NGUITools.Destroy(this.gameObject) ;
			
		}
		else
		{
			NGUITools.Destroy(this.gameObject) ;
			/*
			if(lastUI.Equals("MoneyShop"))
			{
				UIManager.UIManagerScript.ShowBackGround();
				UIManager.UIManagerScript.ShowMoneyShop();
			}
			if(lastUI.Equals("GoldShop"))
			{
				UIManager.UIManagerScript.ShowBackGround();
				UIManager.UIManagerScript.ShowGoldShop();
			}
			*/
		}
	}
}
