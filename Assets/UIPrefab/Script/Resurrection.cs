using UnityEngine;
using System.Collections;

public class Resurrection : MonoBehaviour 
{
	public UITweener[]   tweenTrans ;
	private string            buttonName =  "" ;
	private bool  isClickButton ;
	public UILabel ResurrectionNum ;
	public UISprite ResurrectionIco ;
	void Start ()
	{
		ResurrectionIco.ChangeSprite("UI_Shop_Prop_Tank0"+(PlayerManager.currentTank+1)+"_Resurrection");
		ResurrectionNum.text = "X "+UIManager.ResurrectionNum.ToString() ;
	}
	void PlayAnimate()
	{
		isClickButton = true ;
		for(int i=0;i<tweenTrans.Length;i++)
		{
			tweenTrans[i].Toggle() ;
		}
	}
	void AnimateFinished()
	{
		switch(buttonName)
		{
		case "Continue":
			if(UIManager.ResurrectionNum>0)
			{
				UIManager.ResurrectionNum -- ;
				UIManager.shieldProp = 1 ;
				PlayerManager.playerState = PlayerManager.PlayerState.Resurrection ;
				UIManager.gameState = UIManager.GameState.run ;
				UIManager.useSkill = true ;
				Time.timeScale = 1 ;
			}
			else
			{
				//UIManager.UIManagerScript.ShowShop() ;
				UIManager.UIManagerScript.ShowBackGround() ;
			}
			NGUITools.Destroy(gameObject) ;
			break;
		case "Return":
			UIManager.UIManagerScript.ShowGameOver() ;
			NGUITools.Destroy(gameObject) ;
			break;
		}
		buttonName = "" ;
		isClickButton = false ;
	}
	void ButtonMessage(GameObject obj)
	{
		if(isClickButton)
			return;
		buttonName = obj.name ;
		PlayAnimate() ;
	}
}
