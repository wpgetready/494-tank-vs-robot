using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
public class HintShop : MonoBehaviour {
	
	public string prop_name;
	public string lastUI;
	public UIManager.GameState preGameState ;
	
	// Use this for initialization
	void Start () {
		Time.timeScale = 0 ;
		MoneyProp mp = MoneyPropConfig.Instance.getMoneyPropByPropName(prop_name);
		GameObject go = this.transform.FindChild("Camera").FindChild("Anchor").FindChild("ConfirmPanel").FindChild("Label").gameObject;
		//go.GetComponent<UILabel>().text = UIManager.MultiLanguageMgr.GetComponent<Localization>().Get("GameShop.HintInfo").Replace("##",mp.name);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Confirm":
			Confirm();
			break;
		case "Cancel":
			Cancel();
			break;
		}
	}
	
	void Confirm () {
		MMPurchase.obj = this.gameObject;
		MoneyProp mp = MoneyPropConfig.Instance.getMoneyPropByPropName(prop_name);
		MoneyGold mg = MoneyGoldConfig.Instance.getMoneyGoldByItemId(mp.prop_id);
		
//#if UNITY_ANDROID			
//			using ( AndroidJavaClass unityplayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
//				using ( AndroidJavaObject activity = unityplayer.GetStatic<AndroidJavaObject>("currentActivity")) {
//					activity.CallStatic("makeOrder",activity,mg.product_id,1);
//				}
//			}
//#endif
		Debug.Log ("SHow");
		Advertisement.Show(null, new ShowOptions
		                   {
			resultCallback = result =>
			{
				if (result.ToString()==ShowResult.Finished.ToString() )
				{
					//textCoins.text = result.ToString();
					//GameTalkingManager.Instance.UpdateCoin(-MinionTalking.Constrains.Coin_VideoAds_Reward);
					int prop_id = MoneyPropConfig.Instance.StringToPropId(prop_name);
					//MoneyProp mp = (MoneyProp)MoneyPropConfig.Instance.getMoneyPropByPropId(prop_id);
					UserData.Instance.AddUserProp(prop_id,2,mp.ratio);
					GameData.Instance.WriteUserData();
					//UIManager.UIManagerScript.ShowPaySuccess();
					NGUITools.Destroy(this.gameObject);
				}                
			}
		});
	
	

	}
	
	public void Close () {
		NGUITools.Destroy(this.gameObject);
	}
	
	void Cancel () {
		NGUITools.Destroy(this.gameObject);
	}
	
	
	void OnDestroy()
	{
		/* *
		 * 		如果是通过Close关闭,且前一个状态是run,则在OnDestroy的时候如果当前状态已经是run，则timescale=1，
		 * 		如果不加这种判断，在多个商店之间切换，会导致timescale=1先于gameState被赋值为run，会出现提前动的情况
		 * */
		if(UIManager.gameState == UIManager.GameState.run)
		{
			if(lastUI.Equals("GameScene"))
			{
				Time.timeScale = 1 ;
			}
		}
	} 
}