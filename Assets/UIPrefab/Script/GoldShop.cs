using UnityEngine;
using System.Collections;

public class GoldShop : MonoBehaviour {
	
	public UIPanel[] items;
	public UILabel[] price_labels;
	public UILabel[] level_labels;
	public UISprite[] levelbar_sprites;
	public UILabel gold_num_label;
	public UISprite[] icons;
	public UILabel[] title_labels;
	private UIManager.GameState preGameState ;
	
	public int GoldShopIdxToPropId(int idx)
	{
		return idx+11;
	}
	
	void Awake() {
		Debug.Log("GoldShop awake");
	}
	
	
	// Use this for initialization
	void Start () {
		Debug.Log("GoldShop start");
		if(UIManager.gameState == UIManager.GameState.run)
		{
			preGameState = UIManager.gameState ;
			UIManager.gameState = UIManager.GameState.Shop ;
			Time.timeScale = 0 ;
		}
		Refresh();

	}
		
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void OnDestroy()
	{
		/* *
		 * 		如果是通过Close关闭,且前一个状态是run,则在OnDestroy的时候如果当前状态已经是run，则timescale=1，
		 * 		如果不加这种判断，在多个商店之间切换，会导致timescale=1先于gameState被赋值为run，会出现提前动的情况
		 * */
		if(UIManager.gameState == UIManager.GameState.run)
		{
			Time.timeScale = 1 ;
		}
		
	}
	
	public void Refresh() {
		for(int i=0;i<items.Length;i++)
		{
			int prop_id = GoldShopIdxToPropId(i);
			int level = ((PlayerAttribute)UserData.Instance.player_attribute).getAttributeLevelByPropId(prop_id);
			
			//Debug.Log("prop_id:"+prop_id+" level:"+level);
			int max_level = ((GoldProp)GoldPropConfig.Instance.gold_prop_list[prop_id]).max_level;
			level_labels[i].text = level+"/"+max_level;
			
			int next_level = 0;
			if(level+1 > max_level)
			{
				next_level = max_level;
			}else{
				next_level = level+1;
			}
			PropLevel pl = (PropLevel)GoldPropConfig.Instance.GetGoldPropByPropAndLevel(prop_id,next_level);
			price_labels[i].text = pl.cost.ToString()+"\n"+((GoldProp)(GoldPropConfig.Instance.gold_prop_list[prop_id])).unit.ToString();
			levelbar_sprites[i].fillAmount = (float)level/(float)max_level;
			title_labels[i].text = ((GoldProp)(GoldPropConfig.Instance.gold_prop_list[prop_id])).name.ToString();
		}
		gold_num_label.text = UserData.Instance.player_gold_num.ToString();
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Button1":
		case "Button2":
		case "Button3":
		case "Button4":
		case "Button5":
		case "Button6":	
			//根据ButtonX中X作为idx，由于X从1开始，则X-1作为初始
				int idx = int.Parse(objname.Substring(6));
				Upgrade(idx);
				Refresh();
				break;
		case "Close":
			Close();
			break;
		case "MoreGold":
			MoreGold();
			break;
		}
	}
	public void Showads()
	{
		FindObjectOfType<UnituAdsvideo> ().ShowAds (true);
	}
	
	void MoreGold()
	{
		////NGUITools.Destroy(this.gameObject);
		UIManager.UIManagerScript.ShowMoneyGoldShop("GoldShop",UIManager.gameState,this.transform.localPosition.z-5);
	}
	
	void Close()
	{
		Debug.Log("GoldShop close");
		if(preGameState == UIManager.GameState.run)
		{
			Debug.Log("preGameState == UIManager.GameState.run");
			UIManager.gameState = UIManager.GameState.run ;
			UIManager.UIManagerScript.CloseBackGround() ;
			NGUITools.Destroy(this.gameObject) ;
			
		}
		else
		{
			Debug.Log("preGameState != UIManager.GameState.run");
			NGUITools.Destroy(this.gameObject) ;
			/*
			UIManager.UIManagerScript.ShowBackGround();
			UIManager.UIManagerScript.ShowGameLevel();
			*/
		}
	}
	
	void Upgrade(int idx)
	{
		int prop_id = GoldShopIdxToPropId(idx-1);
		int max_level = ((GoldProp)GoldPropConfig.Instance.gold_prop_list[prop_id]).max_level;
		int level = ((PlayerAttribute)UserData.Instance.player_attribute).getAttributeLevelByPropId(prop_id);
		if(level >= max_level)
		{
			return;
		}
		else
		{
			PropLevel pl = (PropLevel)GoldPropConfig.Instance.GetGoldPropByPropAndLevel(prop_id,level+1);
			if(UserData.Instance.player_gold_num >= pl.cost)
			{
				UserData.Instance.player_gold_num -= pl.cost;
				((PlayerAttribute)UserData.Instance.player_attribute).setAttributeByPropId(prop_id,level+1,pl.ratio);
				((PlayerAttribute)(UserData.Instance.player_attribute)).RefreshAttributes();
				GameData.Instance.WriteUserData();
			}
			else
			{
				UIManager.UIManagerScript.ShowPopUpShop("MoneyGoldShop","GoldShop", UIManager.gameState,this.transform.localPosition.z-5);
			}
		}
	}
}
