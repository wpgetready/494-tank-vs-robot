using UnityEngine;
using System.Collections;

public class GameLevel : MonoBehaviour {
	
	public UIAtlas atlas;
	public static int current_level_id;
	public static int next_level_id;
	public static int next_sub_level_id;
	public static int current_sub_level_id;
	public static SubLevelInfo current_sub_level_info;
	public UISprite level_bar;
	public UILabel[] level_label;
	public GameObject[] levels;
	
	void Awake() {

	}
	
	// Use this for initialization
	void Start () {
		
		//UserData.Instance.player_gold_num +=100000;/////for debug
		UserData.Instance.player_attribute.RefreshAttributes();
		
		current_sub_level_info = new SubLevelInfo();
		
		//getNextLevelAndSubLevelId according to User passed level and sublevel 
		Hashtable ht_next = LevelConfig.Instance.getNextLevelAndSubLevelId(UserData.Instance.max_passed_level_id,UserData.Instance.max_passed_sub_level_id);
		next_level_id = (int)ht_next["next_level_id"];
		next_sub_level_id = (int)ht_next["next_sub_level_id"];

		current_level_id = next_level_id;
		current_sub_level_id = next_sub_level_id;
		RefreshLevelInfo();

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	
	bool isSubLevelOpenForUser(int level_id,int sub_level_id)
	{
		Hashtable ht_next = LevelConfig.Instance.getNextLevelAndSubLevelId(UserData.Instance.max_passed_level_id,UserData.Instance.max_passed_sub_level_id);
		int t_next_level_id = (int)ht_next["next_level_id"];
		int t_next_sub_level_id = (int)ht_next["next_sub_level_id"];
		if(t_next_level_id>level_id)
		{
			return true;
		}
		else if(t_next_level_id == level_id && t_next_sub_level_id >= sub_level_id)
		{
			return true;
		}
		return false;
	}
	
	void RefreshLevelInfo()
	{
		Hashtable ht_next = LevelConfig.Instance.getNextLevelAndSubLevelId(UserData.Instance.max_passed_level_id,UserData.Instance.max_passed_sub_level_id);
		int t_next_level_id = (int)ht_next["next_level_id"];
		level_label[1].GetComponent<UILabel>().text = t_next_level_id.ToString();
		
		for(int i=0;i<levels.Length;i++)
		{
			int level_id = int.Parse(levels[i].name.Substring("Level_".Length));
			GameObject flag_go = levels[i].transform.FindChild("Flag").gameObject;
			if(isSubLevelOpenForUser(level_id,1))
			{
				flag_go.GetComponent<UISprite>().spriteName = "level_unlocked";
			}
			else
			{
				flag_go.GetComponent<UISprite>().spriteName = "level_locked";
			}
			
			for(int j=1;j<=LevelConfig.Instance.max_sub_level_num;j++)
			{
				int sublevel_id = j;
				GameObject go = levels[i].transform.FindChild("SubLevel_"+j).gameObject;
				if(isSubLevelOpenForUser(level_id,sublevel_id))
				{
					go.transform.FindChild("Background").GetComponent<UISprite>().spriteName = "sublevel_unlocked";
				}
				else
				{
					go.transform.FindChild("Background").GetComponent<UISprite>().spriteName = "sublevel_locked";
				}
			}
		}
		
	}
	
	
	void ButtonMessage(GameObject obj)
	{
		switch(obj.name)
		{

		case "Close":
			Close();
			break;
		case "GoldShop":
			GoldShop();
			break;
		case "MoneyShop":
			MoneyShop();
			break;
		default:
			Debug.Log(obj.name);
			if(obj.name.Contains("SubLevel_"))
			{
				string sublevel_id_str = obj.name.Substring("SubLevel_".Length);
				string level_id_str = obj.transform.parent.name.Substring("Level_".Length);
				Debug.Log(level_id_str+":"+sublevel_id_str);
				int sublevel_id = int.Parse(sublevel_id_str);
				int level_id = int.Parse(level_id_str);
				EnterSubLevel(level_id,sublevel_id);
			}
			break;
		}
	}
	
	void EnterSubLevel(int level_id,int sub_level_id)
	{
		if(isSubLevelOpenForUser(level_id,sub_level_id))
		{
			current_level_id = level_id;
			current_sub_level_id = sub_level_id;
			current_sub_level_info = LevelConfig.Instance.getSubLevel(current_level_id,current_sub_level_id);
			UIManager.UIManagerScript.ShowLoading(this.transform.localPosition.z-25) ;
			NGUITools.Destroy(this.gameObject) ;
		}
	}
	
	
	void Close()
	{
		NGUITools.Destroy(this.gameObject) ;
		UIManager.UIManagerScript.ShowBackGround();
		UIManager.UIManagerScript.ShowGameMenu();
	}
	
	void GoldShop()
	{
		UIManager.UIManagerScript.ShowBackGround(1);
		UIManager.UIManagerScript.ShowGoldShop(this.transform.localPosition.z-5);
	}
	
	void MoneyShop()
	{
		UIManager.UIManagerScript.ShowBackGround(1);
		UIManager.UIManagerScript.ShowMoneyShop(this.transform.localPosition.z-5);
	}
	
}
