using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour 
{
	public UITweener	tweenTrans ;
	public UIImageButton next_level_button;
	/*
	public TweenPosition[]	tweenPos ;
	public TweenScale[]	tweenScale ;
	*/
	private string	buttonName =  "init" ;
	public UISprite	result ;
	private AsyncOperation	async ;
	private int waveNum ;
	
	void Awake() {
	}
	
	
	void Start () 
	{

		
		UIManager.UIManagerScript.ShowBackGround(1);
		//UIManager.SaveData() ;
		
		if(UIManager.gameState == UIManager.GameState.GameOver)
		{
			result.ChangeSprite("lose_bg");
			next_level_button.transform.localScale = Vector3.zero;
			next_level_button.enabled = false;
		}
		if(UIManager.gameState == UIManager.GameState.GameWin)
		{
			UserData.Instance.setPassedUserLevelAndSubLevelId(GameLevel.current_level_id,GameLevel.current_sub_level_id);
			result.ChangeSprite("win_bg");
			next_level_button.transform.localScale = new Vector3(1,1,1);
			next_level_button.enabled = true;
		}
		GameData.Instance.WriteUserData();
		Time.timeScale = 0 ;

	}
	
	void AnimateFinished()
	{
		switch(buttonName)
		{
		case "Close":
		case "Return":
			StartCoroutine(ReturnGameLevel()) ;
			break;
		case "Restart":
			StartCoroutine(Restart()) ;
			break;
		case "NextLevel":
			NextLevel();
			break;
		case "init":
			
			break;
		}
		buttonName = "" ;
	}
	void ButtonMessage(GameObject obj)
	{
		if(buttonName!="")
			return ;
		buttonName = obj.name ;
		switch(buttonName)
		{
		case "Close":
		case "Return":
			tweenTrans.Toggle() ;
			break;
		case "Restart":
			tweenTrans.Toggle() ;
			break;
		case "NextLevel":
			tweenTrans.Toggle() ;
			break;
		}
	}
	void OnDestroy()
	{
		UIManager.UIManagerScript.CloseBackGround();
		async = null ;
		Time.timeScale = 1 ;

	}
	
	
	IEnumerator ReturnGameLevel()
	{
		async = Application.LoadLevelAsync(2) ;
		yield return async ;
		UIManager.UIManagerScript.CloseGameScene();
		UIManager.UIManagerScript.ShowBackGround();
		UIManager.UIManagerScript.ShowGameLevel();
		NGUITools.Destroy(gameObject) ;
	}
	
	IEnumerator ReturnMainMenu()
	{
		async = Application.LoadLevelAsync(2) ;
		yield return async ;
		UIManager.UIManagerScript.CloseGameScene();
		UIManager.UIManagerScript.ShowBackGround() ;
		UIManager.UIManagerScript.ShowGameMenu() ;
		NGUITools.Destroy(gameObject) ;
	}
	IEnumerator Restart()
	{
		async = Application.LoadLevelAsync(1) ;
		yield return async ;
		UIManager.UIManagerScript.ShowGameScene() ;
		NGUITools.Destroy(gameObject) ;
	}
	
	void NextLevel()
	{
		Hashtable ht_next = LevelConfig.Instance.getNextLevelAndSubLevelId(UserData.Instance.max_passed_level_id,UserData.Instance.max_passed_sub_level_id);
		GameLevel.next_level_id = (int)ht_next["next_level_id"];
		GameLevel.next_sub_level_id = (int)ht_next["next_sub_level_id"];

		GameLevel.current_level_id = GameLevel.next_level_id;
		GameLevel.current_sub_level_id = GameLevel.next_sub_level_id;
		GameLevel.current_sub_level_info = LevelConfig.Instance.getSubLevel(GameLevel.current_level_id,GameLevel.current_sub_level_id);
		NGUITools.Destroy(this.gameObject) ;
		UIManager.UIManagerScript.CloseGameScene();
		UIManager.UIManagerScript.ShowLoading();
	}
}
