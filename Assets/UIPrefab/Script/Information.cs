using UnityEngine;
using System.Collections;

public class Information : MonoBehaviour
{
	private bool  isClickButton = true ;
	
	void ButtonMessage()
	{
		if(!isClickButton)
		{
			return ;
		}
		isClickButton = false ;
		NGUITools.Destroy(this.gameObject) ;
		UIManager.UIManagerScript.ShowBackGround() ;
		//UIManager.UIManagerScript.ShowMainMenu() ;
		UIManager.UIManagerScript.ShowGameMenu();

	}
}
