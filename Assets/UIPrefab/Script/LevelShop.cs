using UnityEngine;
using System.Collections;

public class LevelShop : MonoBehaviour {
	
	public int money_gold_id = 1;
	// Use this for initialization
	void Start () {
		Time.timeScale = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();	
		}
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Close":
			Close();
			break;
		case "Confirm":
			Confirm();
			break;
		}
	}
	
	void OnDestroy()
	{
		Time.timeScale = 1;
	}
	
	public void Confirm()
	{

	}
	
	public void Close()
	{
		UIManager.UIManagerScript.CloseBackGround();
		UIManager.UIManagerScript.ShowBackGround(1);
		UIManager.UIManagerScript.ShowGameLevel();
		NGUITools.Destroy(this.gameObject) ;
	}
}
