using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
public class ReviveShop : MonoBehaviour {
	
	public UIManager.GameState preGameState ;
	
	// Use this for initialization
	void Start () {
		Time.timeScale = 0 ;
		Cancel ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void ButtonMessage(GameObject obj)
	{
		string objname = obj.name;
		switch(objname)
		{
		case "Confirm":
			Confirm();
			break;
		case "Cancel":
			Cancel();
			break;
		}
	}
	
	void Confirm () {
		MMPurchase.obj = this.gameObject;
		//MoneyGold mg = MoneyGoldConfig.Instance.getMoneyGoldByItemId(10);
		
//#if UNITY_ANDROID			
//			using ( AndroidJavaClass unityplayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
//				using ( AndroidJavaObject activity = unityplayer.GetStatic<AndroidJavaObject>("currentActivity")) {
//					activity.CallStatic("makeOrder",activity,mg.product_id,1);
//				}
//			}
//#endif


		ShowAds ();

	}

	public void ShowAds()
	{
		Debug.Log ("SHow");
		Advertisement.Show(null, new ShowOptions
		                   {
			resultCallback = result =>
			{
				if (result.ToString().Equals(ShowResult.Finished.ToString()) )
				{
					//textCoins.text = result.ToString();
					//GameTalkingManager.Instance.UpdateCoin(-MinionTalking.Constrains.Coin_VideoAds_Reward);
					UIManager.shieldProp = 1 ;
					PlayerManager.playerState = PlayerManager.PlayerState.Resurrection ;
					UIManager.gameState = UIManager.GameState.run ;
					UIManager.useSkill = true ;
					Time.timeScale = 1 ;
					UIManager.UIManagerScript.ShowPaySuccess();
					NGUITools.Destroy(this.gameObject);
				}                
			}
		});
	}


	public void Close () {
		NGUITools.Destroy(this.gameObject);
	}
	
	void Cancel () {
		NGUITools.Destroy(this.gameObject);
		UIManager.UIManagerScript.ShowGameOver(this.transform.localPosition.z-25) ;
	}
	
}