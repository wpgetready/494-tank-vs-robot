using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {
	
	void Awake() {

	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
	
	
	
	void ButtonMessage(GameObject obj)
	{
		switch(obj.name)
		{
		case "StartGame":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowGameLevel() ;
			break;
		case "SetAnOption":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowGameSetting() ;
			break;
		case "Help":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowGameHelp();
			break;
		case "About":
			NGUITools.Destroy(this.gameObject) ;
			UIManager.UIManagerScript.ShowGameInfo();
			break;
		case "ExitGame":
			GameData.Instance.WriteUserData() ;
			UIManager.SaveData() ;
			Application.Quit() ;
			break;
		}
	}

}
