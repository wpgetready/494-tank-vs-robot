﻿using UnityEngine;
using System.Collections;

public class GameShop : MonoBehaviour 
{

	public GameObject  panel ;

	public UILabel     goldNum ; /* 金币 */
	public UITweener[]   tweenTrans ;
	public UILabel[]          typeName ;
	public UILabel[]          currentLevel ;
	public UILabel[]          costGold ;
	public UISprite[]        typeSprite ;    
	public UISprite[]        typeBackSprite ;
	public UILabel[]        upgrade ;
	private string            buttonName ;
	private int            selectType  ; /* 0是属性面板1是道具面板 */
	private string[]       attributeName ;
	private string[]       propName ;
	private string       upgradeName1 ;
	private string       upgradeName2 ;
	private UIManager.GameState preGameState ;
	private bool  isClickButton ;
	void Start () 
	{
		preGameState = UIManager.gameState ;
		UIManager.gameState = UIManager.GameState.Shop ;
		Time.timeScale = 0 ;
		if(preGameState==UIManager.GameState.Resurrection)
		{
			selectType = 1 ;
		}

		InitShowTank() ;
		Update() ;
		upgradeName1 = "升级";
		upgradeName2 = "购买";
		attributeName = new string[]{"炮塔升级","护甲升级","移动速度","攻击速度","瞄准速度","道具吸附"} ;
		propName = new string[]{"防御地雷","终极导弹","多倍金币","原地复活","关卡无敌"} ;
		
		
		UpdateType() ;
	}
	void PlayAnimate()
	{
		isClickButton = true ;
		for(int i=0;i<tweenTrans.Length;i++)
		{
			tweenTrans[i].Toggle() ;
		}
	}
	void AnimateFinished()
	{
		switch(buttonName)
		{
		case "Return":
			if(preGameState == UIManager.GameState.run)
			{
				UIManager.UIManagerScript.ShowGameScene() ;
				UIManager.UIManagerScript.CloseBackGround() ;
			}
			else if(preGameState==UIManager.GameState.Resurrection)
			{
				//UIManager.UIManagerScript.ShowResurrection() ;
				UIManager.UIManagerScript.CloseBackGround() ;
			}
			NGUITools.Destroy(this.gameObject);
			break;
		}
		buttonName = "" ;
		isClickButton = false ;
	}
	void OnDestroy()
	{
		if(currentShowTank!=null)
			Destroy(currentShowTank);
		Time.timeScale = 1 ;
	}
	void ButtonMessage(GameObject obj)
	{
		if(isClickButton)
		{
			return ;
		}
		buttonName = obj.name ;
		switch(buttonName)
		{
		case "Return":
			if(preGameState == UIManager.GameState.run)
			{
				if(PlayerManager.WeaponsScript!=null)
				{
					PlayerManager.WeaponsScript.WeaponsChange();
					PlayerManager.WeaponsScript.ArmorsChange();
					PlayerManager.WeaponsScript.AttackSpeedChange();
				}
			}
			UIManager.SaveData();
			PlayAnimate() ;
			break;
		case "Continue":
			PlayAnimate() ;
			break;
		case "Attribute":
			selectType = 0 ;
			
			UpdateType() ;
			break;
		case "Prop":
			selectType = 1 ;
			UpdateType() ;
			
			break;
		}
	}
	void UpdateType()
	{
		if(selectType==0)
		{
			for(int i=0;i<upgrade.Length;i++)
				upgrade[i].text = upgradeName1 ;
			NGUITools.SetActive(panel,true);
			for(int i=0;i<attributeName.Length;i++)
				typeName[i].text = attributeName[i] ;
			ChangeSprite();
			typeSprite[3].ChangeSprite ( "UI_Shop_Attribute_FireRate") ;
			typeSprite[4].ChangeSprite ( "UI_Shop_Attribute_TurretSpeed") ;
			typeSprite[5].ChangeSprite ( "UI_Shop_Attribute_CoinMagnet") ;
			typeSprite[2].ChangeSprite ( "UI_Shop_Attribute_Tank0"+(PlayerManager.currentTank+1)+"_MovementSpeed" );
		}
		else if(selectType==1)
		{
			for(int i=0;i<upgrade.Length;i++)
				upgrade[i].text = upgradeName2 ;
			NGUITools.SetActive(panel,false);
			for(int i=0;i<propName.Length;i++)
				typeName[i].text = propName[i] ;
			typeSprite[0].ChangeSprite ("UI_Shop_Prop_Tank0"+(PlayerManager.currentTank+1)+"_Bomb") ;
			typeSprite[1].ChangeSprite ( "Button_MaxFirePower_Enable" );
			typeSprite[2].ChangeSprite ( "UI_Shop_Prop_DoubleGold" );
			typeSprite[3].ChangeSprite ( "UI_Shop_Prop_Tank0"+(PlayerManager.currentTank+1)+"_Resurrection") ;
			typeSprite[4].ChangeSprite ( "UI_Shop_Prop_Tank0"+(PlayerManager.currentTank+1)+"_God") ;
		}
		selectIco = 0 ;
		if(preGameState==UIManager.GameState.Resurrection)
			selectIco = 3 ;
		ChangeLevel() ;
		ChangeShowInfo() ;
	}
	
	private int selectIco ;
	void ChangeShowInfo()
	{
		for(int i=0;i<iconBack.Length;i++)
		{
			if(i==selectIco)
				iconBack[i].ChangeSprite("Button_Shop_icon_Background02") ;
			else
				iconBack[i].ChangeSprite("Button_Shop_icon_Background01") ;
		}
		
	}
	public GameObject[] showTank ;
	private GameObject  currentShowTank ;
	private Show         showScript ;
	void InitShowTank()
	{
		if(currentShowTank==null)
		{
			currentShowTank = Instantiate(showTank[PlayerManager.currentTank]) as GameObject ;
			showScript = currentShowTank.GetComponent<Show>() ;
		}
	}
	private int[] armorSpendMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
	private int[] weaponsSpendMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
	private int[] moveSpeedSpendMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
	private int[] attackSpeedMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
	private int[] turretSpeedMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
	private int[] propsAdsorptionMoney = new int[]{380,660,940,1250,1560,1900,2240,2610,2980,3380,0} ;
//	private int   bombMoney = 1000 ;
//	private int   skillMoney = 2000 ;
//	private int   doubleGoldMoney = 50000 ;
//	private int   resurrectionMoney = 2000 ;
//	private int   godMoney = 5000 ;
	void ChangeLevel()
	{
		if(selectType==0)
		{
			currentLevel[0].text = PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel.ToString()+"/10" ;
			currentLevel[1].text = PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel.ToString()+"/10" ;
			currentLevel[2].text = PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel.ToString()+"/10" ;
			currentLevel[3].text = PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel.ToString()+"/10" ;
			currentLevel[4].text = PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel.ToString()+"/10" ;
			currentLevel[5].text = PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel.ToString()+"/10" ;
			costGold[0].text     = weaponsSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel].ToString() ;  
			costGold[1].text     = armorSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel].ToString() ;
			costGold[2].text     = moveSpeedSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel].ToString() ;
			costGold[3].text     = attackSpeedMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel].ToString() ;
			costGold[4].text     = turretSpeedMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel].ToString() ;
			costGold[5].text     = propsAdsorptionMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel].ToString() ;
		}
//		else if(selectType==1)
//		{
//			currentLevel[0].text = "X "+UIManager.BombNum.ToString() ;
//			currentLevel[1].text = "X "+UIManager.SkillNum.ToString() ;
//			currentLevel[2].text = "X "+UIManager.DoubleGoldNum.ToString() ;
//			currentLevel[3].text = "X "+UIManager.ResurrectionNum.ToString() ;
//			currentLevel[4].text = "X "+UIManager.GodNum.ToString() ;
//			costGold[0].text     = bombMoney.ToString() ;  
//			costGold[1].text     = skillMoney.ToString() ;
//			costGold[2].text     = doubleGoldMoney.ToString() ;
//			costGold[3].text     = resurrectionMoney.ToString() ;
//			costGold[4].text     = godMoney.ToString() ;
//		}
	}
	void Update ()
	{
		//goldNum.text = PlayerManager.playerGoldNum.ToString() ;
		goldNum.text = UserData.Instance.player_gold_num.ToString();
	}
	private void ChangeSprite()
	{
		typeSprite[0].ChangeSprite ( "UI_Shop_Attribute_Tank0"+(PlayerManager.currentTank+1)+"_FirePower0"+(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel/3+1)) ;
		typeSprite[1].ChangeSprite ( "UI_Shop_Attribute_Tank0"+(PlayerManager.currentTank+1)+"_Armor0"+(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel/3+1)) ;
		
		if(showScript!=null)
		{
			showScript.ArmorsChange();
			showScript.WeaponsChange();
		}
	}
	public UISprite[] iconBack ;
	void IconBackButton(GameObject obj)
	{
		if(isClickButton)
		{
			return ;
		}
		selectIco = int.Parse(obj.name) ;
		ChangeShowInfo() ;
	}
	void UpgradeButton(GameObject obj)
	{
		if(isClickButton)
		{
			return ;
		}
		switch(obj.name)
		{
		case "UpgradeButton1":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(weaponsSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].weaponsLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
//			else if(selectType==1)
//			{
//				if(PlayerManager.LessenGold(bombMoney))
//				{
//					UIManager.BombNum ++ ;
//					ChangeLevel() ;
//				}
//			}
			selectIco = 0 ;
			break;
		case "UpgradeButton2":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(armorSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].armorLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
//			else if(selectType==1)
//			{
//				if(PlayerManager.LessenGold(skillMoney))
//				{
//					UIManager.SkillNum ++ ;
//					ChangeLevel() ;
//				}
//			}
			selectIco = 1 ;
			break;
		case "UpgradeButton3":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(moveSpeedSpendMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].moveSpeedLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
//			else if(selectType==1)
//			{
//				if(PlayerManager.LessenGold(doubleGoldMoney))
//				{
//					UIManager.DoubleGoldNum ++ ;
//					ChangeLevel() ;
//				}
//			}
			selectIco = 2 ;
			break;
		case "UpgradeButton4":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(attackSpeedMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].attackSpeedLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
//			else if(selectType==1)
//			{
//				if(PlayerManager.LessenGold(resurrectionMoney))
//				{
//					UIManager.ResurrectionNum ++ ;
//					ChangeLevel() ;
//				}
//			}
			selectIco = 3 ;
			break;
		case "UpgradeButton5":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(turretSpeedMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].turretSpeedLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
//			else if(selectType==1)
//			{
//				if(PlayerManager.LessenGold(godMoney))
//				{
//					UIManager.GodNum ++ ;
//					ChangeLevel() ;
//				}
//			}
			selectIco = 4 ;
			break;
		case "UpgradeButton6":
			if(selectType==0)
			{
				if(PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel>=10)
				{
					return ;
				}
				if(PlayerManager.LessenGold(propsAdsorptionMoney[PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel]))
				{
					PlayerManager.playerAttribute[PlayerManager.currentTank].propsAdsorptionLevel++ ;
				}
				ChangeLevel() ;
				ChangeSprite() ;
			}
			selectIco = 5 ;
			break;
		}
		ChangeShowInfo();
	}
}
