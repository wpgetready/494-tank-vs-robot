using UnityEngine;
using System.Collections;

public class GameSetting : MonoBehaviour {
	
	public UISlider[] slider;
	
	
	void Awake() {
		slider[0].sliderValue = UserData.Instance.user_setting.sound_volume ;
		slider[1].sliderValue = UserData.Instance.user_setting.music_volume ;
	}
	
	// Use this for initialization
	void Start () {
		slider[0].sliderValue = UserData.Instance.user_setting.sound_volume ;
		slider[1].sliderValue = UserData.Instance.user_setting.music_volume ;

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void SoundChange(float val)
	{
		UIManager.UIManagerScript.SoundChange(val);
	}
	void MusicChange(float val)
	{
		UIManager.UIManagerScript.MusicChange(val);
	}
	
	void ButtonMessage(GameObject obj)
	{
		string buttonName = obj.name ;
		switch(buttonName)
		{
		case "Close":
			Close();
			break;
		}
	}
	
	void Close()
	{
		GameData.Instance.WriteUserData();
		NGUITools.Destroy(gameObject) ;
		UIManager.UIManagerScript.ShowGameMenu();
	}

}
