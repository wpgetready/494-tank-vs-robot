using UnityEngine;
using System.Collections;

public class GameHelp : MonoBehaviour {
	
	void Awake() {
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void ButtonMessage(GameObject obj)
	{
		string buttonName = obj.name ;
		switch(buttonName)
		{
		case "Close":
			Close();
			break;
		}
	}
	
	void Close()
	{
		NGUITools.Destroy(gameObject) ;
		UIManager.UIManagerScript.ShowGameMenu();
	}
	
}
