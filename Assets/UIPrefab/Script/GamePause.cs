using UnityEngine;
using System.Collections;

public class GamePause : MonoBehaviour 
{
	public UISlider[] slider ;
	public UITweener[]   tweenTrans ;
	private string            buttonName ;
	private AsyncOperation    async ;
	//private bool  isClickButton ;
	
	void Awake () 
	{
		slider[0].sliderValue = UserData.Instance.user_setting.sound_volume ;
		slider[1].sliderValue = UserData.Instance.user_setting.music_volume ;
	}
	
	void SoundChange(float val)
	{
		UIManager.UIManagerScript.SoundChange(val);
	}
	
	void MusicChange(float val)
	{
		UIManager.UIManagerScript.MusicChange(val);
	}
	
	void Start() {
		
	}
	
	void Update() {
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	
	void OnDestroy()
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			Time.timeScale = 1 ;
		}
		async = null ;
	}
	
	void PlayAnimate()
	{
		//isClickButton = false ;
		for(int i=0;i<tweenTrans.Length;i++)
		{
			tweenTrans[i].Toggle() ;
		}
	}
	/*
	void AnimateFinished()
	{
		switch(buttonName)
		{
		case "ReturnMenu":
			StartCoroutine(ReturnMainMenu()) ;
			break;
		case "Close":
			UIManager.gameState = UIManager.GameState.run ;
			UIManager.UIManagerScript.CloseBackGround() ;
			NGUITools.Destroy(gameObject) ;
			break;
		case "Retry":
			StartCoroutine(Restart()) ;
			break;
		}
		buttonName = "" ;
		isClickButton = true ;
	}
	void ButtonMessage(GameObject obj)
	{
		if(!isClickButton)
			return;
		buttonName = obj.name ;
		switch(buttonName)
		{
		case "ReturnMenu":
			PlayAnimate();
			break;
		case "GameHelp":
			PlayAnimate();
			break;
		case "Close":
			PlayAnimate();
			break;
		case "Quit":
			UIManager.SaveData() ;
			Application.Quit() ;
			break;
		case "Retry":
			PlayAnimate();
			break;
			
			
		}
		
	}
	*/
	
	void ButtonMessage(GameObject obj)
	{
		string buttonName = obj.name ;
		switch(buttonName)
		{
		case "Quit":
			UIManager.SaveData() ;
			GameData.Instance.WriteUserData();
			Application.Quit() ;
			break;
		case "ReturnGameLevel":
			StartCoroutine(ReturnGameLevel()) ;
			break;
		case "Continue":
		case "Close":
			Close();
			break;
		case "Retry":
			StartCoroutine(Restart()) ;
			break;
		}
		
	}
	
	void Close()
	{
		UIManager.gameState = UIManager.GameState.run ;
		UIManager.UIManagerScript.CloseBackGround() ;
		NGUITools.Destroy(gameObject) ;
	}
	
	IEnumerator ReturnMainMenu()
	{
		async = Application.LoadLevelAsync(2) ;
		yield return async ;
		NGUITools.Destroy(gameObject) ;
		UIManager.UIManagerScript.ShowGameMenu();
		UIManager.UIManagerScript.CloseGameScene() ;
		
	}
	
	IEnumerator ReturnGameLevel()
	{
		Time.timeScale = 1;
		async = Application.LoadLevelAsync(2) ;
		yield return async ;
		NGUITools.Destroy(gameObject) ;
		UIManager.UIManagerScript.ShowGameLevel();
		UIManager.UIManagerScript.CloseGameScene() ;
	}
	
	IEnumerator Restart()
	{
		async = Application.LoadLevelAsync(1) ;
		yield return async ;
		UIManager.UIManagerScript.CloseBackGround() ;
		UIManager.UIManagerScript.ShowGameScene() ;
		Time.timeScale = 1 ;
		NGUITools.Destroy(gameObject) ;
		
	}
}
