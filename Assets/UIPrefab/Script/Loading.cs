using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour 
{
	public Transform[] loadSprite ; 
	private int         index ;
	
	void Start()
	{
		//ios无硬计费点，注释掉
		/*
		bool isPassed = UserData.Instance.isPassedLevelAndSubLevel(GameLevel.current_level_id,GameLevel.current_sub_level_id);
		bool isLocked = UserData.Instance.isLockedLevel(GameLevel.current_level_id,GameLevel.current_sub_level_id);
		Debug.Log("level_id:"+GameLevel.current_level_id+" sublevel_id:"+GameLevel.current_sub_level_id);
		Debug.Log("isPassed:"+isPassed+" isLocked:"+isLocked);
		if(!isPassed && isLocked)
		{
			NGUITools.Destroy(this.gameObject);
			UIManager.UIManagerScript.ShowBackGround(1);
			UIManager.UIManagerScript.ShowLevelShop();
		}
		else
		*/
		{
			StartCoroutine(Load()) ;
		}
	}
	private AsyncOperation async ;
	IEnumerator Load()
	{
		async = Application.LoadLevelAsync(1) ;
		yield return async ;
		async = null ;
		NGUITools.Destroy(this.gameObject) ;
		UIManager.UIManagerScript.CloseBackGround() ;
		UIManager.UIManagerScript.ShowGameScene() ;
	}
	
	private float t ;
	void Update () 
	{
		t += 0.01f ;
		if(t>0.3f)
		{
			t = 0 ;
			index ++ ;
			if(index>2)
			{
				index = 0 ;
			}
			for(int i=0;i<loadSprite.Length;i++)
			{
				if(i == index)
				{
					loadSprite[i].localScale = new Vector3(60,60,1) ;
				}
				else
				{
					loadSprite[i].localScale = new Vector3(46,46,1) ;
				}
			}
		}
	}
	
	void OnDestroy()
	{
	}
}
