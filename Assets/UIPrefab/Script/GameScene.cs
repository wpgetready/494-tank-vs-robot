using UnityEngine;
using System.Collections;

public class GameScene : MonoBehaviour 
{
	public GameObject[]   ui ;
	public GameObject TipWavePanel ;
	public UISprite[]     waveSprite ;
	public GameObject PromptPanel ;
	public UILabel[]    sceneLabel ;
	//public Transform    bloodBar ;    // 血条 
	public UIWidget[]  unitWidget ;
	public UISprite BloodBar;
	public UISprite icon;
	
	void Awake() {

	}
	
	void Start () 
	{

		ShowHud();
		//sceneLabel[0].text = PlayerManager.playerGoldNum.ToString() ;
		/*
		sceneLabel[0].text = UserData.Instance.player_gold_num.ToString();
		sceneLabel[2].text = NPCManager.currentWaveNum.ToString()
				+"/"
				+GameLevel.current_sub_level_info.wave_num;		//hidden the score and scoreNum
		*/
		//sceneLabel[1].transform.localScale = new Vector3(0,0,0);
		//sceneLabel[3].transform.localScale = new Vector3(0,0,0);
		//sceneLabel[1].text = PlayerManager.playerScoreNum.ToString() ;
		
				
		NGUITools.SetActive(TipWavePanel,false) ;
		CloseUnits() ;
		ClosePropButton();
		Invoke("StartGame",5) ;
		
	}
	
	void Update () 
	{
		//sceneLabel[0].text = PlayerManager.playerGoldNum.ToString() ;
		////sceneLabel[0].text = UserData.Instance.player_gold_num.ToString();
		switch(UIManager.gameState)
		{
			case UIManager.GameState.run:
				
				if(UIManager.isShowWaveTip)
				{
					ShowWaveTip() ;
					ShowUnits() ;
					////sceneLabel[2].text = NPCManager.currentWaveNum.ToString()+"/"+GameLevel.current_sub_level_info.wave_num;
				}
				ChangeUnitsNum();
				ShowPropIcon();
				ShowPropButton();
				
				////BloodBar.fillAmount = PlayerManager.playerHP/PlayerManager.playerMaxHP;
				ShowHud();
				break;
		}
		
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Close();
		}
	}
	void StartGame()
	{
		UIManager.gameState = UIManager.GameState.run ;
		NGUITools.SetActive(PromptPanel,false) ;
	}
	
	
	void ShowHud()
	{
		sceneLabel[0].text = UserData.Instance.player_gold_num.ToString();
		sceneLabel[2].text = NPCManager.currentWaveNum.ToString()+"/"+GameLevel.current_sub_level_info.wave_num;
		if(PlayerManager.playerMaxHP == 0)
		{
			BloodBar.fillAmount = 1;
		}
		else{
			BloodBar.fillAmount = PlayerManager.playerHP/PlayerManager.playerMaxHP;
		}
		
		if(UserData.Instance.player_attribute.isAllReachMaxLevel(15))
		{
			icon.ChangeSprite("tank3");
		}
		else if(UserData.Instance.player_attribute.weaponsLevel >=8)
		{
			icon.ChangeSprite("tank2");
		}
		else if(UserData.Instance.player_attribute.weaponsLevel >=0)
		{
			icon.ChangeSprite("tank1");
		}
		
	}
	/// <summary>
	/// 显示NPC图标.
	/// </summary>
	public Transform[] units ;
	public UILabel[]   unitsLabel ;
	private float      unitsPos ;
	void ShowUnits()
	{
		unitsPos = 0;
		foreach(DictionaryEntry item in GameLevel.current_sub_level_info.npcs)
		{
			NpcInfo npc_info = item.Value as NpcInfo;
			units[NpcIdToUnitsLabelIdx(npc_info.npc_id)].localPosition = new Vector3(unitsPos,-10,0);
			unitsPos += 60;
			unitsLabel[NpcIdToUnitsLabelIdx(npc_info.npc_id)].text = npc_info.num.ToString();//old:NPCManager.units1Num.ToString() ;
			unitWidget[NpcIdToUnitsLabelIdx(npc_info.npc_id)*2].enabled = true ;
			unitWidget[NpcIdToUnitsLabelIdx(npc_info.npc_id)*2+1].enabled = true;
		}
	}
	
	public int NpcIdToUnitsLabelIdx(int npc_id)
	{
		if(npc_id <=5)
		{
			return npc_id-1;
		}else if(npc_id > 100)//boss
		{
			return npc_id-1-100+5;
		}
		return -1;
	}
	
	void ChangeUnitsNum()
	{
		foreach(DictionaryEntry item in NPCManager.leftNpcNum)
		{
			int npc_id = (int)(item.Key);
			string left_num = item.Value.ToString();
			unitsLabel[NpcIdToUnitsLabelIdx(npc_id)].text = left_num;
		}
	}
	void CloseUnits()
	{
		for(int i=0;i<unitWidget.Length;i++)
		{
			unitWidget[i].enabled = false ;
		}
	}
	/// <summary>
	/// 显示道具图标.
	/// </summary>
	public UISprite[] propSprite ;
	void ShowPropIcon()
	{
		if(UIManager.firePowerProp>0)
		{
			UIManager.firePowerProp -= Time.deltaTime*0.1f ;
			propSprite[0].alpha = UIManager.firePowerProp ;
		}
		else
		{
			propSprite[0].alpha = 0 ;
		}
		if(UIManager.magnetProp>0)
		{
			UIManager.magnetProp -= Time.deltaTime*0.1f ;
			propSprite[1].alpha = UIManager.magnetProp ;
		}
		else
		{
			propSprite[1].alpha = 0 ;
		}
		if(UIManager.shieldProp>0)
		{
			UIManager.shieldProp -= Time.deltaTime*0.1f ;
			propSprite[2].alpha = UIManager.shieldProp ;
		}
		else
		{
			propSprite[2].alpha = 0 ;
		}
		if(UIManager.speedProp>0)
		{
			UIManager.speedProp -= Time.deltaTime*0.1f ;
			propSprite[3].alpha = UIManager.speedProp ;
		}
		else
		{
			propSprite[3].alpha = 0 ;
		}
	}
	
	void UseProp(string name)
	{
		int prop_id = MoneyPropConfig.Instance.StringToPropId(name);
		UserProp up = (UserProp)UserData.Instance.GetUserProp(prop_id);
		if(up!=null && up.num>0)
		{
			UserData.Instance.SubUserProp(prop_id,1);
			switch(name)
			{
			case "FixBox":
				PlayerManager.AddHP(30);
				break;
			case "FirePower":
				UIManager.firePowerProp = 1;
				break;
			case "Shield":
				UIManager.shieldProp = 1;
				break;
			case "Speed":
				UIManager.speedProp = 1;
				break;
			}
			GameData.Instance.WriteUserData();
		}
		else
		{
			//UIManager.UIManagerScript.ShowPopUpShop("MoneyShop","GameScene",UIManager.gameState,this.transform.localPosition.z-25);
			UIManager.UIManagerScript.ShowHintShop("GameScene",UIManager.gameState,name,this.transform.localPosition.z-25);
		}
	}
	
	public UIImageButton[] propButton;
	void ShowPropButton()
	{
		for(int i=0;i<propButton.Length;i++)
		{
			int prop_id = MoneyPropConfig.Instance.StringToPropId(propButton[i].name);
			UserProp up = (UserProp)UserData.Instance.GetUserProp(prop_id);
			int num = 0;
			if(up != null)
			{
				num = up.num;
			}
			
			propButton[i].enabled = true;
			propButton[i].transform.localScale = new Vector3(1,1,1);
			propButton[i].transform.Find("Label").GetComponent<UILabel>().text = num.ToString();
		}
	}
	
	void ClosePropButton()
	{
		for(int i=0;i<propButton.Length;i++)
		{
			propButton[i].enabled = false;
			propButton[i].transform.localScale = Vector3.zero;
		}
	}
	
	
	void ShowWaveTip()
	{
		UIManager.isShowWaveTip = false ;
		waveSprite[0].spriteName = "UI_Num"+(NPCManager.currentWaveNum/10).ToString()  ;
		waveSprite[1].spriteName = "UI_Num"+(NPCManager.currentWaveNum%10).ToString() ;
		NGUITools.SetActive(TipWavePanel,true) ;
		TipWavePanel.animation.Play() ;
	}
	
	void ButtonMessage(GameObject obj)
	{
		if(PlayerManager.playerTransform.gameObject.active==false)
		{
			return;
		}
		switch(obj.name)
		{
		case "Pause":
			UIManager.UIManagerScript.ShowGamePause(this.transform.localPosition.z-25) ;
			UIManager.UIManagerScript.ShowBackGround(0,(float)this.transform.localPosition.z-24);
			AdmobControl.Instance.showInterstitial ();
			break;
		case "GoldShop":
			UIManager.UIManagerScript.ShowBackGround(1,(float)this.transform.localPosition.z-24) ;
			UIManager.UIManagerScript.ShowGoldShop(this.transform.localPosition.z-25) ;
			break;
		case "MoneyShop":
			UIManager.UIManagerScript.ShowBackGround(1,(float)this.transform.localPosition.z-24) ;
			UIManager.UIManagerScript.ShowMoneyShop(this.transform.localPosition.z-25) ;
			break;
			/*
		case "FixBox":
			PlayerManager.AddHP(30);
			break;
		case "FirePower":
			UIManager.firePowerProp = 1;
			break;
		case "Shield":
			UIManager.shieldProp = 1;
			break;
		case "Speed":
			UIManager.speedProp = 1;
			break;
			*/
		case "FixBox":
		case "FirePower":
		case "Shield":
		case "Speed":
			UseProp(obj.name);
			break;
		}
	}
	
	void Close()
	{
		if(UIManager.gameState == UIManager.GameState.run)
		{
			StartCoroutine(ReturnGameLevel()) ;
		}
	}
	
	private AsyncOperation async;
	IEnumerator ReturnGameLevel()
	{
		async = Application.LoadLevelAsync(2) ;
		yield return async ;
		NGUITools.Destroy(gameObject) ;
		UIManager.UIManagerScript.CloseGameScene();
		UIManager.UIManagerScript.ShowBackGround();
		UIManager.UIManagerScript.ShowGameLevel();
	}
}
